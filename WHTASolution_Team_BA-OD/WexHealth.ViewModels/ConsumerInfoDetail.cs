﻿using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.ViewModels
{
    public class ConsumerInfoDetail : IConsumerInfoDetail
    {
        public int ConsumerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SSN { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public byte StateId { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string EmployerInfo { get; set; }
    }
}