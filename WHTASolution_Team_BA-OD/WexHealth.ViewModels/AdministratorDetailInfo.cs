﻿using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.ViewModels
{
    public class AdministratorDetailInfo : IAdministratorDetailInfo
    {
        public int AdminId { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public byte StateId { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
    }
}