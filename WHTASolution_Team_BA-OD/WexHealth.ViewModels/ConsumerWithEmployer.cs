﻿using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.ViewModels
{
    public class ConsumerWithEmployer : ConsumerInfo, IConsumerWithEmployer
    {
        public int EmployerId { get; set; }
        public int ParticipantId { get; set; }
        public string EmployerName { get; set; }
        public string EmployerCode { get; set; }
    }
}
