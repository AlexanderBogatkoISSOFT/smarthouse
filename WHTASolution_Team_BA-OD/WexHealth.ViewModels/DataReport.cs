﻿using System;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.ViewModels
{
    public class DataReport : IDataReport
    {
        public string RequestedStatus { get; set; }
        public DateTime DateTime { get; set; }
        public string RequestedBy { get; set; }
        public string Format { get; set; }
    }
}