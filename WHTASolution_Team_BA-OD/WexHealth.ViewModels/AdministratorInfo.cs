﻿using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.ViewModels
{
    public class AdministratorInfo : IAdministratorInfo
    {
        public int AdminId { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
    }
}