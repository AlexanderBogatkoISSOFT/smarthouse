﻿using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.ViewModels
{
    public class EnrollmentInfo : IEnrollmentInfo
    {
        public int PlanId { get; set; }
        public string Plan { get; set; }
        public int Election { get; set; }
        public int Contribution { get; set; }
    }
}