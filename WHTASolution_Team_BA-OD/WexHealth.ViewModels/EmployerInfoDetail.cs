﻿using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.ViewModels
{
    public class EmployerInfoDetail : EmployerInfo, IEmployerInfoDetail
    {
        public string Street { get; set; }
        public string City { get; set; }
        public byte StateId { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public byte[] Logo { get; set; }
    }
}