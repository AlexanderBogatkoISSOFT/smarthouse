﻿using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.ViewModels
{
    public class ConsumerInfo : IConsumerInfo
    {
        public int ConsumerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SSN { get; set; }
    }
}