﻿using System;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.ViewModels
{
    public class ClaimInfo : IClaimInfo
    {
        public long ClaimId { get; set; }
        public int ConsumerId { get; set; }
        public int EmployerId { get; set; }
        public string ClaimNumber { get; set; }
        public string ConsumerName { get; set; }
        public string EmployerName { get; set; }
        public string ConsumerPhoneNumber { get; set; }
        public DateTime DateOfService { get; set; }
        public byte Plan { get; set; }
        public decimal Amount { get; set; }
    }
}