﻿using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.ViewModels
{
    public class EmployerInfo : IEmployerInfo
    {
        public int EmployerId { get; set; }
        public int AdminId { get; set; }
        public string EmployerName { get; set; }
        public string EmployerCode { get; set; }
    }
}