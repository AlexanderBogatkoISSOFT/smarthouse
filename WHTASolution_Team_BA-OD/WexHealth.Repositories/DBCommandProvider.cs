﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace WexHealth.Repositories
{
    /// <summary>
    /// Provides method set to execute sql commands.
    /// </summary>
    public class DBCommandProvider
    {
        private readonly SqlConnection _connection;

        /// <summary>
        /// Creates new abstract repository with connection to database.
        /// </summary>
        /// <param name="connectionString">Special <seealso cref="string"/> variable that represents set of key-value pairs that are formed a connection to database.</param>
        public DBCommandProvider(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

            _connection = new SqlConnection(connectionString);
        }

        /// <summary>
        /// Executes <seealso cref="SqlCommand"/> that represents sql procedure with given <paramref name="procedureName"/>.
        /// </summary>
        /// <param name="procedureName">Full name (with schema) of storage procedure in database.</param>
        /// <param name="parameters">Parameters of called procedure.</param>
        public void ExecuteCommand(string procedureName, IEnumerable<SqlParameter> parameters = null)
        {
            using (var command = new SqlCommand(procedureName, _connection) { CommandType = CommandType.StoredProcedure })
            {
                _connection.Open();

                if (parameters != null && parameters.Count() > 0)
                {
                    foreach (var item in parameters)
                    {
                        command.Parameters.Add(item);
                    }
                }

                try
                {
                    command.ExecuteNonQuery();
                }
                finally
                {
                    _connection.Close();
                }
            }
        }

        /// <summary>
        /// Executes <seealso cref="SqlCommand"/> that represents sql procedure with given <paramref name="procedureName"/>.
        /// </summary>
        /// <typeparam name="T">Type of model that represents entity in database</typeparam>
        /// <param name="ExtractEntity">Extracts entity from <see cref="SqlDataReader"/>.</param>
        /// <param name="procedureName">Full name (with schema) of storage procedure in database.</param>
        /// <param name="parameters">Parameters of called procedure.</param>
        /// <returns>Model if extracting was completed successfully.</returns>
        public T ExecuteCommandWithReturnValue<T>(Func<SqlDataReader, T> ExtractEntity, string procedureName, IEnumerable<SqlParameter> parameters = null) where T : class
        {
            if (ExtractEntity is null)
            {
                throw new ArgumentNullException(nameof(ExtractEntity));
            }

            using (var command = new SqlCommand(procedureName, _connection) { CommandType = CommandType.StoredProcedure })
            {
                _connection.Open();

                if (parameters != null && parameters.Count() > 0)
                {
                    foreach (var item in parameters)
                    {
                        command.Parameters.Add(item);
                    }
                }

                try
                {
                    var reader = command.ExecuteReader();
                    return ExtractEntity(reader);
                }
                finally
                {
                    _connection.Close();
                }
            }
        }
    }
}