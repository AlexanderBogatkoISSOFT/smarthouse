﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using WexHealth.Core.Repositories.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;
using WexHealth.ViewModels;

namespace WexHealth.Repositories
{
    /// <summary>
    /// Represents a set of methods for <seealso cref="ClaimInfo" objects/> model for interacting with database.
    /// </summary>
    public class ClaimInfoRepository : IClaimInfoRepository
    {
        private readonly DBCommandProvider _dbCommand;

        /// <summary>
        /// Creates new instance with db connection
        /// </summary>
        /// <param name="connectionString">Connection string to database</param>
        public ClaimInfoRepository(string connectionString)
        {
            _dbCommand = new DBCommandProvider(connectionString);
        }

        /// <summary>
        /// Returns ClaimInfo collection which is filtered by params
        /// </summary>
        /// <param name="claimNumber">Represents number of claim in database</param>
        /// <param name="employerId">Represents id of employer in database</param>
        /// <returns>Collection of claims which filtered by employer's id or claim number</returns>
        public IEnumerable<IClaimInfo> Find(string claimNumber = null, int? employerId = null)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@claim_number", Value = CheckStringParam(claimNumber)},
                new SqlParameter() {ParameterName = "@employer_id", Value = CheckInt32Param(employerId)}
            };

            return _dbCommand.ExecuteCommandWithReturnValue(ExtractEntity, "usp_GetEmployers", sqlParams);
        }

        /// <summary>
        /// Return ClaimItem Collection
        /// </summary>
        public IEnumerable<IClaimInfo> GetAll()
        {
            return _dbCommand.ExecuteCommandWithReturnValue(ExtractEntity, "usp_GetClaims");
        }

        public IClaimInfo GetClaimInfoById(int claimId)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@parClaimId", Value = claimId},
            };

            return _dbCommand.ExecuteCommandWithReturnValue(ExtractClaimInfo, "usp_GetClaimById", sqlParams);
        }

        public void UpdateClaim(int claimId, DateTime? dateOfService, decimal? amount,  byte? plan)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter(){ParameterName = "@parClaimId", Value = claimId},
                new SqlParameter(){ParameterName = "@parDateOfService", Value = dateOfService},
                new SqlParameter(){ParameterName = "@parPlan", Value = plan},
                new SqlParameter(){ParameterName = "@parAmount", Value = amount}
            };

            _dbCommand.ExecuteCommand("usp_UpdateClaim", sqlParams);
        }

        /// <summary>
        /// Extracts <seealso cref="ClaimInfo"/> model from <see cref="SqlDataReader"/>.
        /// </summary>
        /// <param name="reader">Param which contains entity of <seealso cref="ClaimInfo"/> model</param>
        /// <returns>Collection of <seealso cref="ClaimInfo"/> model</returns>
        private ICollection<IClaimInfo> ExtractEntity(SqlDataReader reader)
        {
            if (!reader.HasRows)
            {
                return null;
            }

            var claimItems = new List<IClaimInfo>();
            while (reader.Read())
            {
                var claim = new ClaimInfo()
                {
                    ClaimId = reader.GetInt64(reader.GetOrdinal("claim_id")),
                    ConsumerId = reader.GetInt32(reader.GetOrdinal("consumer_id")),
                    EmployerId = reader.GetInt32(reader.GetOrdinal("employer_id")),
                    ClaimNumber = reader.GetString(reader.GetOrdinal("claim_number")),
                    ConsumerName = reader.GetString(reader.GetOrdinal("consumer_fullname")),
                    EmployerName = reader.GetString(reader.GetOrdinal("employer_fullname")),
                    DateOfService = reader.GetDateTime(reader.GetOrdinal("claim_dateOfService")),
                    Plan = reader.GetByte(reader.GetOrdinal("benefitsOffering_plan")),
                    Amount = reader.GetDecimal(reader.GetOrdinal("claim_amount"))
                };
                claimItems.Add(claim);
            }
            return claimItems;
        }


        private IClaimInfo ExtractClaimInfo(SqlDataReader reader)
        {
            if (!reader.HasRows)
            {
                return null;
            }

            if (!reader.Read())
            {
                return null;
            }

            var claim = new ClaimInfo()
            {
                ConsumerId = reader.GetInt32(reader.GetOrdinal("ConsumerId")),
                ClaimNumber = reader.GetString(reader.GetOrdinal("ClaimNumber")),
                ConsumerName = reader.GetString(reader.GetOrdinal("ConsumerName")),
                DateOfService = reader.GetDateTime(reader.GetOrdinal("DateOfService")),
                Plan = reader.GetByte(reader.GetOrdinal("Plan")),
                ConsumerPhoneNumber = reader.GetString(reader.GetOrdinal("PhoneNumber")),
                Amount = reader.GetDecimal(reader.GetOrdinal("Amount"))
            };

            return claim;
        }


        /// <summary>
        /// Returns database null value if string is null or <seealso cref="string.Empty"/>, otherwise <seealso cref="str"/>
        /// </summary>
        /// <param name="str">The string to test.</param>
        /// <returns><seealso cref="DBNull.Value"/> if the value parameter is null or an empty string (""); otherwise, <seealso cref="str"/>.</returns>
        private object CheckStringParam(string str)
        {
            object temp;
            if (string.IsNullOrEmpty(str))
            {
                temp = DBNull.Value;
            }
            else
            {
                temp = str;
            }

            return temp;
        }

        /// <summary>
        /// Returns database null value if <seealso cref="identifier"/> is null, otherwise <seealso cref="identifier"/>
        /// </summary>
        /// <param name="identifier">The value to test.</param>
        /// <returns><seealso cref="DBNull.Value"/> if the value parameter is null or an empty string (""); otherwise, <seealso cref="str"/>.</returns>
        private object CheckInt32Param(int? identifier)
        {
            object temp;
            if (identifier is null)
            {
                temp = DBNull.Value;
            }
            else
            {
                temp = identifier;
            }

            return temp;
        }
    }
}