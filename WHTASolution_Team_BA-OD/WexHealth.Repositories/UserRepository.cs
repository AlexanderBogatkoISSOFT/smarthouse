﻿using System.Collections.Generic;
using System.Data.SqlClient;
using WexHealth.Core.Repositories.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;
using WexHealth.ViewModels;

namespace WexHealth.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DBCommandProvider _dbCommand;

        public UserRepository(string connectionString)
        {
            _dbCommand = new DBCommandProvider(connectionString);
        }

        /// <summary>
        /// Get all users by Employer Id
        /// </summary>
        /// <param name="employerId"></param>
        /// <returns></returns>
        public IEnumerable<IUserInfo> GetUsersByEmployerId(int employerId)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@parEmployerId", Value = employerId}
            };

            return _dbCommand.ExecuteCommandWithReturnValue(ExtractUsersInfo, "usp_GetUsersInfo", sqlParams);
        }

        /// <summary>
        /// Get All users by Admin id
        /// </summary>
        /// <param name="adminId"></param>
        /// <returns></returns>
        public IEnumerable<IUserInfo> GetUsersByAdminId(int adminId)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@parAdminId", Value = adminId}
            };

            return _dbCommand.ExecuteCommandWithReturnValue(ExtractUsersInfo, "usp_GetUsersInfo", sqlParams);
        }

        /// <summary>
        /// Insert new user in database
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="loginType"></param>
        public void InsertUser(IUserInfo userInfo, int loginType)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@parFirstName ", Value = userInfo.FirsName},
                new SqlParameter() {ParameterName = "@parLastName", Value = userInfo.LastName},
                new SqlParameter() {ParameterName = "@parUserName", Value = userInfo.UserName},
                new SqlParameter() {ParameterName = "@parEmail", Value = userInfo.Email},
                new SqlParameter() {ParameterName = "@parPassword  ", Value = userInfo.Password},
                new SqlParameter() {ParameterName = "@parEmployerId", Value = userInfo.EmployerId},
                new SqlParameter() {ParameterName = "@parLoginType", Value = loginType}
            };

            _dbCommand.ExecuteCommand("usp_InsertUserInfo", sqlParams);
        }

        /// <summary>
        /// Update exist user
        /// </summary>
        /// <param name="userInfo"></param>
        public void UpdateUser(IUserInfo userInfo)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@parFirstName ", Value = userInfo.FirsName},
                new SqlParameter() {ParameterName = "@parLastName", Value = userInfo.LastName},
                new SqlParameter() {ParameterName = "@parUserName", Value = userInfo.UserName},
                new SqlParameter() {ParameterName = "@parEmail", Value = userInfo.Email},
                new SqlParameter() {ParameterName = "@parPassword  ", Value = userInfo.Password},
                new SqlParameter() {ParameterName = "@parLoginId", Value = userInfo.LoginId}
            };

            _dbCommand.ExecuteCommand("usp_UpdateUserInfoById", sqlParams);
        }

        public void SaveChanges(IUserInfo userInfo, int? loginType)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@parFirstName ", Value = userInfo.FirsName},
                new SqlParameter() {ParameterName = "@parLastName", Value = userInfo.LastName},
                new SqlParameter() {ParameterName = "@parUserName", Value = userInfo.UserName},
                new SqlParameter() {ParameterName = "@parEmail", Value = userInfo.Email},
                new SqlParameter() {ParameterName = "@parPassword  ", Value = userInfo.Password}

            };

            if (userInfo.EmployerId > 0 && loginType != null)
            {
                sqlParams.Add(new SqlParameter() {ParameterName = "@parEmployerId", Value = userInfo.LoginId});
            }

            _dbCommand.ExecuteCommand("usp_InsertUpdateUserInfo", sqlParams);
        }


        /// <summary>
        /// Convert SqlDataReader data to List UserInfo
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public IEnumerable<IUserInfo> ExtractUsersInfo(SqlDataReader reader)
        {
            if (!reader.HasRows)
            {
                return null;
            }

            var users = new List<IUserInfo>();
            while (reader.Read())
            {
                var user = new UserInfo()
                {
                    LoginId = reader.GetInt32(reader.GetOrdinal("LoginId")),
                    FirsName = reader.GetString(reader.GetOrdinal("FirstName")),
                    LastName = reader.GetString(reader.GetOrdinal("LastName")),
                    UserName = reader.GetString(reader.GetOrdinal("UserName")),
                    Password = reader.GetString(reader.GetOrdinal("Passsword")),
                    EmployerId = reader.GetInt32(reader.GetOrdinal("EmployerId"))
                };
                users.Add(user);
            }
            return users;
        }
    }
}