﻿//using System;
//using System.Collections.Generic;
//using System.Data.SqlClient;
//using WexHealth.Models;

//namespace WexHealth.Repositories
//{
//    /// <summary>
//    /// Represents a set of methods for <seealso cref="Domain" objects/> model for interacting with database.
//    /// </summary>
//    public class DomainRepository : DBCommandProvider<Domain>, IRepository<Domain, short>
//    {
//        /// <summary>
//        /// Creates new instance of repository for <seealso cref="Domain" objects/> model.
//        /// </summary>
//        /// <param name="connectionString">Special <seealso cref="string"/> variable that represents set of key-value pairs that are formed a connection to database.</param>
//        public DomainRepository(string connectionString) : base(connectionString) { }
//        /// <summary>
//        /// Creates new <seealso cref="Domain"/> model in database.
//        /// </summary>
//        /// <param name="entity">Model that will be created in database</param>
//        public void Create(Domain entity)
//        {
//            var sqlParameters = new List<SqlParameter>()
//            {
//                new SqlParameter(){ParameterName = "@domain_id",Value = DBNull.Value},
//                new SqlParameter(){ParameterName = "@name",Value = entity.Name},
//            };

//            ExecuteCommand("usp_Domain_Update", sqlParameters);
//        }
//        /// <summary>
//        /// Deletes <seealso cref="Domain"/> model from database with the same identificator like <see cref="entity"/>
//        /// </summary>
//        /// <param name="entity">Model that will be deleted from database</param>
//        public void Delete(Domain entity)
//        {
//            var sqlParameters = new List<SqlParameter>()
//            {
//                new SqlParameter(){ParameterName = "@domain_id",Value = entity.DomainId}
//            };

//            ExecuteCommand("usp_Domain_Delete", sqlParameters);
//        }
//        /// <summary>
//        /// Modifies <seealso cref="Domain"/> model in database.
//        /// </summary>
//        /// <param name="entity">Model with new values.</param>
//        public void Update(Domain entity)
//        {
//            var sqlParameters = new List<SqlParameter>()
//            {
//                new SqlParameter(){ParameterName = "@domain_id", Value = entity.DomainId},
//                new SqlParameter(){ParameterName = "@name", Value = entity.Name}
//            };

//            ExecuteCommand("usp_Domain_Update", sqlParameters);
//        }
//        /// <summary>
//        /// Returns <seealso cref="Domain"/> model with <see cref="id"/> of entity in database.
//        /// </summary>
//        /// <param name="id">Identificator of existing entity in database.</param>
//        /// <returns><seealso cref="Domain"/> model if serching successfullty completed.</returns>
//        public Domain Get(short id)
//        {
//            var sqlParameters = new List<SqlParameter>()
//            {
//                new SqlParameter(){ParameterName = "@domain_id",Value = id}
//            };

//            var entity = ExecuteCommandWithReturnValue("usp_Domain_Read", sqlParameters);

//            return entity;
//        }

//        protected override Domain ExtractEntity(SqlDataReader reader)
//        {
//            if (reader.Read())
//            {
//                var entity = new Domain
//                {
//                    DomainId = reader.GetInt16(reader.GetOrdinal("domain_id")),
//                    Name = reader.GetString(reader.GetOrdinal("domain_name")),
//                    ChangeDate = reader.GetDateTime(reader.GetOrdinal("domain_change_date"))
//                };

//                return entity;
//            }

//            return null;
//        }
//    }
//}