﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using WexHealth.Core.Models.Interfaces;
using WexHealth.Core.Repositories.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;
using WexHealth.Models;
using WexHealth.ViewModels;

namespace WexHealth.Repositories
{
    /// <summary>
    /// Represents a set of methods for <seealso cref="Employer" objects/> model for interacting with database.
    /// </summary>
    public class EmployerRepository : IEmployerRepository
    {
        private readonly DBCommandProvider _dbCommand;

        /// <summary>
        /// Creates new instance of repository for <seealso cref="Employer" objects/> model.
        /// </summary>
        /// <param name="connectionString">Special <seealso cref="string"/> variable that represents set of key-value pairs that are formed a connection to database.</param>
        public EmployerRepository(string connectionString)
        {
            _dbCommand = new DBCommandProvider(connectionString);
        }

        /// <summary>
        /// Returns Employers collection
        /// </summary>
        public IEnumerable<IEmployer> GetAll()
        {
            return _dbCommand.ExecuteCommandWithReturnValue(ExtractEntity, "usp_GetEmployers");
        }

        /// <summary>
        /// Returns Employers collection which is filtered by params
        /// </summary>
        /// <param name="employerName">Represents name of employer in database</param>
        /// <param name="employerCode">Represents code (short name) of employer in database</param>
        /// <returns>Collection of employers which filtered by employer's name or code</returns>
        public IEnumerable<IEmployer> Find(string employerName = null, string employerCode = null)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@employer_name", Value = CheckStringParam(employerName)},
                new SqlParameter() {ParameterName = "@employer_code", Value = CheckStringParam(employerCode)}
            };

            return _dbCommand.ExecuteCommandWithReturnValue(ExtractEntity, "usp_GetEmployers", sqlParams);
        }

        /// <summary>
        /// Returns Employer Detail Info by Id
        /// </summary>
        /// <param name="employerId">Represents Id of employer in database</param>
        /// <returns>Employer Detail Info <seealso cref="IEmployerInfoDetail"/></returns>
        public IEmployerInfoDetail GetEmployerDetailById(int employerId)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@parEmployerId", Value = employerId}
            };

            return _dbCommand.ExecuteCommandWithReturnValue(ExtractEmployerDetail, "usp_GetEmployerInfoById", sqlParams);
        }

        /// <summary>
        /// Insert new Employer in database
        /// </summary>
        /// <param name="employerInfoDetail"></param>
        public void AddNewEmployer(IEmployerInfoDetail employerInfoDetail)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@parAdminId", Value = employerInfoDetail.AdminId},
                new SqlParameter() {ParameterName = "@parName", Value = employerInfoDetail.EmployerName},
                new SqlParameter() {ParameterName = "@parCode", Value = employerInfoDetail.EmployerCode},
                new SqlParameter() {ParameterName = "@parStreet", Value = employerInfoDetail.Street},
                new SqlParameter() {ParameterName = "@parCity", Value = employerInfoDetail.City},
                new SqlParameter() {ParameterName = "@parStateId", Value = employerInfoDetail.StateId},
                new SqlParameter() {ParameterName = "@parZipCode", Value = employerInfoDetail.ZipCode},
                new SqlParameter() {ParameterName = "@parPhoneNumber", Value = employerInfoDetail.Phone},
                new SqlParameter() {ParameterName = "@parLogoData", Value = employerInfoDetail.Logo},
            };

            _dbCommand.ExecuteCommand("usp_InsertEmployerInfo", sqlParams);
        }

        /// <summary>
        /// Update exist employer data
        /// </summary>
        /// <param name="employerInfoDetail"></param>
        public void UpdateEmployer(IEmployerInfoDetail employerInfoDetail)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@parEmployerId", Value = employerInfoDetail.EmployerId},
                new SqlParameter() {ParameterName = "@parName", Value = employerInfoDetail.EmployerName},
                new SqlParameter() {ParameterName = "@parCode", Value = employerInfoDetail.EmployerCode},
                new SqlParameter() {ParameterName = "@parStreet", Value = employerInfoDetail.Street},
                new SqlParameter() {ParameterName = "@parCity", Value = employerInfoDetail.City},
                new SqlParameter() {ParameterName = "@parStateId", Value = employerInfoDetail.StateId},
                new SqlParameter() {ParameterName = "@parZipCode", Value = employerInfoDetail.ZipCode},
                new SqlParameter() {ParameterName = "@parPhoneNumber", Value = employerInfoDetail.Phone},
                new SqlParameter() {ParameterName = "@parLogoData", Value = employerInfoDetail.Logo},
            };

            _dbCommand.ExecuteCommand("usp_UpdateEmployerInfo", sqlParams);
        }

        /// <summary>
        /// Insert new Employer or Update exist employer
        /// </summary>
        /// <param name="employerInfoDetail"></param>
        public void SaveChanges(IEmployerInfoDetail employerInfoDetail)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@parName", Value = employerInfoDetail.EmployerName},
                new SqlParameter() {ParameterName = "@parCode", Value = employerInfoDetail.EmployerCode},
                new SqlParameter() {ParameterName = "@parStreet", Value = employerInfoDetail.Street},
                new SqlParameter() {ParameterName = "@parCity", Value = employerInfoDetail.City},
                new SqlParameter() {ParameterName = "@parStateId", Value = employerInfoDetail.StateId},
                new SqlParameter() {ParameterName = "@parZipCode", Value = employerInfoDetail.ZipCode},
                new SqlParameter() {ParameterName = "@parPhoneNumber", Value = employerInfoDetail.Phone},
                new SqlParameter() {ParameterName = "@parLogoData", Value = employerInfoDetail.Logo},
            };


            if (employerInfoDetail.EmployerId == default(int) && employerInfoDetail.AdminId != default(int))
            {
                sqlParams.Add(new SqlParameter() { ParameterName = "@parAdminId", Value = employerInfoDetail.AdminId });
            }
            else
            {
                sqlParams.Add(new SqlParameter() { ParameterName = "@parEmployerId", Value = employerInfoDetail.EmployerId });
            }

            _dbCommand.ExecuteCommand("usp_InsertUpdateEmployerInfo", sqlParams);
        }

        /// <summary>
        /// Extracts <seealso cref="Employer"/> model from <see cref="SqlDataReader"/>.
        /// </summary>
        /// <param name="reader">Param which contains entity of <seealso cref="Employer"/> model</param>
        /// <returns>Collection of <seealso cref="Employer"/> model</returns>
        private IEnumerable<IEmployer> ExtractEntity(SqlDataReader reader)
        {
            if (!reader.HasRows)
            {
                return null;
            }

            var employers = new List<IEmployer>();
            while (reader.Read())
            {
                var employer = new Employer()
                {
                    EmployerId = reader.GetInt32(reader.GetOrdinal("employer_id")),
                    Name = reader.GetString(reader.GetOrdinal("employer_name")),
                    Code = reader.GetString(reader.GetOrdinal("employer_code")),
                    LogoId = reader.GetInt32(reader.GetOrdinal("employer_logoId")),
                };
                employers.Add(employer);
            }
            return employers;
        }

        /// <summary>
        /// Extracts <seealso cref="IEmployerInfoDetail"/> model from <see cref="SqlDataReader"/>.
        /// </summary>
        /// <param name="reader">Param which contains entity of <seealso cref="IEmployerInfoDetail"/> model</param>
        /// <returns>Single object of <seealso cref="IEmployerInfoDetail"/> model</returns>
        private IEmployerInfoDetail ExtractEmployerDetail(SqlDataReader reader)
        {
            EmployerInfoDetail employer = null;

            if (!reader.HasRows)
            {
                return null;
            }

            if (reader.Read())
            {
                employer = new EmployerInfoDetail()
                {
                    EmployerName = reader.GetString(reader.GetOrdinal("Name")),
                    EmployerCode = reader.GetString(reader.GetOrdinal("Code")),
                    Street = reader.GetString(reader.GetOrdinal("Street")),
                    City = reader.GetString(reader.GetOrdinal("City")),
                    StateId = reader.GetByte(reader.GetOrdinal("StateId")),
                    ZipCode = reader.GetString(reader.GetOrdinal("ZipCode")),
                    Phone = reader.GetString(reader.GetOrdinal("Phone")),
                    AdminId = reader.GetInt32(reader.GetOrdinal("AdminId"))
                };
            }

            return employer;
        }

        /// <summary>
        /// Returns database null value if string is null or <seealso cref="string.Empty"/>, otherwise <seealso cref="str"/>
        /// </summary>
        /// <param name="str">The string to test.</param>
        /// <returns><seealso cref="DBNull.Value"/> if the value parameter is null or an empty string (""); otherwise, <seealso cref="str"/>.</returns>
        private object CheckStringParam(string str)
        {
            object temp;
            if (string.IsNullOrEmpty(str))
            {
                temp = DBNull.Value;
            }
            else
            {
                temp = str;
            } 

            return temp;
        }
    }
}