﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using WexHealth.Core.Repositories.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;
using WexHealth.ViewModels;

namespace WexHealth.Repositories
{
    public class ConsumerInfoRepository : IConsumerInfoRepository
    {
        private readonly DBCommandProvider _dbCommand;

        public ConsumerInfoRepository(string connectionString)
        {
            _dbCommand = new DBCommandProvider(connectionString);
        }

        /// <summary>
        /// Return ConsumerItem Collection
        /// </summary>
        public IEnumerable<IConsumerInfo> GetAll()
        {
            return _dbCommand.ExecuteCommandWithReturnValue(ExtractEntity, "usp_GetConsumers");
        }

        /// <summary>
        /// Returns IConsumerInfo collection which is filtered by params
        /// </summary>
        /// <param name="firstName">Represents first name of consumer in database</param>
        /// <param name="lastName">Represents last name of consumer in database</param>
        /// <param name="employerId">Represents id of an employer in database</param>
        /// <returns>Collection of consumers which filtered by employer's id, last or first name</returns>
        public IEnumerable<IConsumerWithEmployer> Find(string firstName, string lastName, int? employerId)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@first_name", Value = CheckStringParam(firstName)},
                new SqlParameter() {ParameterName = "@last_name", Value = CheckStringParam(lastName)},
                new SqlParameter() {ParameterName = "@employer_id", Value = CheckInt32Param(employerId)}
            };

            return _dbCommand.ExecuteCommandWithReturnValue(ExtractEntityConsumersWithEmployers, "usp_GetConsumerByFullName", sqlParams);
        }

        /// <summary>
        /// Returns Consumer Detail Info by Id
        /// </summary>
        /// <param name="consumerId">Represents Id of Consumer in database</param>
        /// <returns>Employer Detail Info <seealso cref="IConsumerInfoDetail"/></returns>
        public IConsumerInfoDetail GetConsumerById(int consumerId)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@participant_id", Value = consumerId}
            };

            return _dbCommand.ExecuteCommandWithReturnValue(ExtractEntityDetail, "usp_GetConsumerInfoById", sqlParams);
        }

        /// <summary>
        /// Returns Consumer Detail Info by Id
        /// </summary>
        /// <param name="consumerId">Represents Id of a consumer in database</param>
        /// <returns>Employer Detail Info <seealso cref="IConsumerInfoDetail"/></returns>
        public IConsumerWithEmployer GetConsumerShortInfoById(int consumerId)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@participant_id", Value = consumerId}
            };

            return _dbCommand.ExecuteCommandWithReturnValue(ExtractConsumerWithEmployer, "usp_GetConsumerByFullName", sqlParams);
        }
#warning Need to Add SaveChangesMethod
         
        #region Extractors entity

        private IEnumerable<IConsumerInfo> ExtractEntity(SqlDataReader reader)
        {
            if (!reader.HasRows)
            {
                return null;
            }

            var consumers = new List<IConsumerInfo>();
            while (reader.Read())
            {
                var consumer = new ConsumerInfo()
                {
                    ConsumerId = reader.GetInt32(reader.GetOrdinal("participant_id")),
                    FirstName = reader.GetString(reader.GetOrdinal("individual_firstName")),
                    LastName = reader.GetString(reader.GetOrdinal("individual_lastName")),
                    SSN = reader.GetString(reader.GetOrdinal("individual_ssn"))
                };
                consumers.Add(consumer);
            }
            return consumers;
        }

        private IEnumerable<IConsumerWithEmployer> ExtractEntityConsumersWithEmployers(SqlDataReader reader)
        {
            if (!reader.HasRows)
            {
                return null;
            }

            var consumers = new List<IConsumerWithEmployer>();
            for (var i = 0; i < reader.RecordsAffected; i++)
            {
                consumers.Add(ExtractConsumerWithEmployer(reader));
            }
            return consumers;
        }

        private IConsumerWithEmployer ExtractConsumerWithEmployer(SqlDataReader reader)
        {
            if (!reader.HasRows)
            {
                return null;
            }

            if (!reader.Read())
            {
                return null;
            }

            return new ConsumerWithEmployer()
            {
                ConsumerId = reader.GetInt32(reader.GetOrdinal("individual_id")),
                FirstName = reader.GetString(reader.GetOrdinal("individual_firstName")),
                LastName = reader.GetString(reader.GetOrdinal("individual_lastName")),
                SSN = reader.GetString(reader.GetOrdinal("individual_ssn")),
                EmployerId = reader.GetInt32(reader.GetOrdinal("employer_id")),

                EmployerName = reader.GetString(reader.GetOrdinal("employer_name")),
                EmployerCode = reader.GetString(reader.GetOrdinal("employer_code")),
                ParticipantId = reader.GetInt32(reader.GetOrdinal("participant_id"))
            };
        }

        private IConsumerInfoDetail ExtractEntityDetail(SqlDataReader reader)
        {
            ConsumerInfoDetail consumer = null;

            if (!reader.HasRows)
            {
                return null;
            }

            if (reader.Read())
            {
                consumer = new ConsumerInfoDetail
                {
                    ConsumerId = reader.GetInt32(reader.GetOrdinal("participant_id")),
                    FirstName = reader.GetString(reader.GetOrdinal("individual_firstName")),
                    LastName = reader.GetString(reader.GetOrdinal("individual_lastName")),
                    SSN = reader.GetString(reader.GetOrdinal("individual_ssn")),

                    Phone = reader.GetString(reader.GetOrdinal("phone_number")),

                    Street = reader.GetString(reader.GetOrdinal("address_street")),
                    City = reader.GetString(reader.GetOrdinal("address_city")),
                    ZipCode = reader.GetString(reader.GetOrdinal("address_zipCode")),
                    StateId = reader.GetByte(reader.GetOrdinal("state_id")),
                    Password = reader.GetString(reader.GetOrdinal("securityLogin_password")),
                    UserName = reader.GetString(reader.GetOrdinal("securityLogin_name"))
                };
            }

            return consumer;
        }

        #endregion

        /// <summary>
        /// Save <seealso cref="IConsumerInfoDetail"/> model in database
        /// </summary>
        /// <param name="cunsomer">instance of model wich will be represented to entity and saved in database</param>
        public void SaveConsumerInfoDetail(IConsumerInfoDetail cunsomer)
        {
            try
            {
                var sqlParams = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@participant_id", Value = cunsomer.ConsumerId},
                    new SqlParameter() {ParameterName = "@state_id", Value = cunsomer.StateId},
                    new SqlParameter() {ParameterName = "@first_name", Value = cunsomer.FirstName},
                    new SqlParameter() {ParameterName = "@last_name", Value = cunsomer.LastName},
                    new SqlParameter() {ParameterName = "@ssn", Value = cunsomer.SSN},
                    new SqlParameter() {ParameterName = "@phone", Value = cunsomer.Phone},
                    new SqlParameter() {ParameterName = "@street", Value = cunsomer.Street},
                    new SqlParameter() {ParameterName = "@city", Value = cunsomer.City},
                    new SqlParameter() {ParameterName = "@zip_code", Value = cunsomer.ZipCode},
                    new SqlParameter() {ParameterName = "@user_name", Value = cunsomer.UserName},
                    new SqlParameter() {ParameterName = "@password", Value = cunsomer.Password},
                };

                _dbCommand.ExecuteCommand("usp_InsertUpdateConsumerDetailedInfo", sqlParams);
            }
            catch (Exception e)
            {
                var exc = $"{e.Message}{Environment.NewLine}{e.InnerException}";
            }
        }

        /// <summary>
        /// Returns database null value if string is null or <seealso cref="string.Empty"/>, otherwise <seealso cref="str"/>
        /// </summary>
        /// <param name="str">The string to test.</param>
        /// <returns><seealso cref="DBNull.Value"/> if the value parameter is null or an empty string (""); otherwise, <seealso cref="str"/>.</returns>
        private object CheckStringParam(string str)
        {
            object temp;
            if (string.IsNullOrEmpty(str))
            {
                temp = DBNull.Value;
            }
            else
            {
                temp = str;
            }

            return temp;
        }

        /// <summary>
        /// Returns database null value if <seealso cref="identifier"/> is null, otherwise <seealso cref="identifier"/>
        /// </summary>
        /// <param name="identifier">The value to test.</param>
        /// <returns><seealso cref="DBNull.Value"/> if the value parameter is null or an empty string (""); otherwise, <seealso cref="str"/>.</returns>
        private object CheckInt32Param(int? identifier)
        {
            object temp;
            if (identifier is null)
            {
                temp = DBNull.Value;
            }
            else
            {
                temp = identifier;
            }

            return temp;
        }
    }
}