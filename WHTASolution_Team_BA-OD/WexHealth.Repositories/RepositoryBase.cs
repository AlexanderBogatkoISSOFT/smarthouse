﻿using WexHealth.Core.Repositories.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.Repositories
{
    public abstract class RepositoryBase<T> : IRepositoryBase
        where T : class, IDomainModel
    {
        protected readonly DBCommandProvider _commandProvider;

        protected RepositoryBase(string connectionString)
        {
            _commandProvider = new DBCommandProvider(connectionString);
        }

        protected T Get(string uspName, int id)
        {
            return _commandProvider.ExecuteCommandWithReturnValue<T>(null, uspName, null);
        }
    }
}