﻿using System.Collections.Generic;
using System.Data.SqlClient;
using WexHealth.Core.Repositories.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;
using WexHealth.ViewModels;

namespace WexHealth.Repositories
{
    public class AdminRepository : IAdminRepository
    {
        private readonly DBCommandProvider _dbCommand;

        public AdminRepository(string connectionString)
        {
            _dbCommand = new DBCommandProvider(connectionString);
        }

        /// <summary>
        /// Get detail admin Info by Id
        /// </summary>
        /// <param name="adminId"></param>
        /// <returns></returns>
        public IAdministratorDetailInfo GetAdminInfoById(int adminId)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@parAdminId", Value = adminId}
            };

            return _dbCommand.ExecuteCommandWithReturnValue(ExtractAdminInfoDetail, "usp_GetAdminDetailInfoById", sqlParams);
        }

        /// <summary>
        /// Create new Admin
        /// </summary>
        /// <param name="administratorDetailInfo"></param>
        public void CreateNewAdmin(IAdministratorDetailInfo administratorDetailInfo)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@parName", Value = administratorDetailInfo.Name},
                new SqlParameter() {ParameterName = "@parAlias", Value = administratorDetailInfo.Alias},
                new SqlParameter() {ParameterName = "@parStreet", Value = administratorDetailInfo.Street},
                new SqlParameter() {ParameterName = "@parCity", Value = administratorDetailInfo.City},
                new SqlParameter() {ParameterName = "@parStateId", Value = administratorDetailInfo.StateId},
                new SqlParameter() {ParameterName = "@parZipCode", Value = administratorDetailInfo.ZipCode},
                new SqlParameter() {ParameterName = "@parPhoneNumber", Value = administratorDetailInfo.Phone},

            };
            
            _dbCommand.ExecuteCommand("usp_InsertUpdateAdminInfo", sqlParams);
        }                                            
               
        /// <summary>
        /// Update exist admin info
        /// </summary>
        /// <param name="administratorDetailInfo"></param>
        public void UpdateAdmin(IAdministratorDetailInfo administratorDetailInfo)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@parAdminId", Value = administratorDetailInfo.AdminId},
                new SqlParameter() {ParameterName = "@parName", Value = administratorDetailInfo.Name},
                new SqlParameter() {ParameterName = "@parAlias", Value = administratorDetailInfo.Alias},
                new SqlParameter() {ParameterName = "@parStreet", Value = administratorDetailInfo.Street},
                new SqlParameter() {ParameterName = "@parCity", Value = administratorDetailInfo.City},
                new SqlParameter() {ParameterName = "@parStateId", Value = administratorDetailInfo.StateId},
                new SqlParameter() {ParameterName = "@parZipCode", Value = administratorDetailInfo.ZipCode},
                new SqlParameter() {ParameterName = "@parPhoneNumber", Value = administratorDetailInfo.Phone},

            };

            _dbCommand.ExecuteCommand("usp_InsertUpdateAdminInfo", sqlParams);
        }


        public void SaveChanges(IAdministratorDetailInfo administratorDetailInfo)
        {
            var sqlParams = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@parName", Value = administratorDetailInfo.Name},
                new SqlParameter() {ParameterName = "@parAlias", Value = administratorDetailInfo.Alias},
                new SqlParameter() {ParameterName = "@parStreet", Value = administratorDetailInfo.Street},
                new SqlParameter() {ParameterName = "@parCity", Value = administratorDetailInfo.City},
                new SqlParameter() {ParameterName = "@parStateId", Value = administratorDetailInfo.StateId},
                new SqlParameter() {ParameterName = "@parZipCode", Value = administratorDetailInfo.ZipCode},
                new SqlParameter() {ParameterName = "@parPhoneNumber", Value = administratorDetailInfo.Phone},
            };

            if (administratorDetailInfo.AdminId > 0)
            {
                sqlParams.Add(new SqlParameter() { ParameterName = "@parAdminId", Value = administratorDetailInfo.AdminId});
            }

            _dbCommand.ExecuteCommand("usp_InsertUpdateAdminInfo", sqlParams);
        }



        /// <summary>
        /// Get all admins
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IAdministratorInfo> GetAdmins()
        {
            return _dbCommand.ExecuteCommandWithReturnValue(ExtractAdminsInfo, "usp_GetAdmins");
        }

        /// <summary>
        /// Convert <see cref="SqlDataReader"/> Data to <see cref="IAdministratorDetailInfo"/>
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private IAdministratorDetailInfo ExtractAdminInfoDetail(SqlDataReader reader)
        {
            IAdministratorDetailInfo admin = null;

            if (!reader.HasRows)
            {
                return null;
            }

            if (reader.Read())
            {
                admin = new AdministratorDetailInfo()
                {
                    Name = reader.GetString(reader.GetOrdinal("Name")),
                    Alias = reader.GetString(reader.GetOrdinal("Alias")),
                    Street = reader.GetString(reader.GetOrdinal("Street")),
                    City = reader.GetString(reader.GetOrdinal("City")),
                    StateId = reader.GetByte(reader.GetOrdinal("StateId")),
                    ZipCode = reader.GetString(reader.GetOrdinal("ZipCode")),
                    Phone = reader.GetString(reader.GetOrdinal("Phone"))
                };
            }

            return admin;
        }

        /// <summary>
        /// Convert SqlDataReader to List <see cref="IAdministratorDetailInfo"/>
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private IEnumerable<IAdministratorInfo> ExtractAdminsInfo(SqlDataReader reader)
        {
            if (!reader.HasRows)
            {
                return null;
            }

            var admins = new List<IAdministratorInfo>();
            while (reader.Read())
            {
                var admin = new AdministratorInfo()
                {
                    AdminId = reader.GetInt32(reader.GetOrdinal("AdminId")),
                    Name = reader.GetString(reader.GetOrdinal("Name")),
                    Alias = reader.GetString(reader.GetOrdinal("Alias")),
                };
                admins.Add(admin);
            }
            return admins;
        }
    }
}