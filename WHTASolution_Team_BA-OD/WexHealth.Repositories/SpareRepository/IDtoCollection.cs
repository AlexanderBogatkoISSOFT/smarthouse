﻿using System.Collections.Generic;

namespace WexHealth.Repositories.SpareRepository
{
    /// <summary>
    /// General method set that every DTOCollection must have
    /// </summary>
    /// <typeparam name="T">Type of model that represents entity in database</typeparam>
    public interface IDtoCollection<T>
        where T : class
    {
        /// <summary>
        /// Returns all entities from database
        /// </summary>
        IEnumerable<T> GetAll();
    }
}