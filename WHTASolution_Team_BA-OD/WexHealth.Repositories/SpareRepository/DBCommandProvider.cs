﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace WexHealth.Repositories.SpareRepository
{
    /// <summary>
    /// Provides method set to execute sql commands.
    /// </summary>
    /// <typeparam name="T">Type of model that represents entity in database</typeparam>
    [Obsolete("Use DBCommand")]
    public class DBCommandProvider<T>
        where T : class
    {
        private readonly SqlConnection _connection;

        public Func<SqlDataReader, T> ExtractEntityMethod { get; set; }

        /// <summary>
        /// Creates new abstract repository with connection to database.
        /// </summary>
        /// <param name="connectionString">Special <seealso cref="string"/> variable that represents set of key-value pairs that are formed a connection to database.</param>
        public DBCommandProvider(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

            _connection = new SqlConnection(connectionString);
        }

        /// <summary>
        /// Executes <seealso cref="SqlCommand"/> that represents sql procedure with given <paramref name="procedureName"/>.
        /// </summary>
        /// <param name="procedureName">Full name (with schema) of storage procedure in database.</param>
        /// <param name="parameters">Parameters of called procedure.</param>
        public void ExecuteCommand(string procedureName, IEnumerable<SqlParameter> parameters = null)
        {
            using (var command = new SqlCommand(procedureName, _connection) { CommandType = CommandType.StoredProcedure })
            {
                _connection.Open();

                if (parameters != null && parameters.Count() > 0)
                {
                    foreach (var item in parameters)
                    {
                        command.Parameters.Add(item);
                    }
                }

                try
                {
                    command.ExecuteNonQuery();
                }
                finally
                {
                    _connection.Close();
                }
            }
        }

        /// <summary>
        /// Executes <seealso cref="SqlCommand"/> that represents sql procedure with given <paramref name="procedureName"/> and returns entity which extracting in <see cref="ExtractEntity"/>.
        /// </summary>
        /// <param name="procedureName">Full name (with schema) of storage procedure in database.</param>
        /// <param name="parameters">Parameters of called procedure.</param>
        /// <param name="ExtractEntity">Extracts entity from <see cref="SqlDataReader"/>.</param>
        /// <returns>Model if extracting was completed successfully.</returns>
        public T ExecuteCommandWithReturnValue(string procedureName, IEnumerable<SqlParameter> parameters = null)
        {
            if (ExtractEntityMethod is null)
            {
                throw new ArgumentNullException(nameof(ExtractEntityMethod));
            }

            using (var command = new SqlCommand(procedureName, _connection) { CommandType = CommandType.StoredProcedure })
            {
                _connection.Open();

                if (parameters != null && parameters.Count() > 0)
                {
                    foreach (var item in parameters)
                    {
                        command.Parameters.Add(item);
                    }
                }

                try
                {
                    var reader = command.ExecuteReader();
                    return ExtractEntityMethod.Invoke(reader);
                }

                finally
                {
                    _connection.Close();
                }
            }
        }

        public IEnumerable<T> ExecuteCommandWithReturnValues(string procedureName, IEnumerable<SqlParameter> parameters = null)
        {
            if (ExtractEntityMethod is null)
            {
                throw new ArgumentNullException(nameof(ExtractEntities));
            }

            using (var command = new SqlCommand(procedureName, _connection) { CommandType = CommandType.StoredProcedure })
            {
                _connection.Open();

                if (parameters != null && parameters.Count() > 0)
                {
                    foreach (var item in parameters)
                    {
                        command.Parameters.Add(item);
                    }
                }

                try
                {
                    var reader = command.ExecuteReader();
                    return ExtractEntities(reader);
                }

                finally
                {
                    _connection.Close();
                }
            }
        }

        public IEnumerable<T> ExtractEntities(SqlDataReader reader)
        {
            var entities = new List<T>();

            if (!reader.HasRows)
            {
                return entities;
            }

            while (reader.Read())
            {
                var entity = ExtractEntityMethod(reader);
                entities.Add(entity);
            }

            return entities;
        }
    }
}