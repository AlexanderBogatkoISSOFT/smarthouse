﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using WexHealth.Core.Repositories.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.Repositories.SpareRepository
{
    [Obsolete("Use another realisation")]
    public abstract class RepositoryBase<TDomainModel> :  IRepositoryBase
        where TDomainModel : class, IDomainModel
    {
        protected readonly DBCommandProvider<TDomainModel> _commandProvider;

        protected RepositoryBase(string connectionString)
        {
            _commandProvider = new DBCommandProvider<TDomainModel>(connectionString)
            {
                ExtractEntityMethod = ExtractEntity
            };
        }

        protected TDomainModel Get(string uspName, int id)
        {
            return _commandProvider.ExecuteCommandWithReturnValue(uspName, null);
        }

        protected IEnumerable<TDomainModel> GetAll(string uspName)
        {
            return _commandProvider.ExecuteCommandWithReturnValues(uspName);
        }

        public abstract TDomainModel ExtractEntity(SqlDataReader reader);
    }
}