﻿using System.Collections.Generic;
using System.Data.SqlClient;
using WexHealth.Core.Models.Interfaces;
using WexHealth.Core.Repositories.Interfaces;
using WexHealth.Models;

namespace WexHealth.Repositories
{
    /// <summary>
    /// Represents a set of methods for interacting with <seealso cref="State" objects/> & <seealso cref="Address" objects/> models in database.
    /// </summary>
    public class AddressRepository : IAddressRepository
    {
        private readonly DBCommandProvider _dbCommand;

        /// <summary>
        /// Creates new instance of repository for interraction with <seealso cref="State" objects/> & <seealso cref="Address" objects/> models.
        /// </summary>
        /// <param name="connectionString">Special <seealso cref="string"/> variable that represents set of key-value pairs that are formed a connection to database.</param>
        public AddressRepository(string connectionString)
        {
            _dbCommand = new DBCommandProvider(connectionString);
        }

        /// <summary>
        /// Returns IStates collection
        /// </summary>
        /// <returns>collection of <seealso cref="IState"/></returns>
        public IEnumerable<IState> GetAllStates()
        {
            return _dbCommand.ExecuteCommandWithReturnValue(ExtractEntity, "usp_GetStates");
        }

        /// <summary>
        /// Extracts <seealso cref="State"/> model from <see cref="SqlDataReader"/>.
        /// </summary>
        /// <param name="reader">Param which contains entity of <seealso cref="State"/> model</param>
        /// <returns>Collection of <seealso cref="State"/> model</returns>
        private IEnumerable<IState> ExtractEntity(SqlDataReader reader)
        {
            if (!reader.HasRows)
            {
                return null;
            }

            var states = new List<IState>();
            while (reader.Read())
            {
                var state = new State()
                {
                    StateId = reader.GetByte(reader.GetOrdinal("state_id")),
                    Name = reader.GetString(reader.GetOrdinal("state_name")),
                    ShortName = reader.GetString(reader.GetOrdinal("state_shortName"))
                };
                states.Add(state);
            }
            return states;
        }
    }
}
