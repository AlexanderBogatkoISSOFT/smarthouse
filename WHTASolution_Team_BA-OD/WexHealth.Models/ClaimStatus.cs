﻿using System;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class ClaimStatus : IClaimStatus
    {
        public long ClaimStatusId { get; set; }
        public byte Status { get; set; }
        public string Notes { get; set; }
        public DateTime Date { get; set; }
    }
}