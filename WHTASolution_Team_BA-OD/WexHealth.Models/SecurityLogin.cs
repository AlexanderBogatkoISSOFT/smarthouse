﻿using System;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class SecurityLogin : ISecurityLogin
    {
        public int LoginId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int IndividualId { get; set; }
        public byte LoginType { get; set; }
        public int EmployerId { get; set; }
        public short DomainId { get; set; }
        public DateTime ChangeDate { get; set; }
    }
}