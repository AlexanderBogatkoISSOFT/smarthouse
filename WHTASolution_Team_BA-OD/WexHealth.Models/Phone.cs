﻿using System;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class Phone : IPhone
    {
        public long PhoneId { get; set; }
        public string Number { get; set; }
        public DateTime ChangeDate { get; set; }
    }
}