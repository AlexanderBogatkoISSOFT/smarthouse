﻿using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class State : IState
    {
        public byte StateId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
    }
}
