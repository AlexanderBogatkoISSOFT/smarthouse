﻿using System;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class Receipt : IReceipt
    {
        public long ReceiptId { get; set; }
        public byte[] Data { get; set; }
        public DateTime ChangeDate { get; set; }
    }
}