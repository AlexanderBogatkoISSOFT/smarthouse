﻿using System;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class Setting : ISetting
    {
        public int SettingId { get; set; }
        public string Name { get; set; }
        public string DefaultValue { get; set; }
        public string ValueType { get; set; }
        public short SettingCategory { get; set; }
        public DateTime ChangeDate { get; set; }
    }
}