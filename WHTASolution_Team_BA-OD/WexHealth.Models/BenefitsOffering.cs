﻿using System;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class BenefitsOffering : IBenefitsOffering
    {
        public int BenefitsOfferingId { get; set; }
        public string Name { get; set; }
        public byte Plan { get; set; }
        public decimal ContributionAmount { get; set; }
        public int BenefitsPackageId { get; set; }
        public DateTime ChangeDate { get; set; }
    }
}