﻿using System;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class Address : IAddress
    {
        public long AddressId { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string ZipCode { get; set; }
        public byte StateId { get; set; }
        public byte AddressType { get; set; }
        public DateTime ChangeDate { get; set; }
    }
}