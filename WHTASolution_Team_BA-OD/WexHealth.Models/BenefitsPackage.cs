﻿using System;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class BenefitsPackage : IBenefitsPackage
    {
        public int BenefitsPackageId { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public byte PayrollFrequency { get; set; }
        public int PackageStatusId { get; set; }
        public int EmployerId { get; set; }
        public DateTime ChangeDate { get; set; }
    }
}