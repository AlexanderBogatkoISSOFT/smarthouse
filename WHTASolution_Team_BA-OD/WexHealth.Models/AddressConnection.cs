﻿using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class AddressConnection : IAddressConnection
    {
        public long AddressId { get; set; }
        public byte StateId { get; set; }
        public int ObjectId { get; set; }
        public byte ObjectType { get; set; }
    }
}