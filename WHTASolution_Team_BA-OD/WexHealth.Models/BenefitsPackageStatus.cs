﻿using System;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class BenefitsPackageStatus : IBenefitsPackageStatus
    {
        public int PackageStatusId { get; set; }
        public DateTime Date { get; set; }
    }
}