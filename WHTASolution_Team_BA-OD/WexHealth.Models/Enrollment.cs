﻿using System;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class Enrollment : IEnrollment
    {
        public int EnrollmentId { get; set; }
        public decimal ElectionAmount { get; set; }
        public decimal ContributionAmount { get; set; }
        public int ParticipantId { get; set; }
        public int BenefitsOfferingId { get; set; }
        public DateTime ChangeDate { get; set; }
    }
}