﻿using System;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class Individual : IIndividual
    {
        public int IndividualId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime? Birthdate { get; set; }
        public byte LoginType { get; set; }
        public string Ssn { get; set; }
        public DateTime ChangeDate { get; set; }
    }
}