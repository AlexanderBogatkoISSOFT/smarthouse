﻿using System;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class ImportEnrollment : IImportEnrollment
    {
        public int ImportEnrollmentId { get; set; }
        public int EmployerId { get; set; }
        public int ImportEnrollmentStatusId { get; set; }
        public int InitialFileName { get; set; }
        public string Error { get; set; }
        public DateTime ChangeDate { get; set; }
    }
}