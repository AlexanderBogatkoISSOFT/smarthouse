﻿using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class Participant : IParticipant
    {
        public int ParticipantId { get; set; }
        public int IndividualId { get; set; }
        public int EmployerId { get; set; }
    }
}