﻿using System;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class Domain : IDomain
    {
        public short DomainId { get; set; }
        public string Name { get; set; }
        public DateTime ChangeDate { get; set; }
    }
}