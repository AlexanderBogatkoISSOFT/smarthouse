﻿using System;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class Claim : IClaim
    {
        public long ClaimId { get; set; }
        public DateTime DateOfService { get; set; }
        public string ClaimNumber { get; set; }
        public decimal Amount { get; set; }
        public int ParticipantId { get; set; }
        public int BenefitsOfferingId { get; set; }
        public long ClaimStatusId { get; set; }
        public long ReceiptId { get; set; }
        public DateTime ChangeDate { get; set; }
    }
}