﻿using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class SettingConnection : ISettingConnection
    {
        public int SettingConnectionId { get; set; }
        public string Value { get; set; }
        public int SettingId { get; set; }
    }
}