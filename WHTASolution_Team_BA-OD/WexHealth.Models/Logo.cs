﻿using System;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class Logo : ILogo
    {
        public int LogoId { get; set; }
        public byte[] Data  { get; set; }
        public DateTime ChangeDate { get; set; }
    }
}