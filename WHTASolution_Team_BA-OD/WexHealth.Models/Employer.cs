﻿using System;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class Employer : IEmployer
    {
        public int EmployerId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int LogoId { get; set; }
        public DateTime ChangeDate { get; set; }
    }
}