﻿using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class PhoneConnection : IPhoneConnection
    {
        public long PhoneId { get; set; }
        public byte PhoneType { get; set; }
        public int Object { get; set; }
        public byte ObjectType { get; set; }
    }
}