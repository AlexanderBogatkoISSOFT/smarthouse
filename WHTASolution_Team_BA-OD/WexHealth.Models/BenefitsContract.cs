﻿using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class BenefitsContract : IBenefitsContract
    {
        public int EmployerId { get; set; }
        public int AdministratorId { get; set; }
    }
}