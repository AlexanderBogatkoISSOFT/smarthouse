﻿using System;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class ImportEnrollmentStatus : IImportEnrollmentStatus
    {
        public int ImportEnrollmentStatusId { get; set; }
        public byte Status { get; set; }
        public DateTime Date { get; set; }
    }
}