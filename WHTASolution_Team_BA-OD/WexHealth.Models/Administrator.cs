﻿using System;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Models
{
    public class Administrator : IAdministrator
    {
        public int AdministratorId { get; set; }
        public string Name { get; set; }
        public short DomainId { get; set; }
        public DateTime ChangeDate  { get; set; }
    }
}