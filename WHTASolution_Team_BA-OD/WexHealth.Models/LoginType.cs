﻿namespace WexHealth.Models
{
    public enum LoginType
    {
        Administrator,
        Employer,
        Participant
    }
}