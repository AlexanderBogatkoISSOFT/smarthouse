﻿namespace WexHealth.IDomainModels
{
    public interface IPersonInfo : IDomainModel
    {
        string Street { get; set; }
        string City { get; set; }
        string State { get; set; }
        string ZipCode { get; set; }
        string Phone { get; set; }
    }
}