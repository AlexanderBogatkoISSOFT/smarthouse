﻿namespace WexHealth.IDomainModels
{
    public interface IEnrollmentInfo : IDomainModel
    {
        int PlanId { get; set; }
        string Plan { get; set; }
        int Election { get; set; }
        int Contribution { get; set; }
    }
}