﻿namespace WexHealth.IDomainModels
{
    public interface IEmployerInfo : IDomainModel
    {
        int EmployerId { get; set; }
        string EmployerName { get; set; }
        string EmployerCode { get; set; }
    }
}