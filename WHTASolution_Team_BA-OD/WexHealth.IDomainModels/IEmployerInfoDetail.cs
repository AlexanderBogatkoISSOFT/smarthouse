﻿namespace WexHealth.IDomainModels
{
    public interface IEmployerInfoDetail : IEmployerInfo, IPersonInfo
    {
        byte[] Logo { get; set; }
    }
}