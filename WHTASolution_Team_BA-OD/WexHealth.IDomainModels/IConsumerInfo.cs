﻿namespace WexHealth.IDomainModels
{
    public interface IConsumerInfo : IDomainModel
    {
        int ConsumerId { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string SSN { get; set; }
    }
}