﻿using System;

namespace WexHealth.IDomainModels
{
    public interface IClaimInfo : IDomainModel
    {
        int ClaimId { get; set; }
        int ConsumerId { get; set; }
        int EmployerId { get; set; }
        string ClaimNumber { get; set; }
        string ConsumerName { get; set; }
        string EmployerName { get; set; }
        string ConsumerPhoneNumber { get; set; }
        DateTime DateOfService { get; set; }
        int Plan { get; set; }
        int Amount { get; set; }
    }
}