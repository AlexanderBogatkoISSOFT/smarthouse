﻿namespace WexHealth.IDomainModels
{
    public interface IConsumerInfoDetail : IConsumerInfo, IPersonInfo
    {
        string UserName { get; set; }
        string Password { get; set; }
        string EmployerInfo { get; set; }
    }
}