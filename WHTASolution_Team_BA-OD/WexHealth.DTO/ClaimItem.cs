﻿using System;

namespace WexHealth.DTO
{
    public class ClaimItem
    {
        public int ClaimId { get; set; }
        public int ConsumerId { get; set; }
        public int EmployerId { get; set; }
        public string ClaimNumber { get; set; }
        public string ConsumerName { get; set; }
        public string EmployerName { get; set; }
        public DateTime DateOfService { get; set; }
        public string Plan { get; set; }
        public int Amount { get; set; }
    }
}
