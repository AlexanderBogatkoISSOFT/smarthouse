﻿namespace WexHealth.DTO
{
    public class ConsumerItem
    {
        public int ParticipantId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string SSN { get; set; }
    }
}