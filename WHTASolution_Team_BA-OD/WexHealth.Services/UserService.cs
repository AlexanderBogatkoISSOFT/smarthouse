﻿using System.Collections.Generic;
using WexHealth.Core.Repositories.Interfaces;
using WexHealth.Core.Services.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public IEnumerable<IUserInfo> GetUsers(int employerId)
        {
            return _userRepository.GetUsersByAdminId(employerId);
        }

#warning Get users by empId & AdminId

        public void AddNewUser(IUserInfo userInfo, int loginType)
        {
            _userRepository.InsertUser(userInfo, loginType);
        }

        public void UpdateUser(IUserInfo userInfo)
        {
            _userRepository.UpdateUser(userInfo);
        }
    }
}