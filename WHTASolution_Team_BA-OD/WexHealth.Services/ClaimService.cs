﻿using System;
using System.Collections.Generic;
using WexHealth.Core.Models.Interfaces;
using WexHealth.Core.Repositories.Interfaces;
using WexHealth.Core.Services.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.Services
{
    /// <summary>
    /// Represents service for claims
    /// </summary>
    public class ClaimService : IClaimService
    {
        private readonly IClaimInfoRepository _claimInfoRepository;
        private readonly IEmployerRepository _employerRepository;

        /// <summary>
        /// Creates instance of Claim service.
        /// </summary>
        /// <param name="claimInfoRepository">Name of repository</param>
        /// <param name="employerRepository">Name of repository</param>
        public ClaimService(IClaimInfoRepository claimInfoRepository, IEmployerRepository employerRepository)
        {
            _claimInfoRepository = claimInfoRepository;
            _employerRepository = employerRepository;
        }

        /// <summary>
        /// Gets list of claims
        /// </summary>
        /// <returns>Collection of <seealso cref="IClaimInfo"/></returns>
        public IEnumerable<IClaimInfo> GetAll()
        {
            return _claimInfoRepository.GetAll();
        }

        /// <summary>
        /// Returns ClaimInfo collection which is filtered by params
        /// </summary>
        /// <param name="claimNumber">will find by claim number</param>
        /// <param name="employerId">will find by employer Id</param>
        /// <returns>Collection of claims which filtered by claim number name or employer Id</returns>
        public IEnumerable<IClaimInfo> Find(string claimNumber, int? employerId)
        {
            return _claimInfoRepository.Find(claimNumber, employerId);
        }

        public IEnumerable<IEmployer> GetEmployers()
        {
            return _employerRepository.GetAll();
        }

        public IClaimInfo GetClaimInfoById(int claimId)
        {
            return _claimInfoRepository.GetClaimInfoById(claimId);
        }

        public void UpdateClaim(int claimId, DateTime? dateOfService, decimal? amount, byte? plan)
        {
            _claimInfoRepository.UpdateClaim(claimId, dateOfService, amount, plan);
        }
    }
}