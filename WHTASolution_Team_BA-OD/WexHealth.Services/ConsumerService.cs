﻿using System;
using System.Collections.Generic;
using WexHealth.Core.Models.Interfaces;
using WexHealth.Core.Repositories.Interfaces;
using WexHealth.Core.Services.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.Services
{
    /// <summary>
    /// Represents service for consumers
    /// </summary>
    public class ConsumerService : IConsumerService
    {
        private readonly IConsumerInfoRepository _consumerInfoRepository;
        private readonly IEmployerRepository _employerRepository;
        private readonly IAddressRepository _addressRepository;

        /// <summary>
        /// Creates instance of Consumer service.
        /// </summary>
        /// <param name="consumerInfoRepository">Name of repository</param>
        public ConsumerService(IConsumerInfoRepository consumerInfoRepository, IEmployerRepository employerRepository, IAddressRepository addressRepository)
        {
            _consumerInfoRepository = consumerInfoRepository;
            _employerRepository = employerRepository;
            _addressRepository = addressRepository;
        }

        /// <summary>
        /// Gets list of consumers
        /// </summary>
        /// <returns>Collection of <seealso cref="IConsumerInfo"/></returns>
        public IEnumerable<IConsumerInfo> GetAll()
        {
            return _consumerInfoRepository.GetAll();
        }

        public IEnumerable<IEmployer> GetEmployers()
        {
            return _employerRepository.GetAll();
        }

        public IEnumerable<IConsumerInfo> Find(string firstName = null, string lastName = null, int? employerId = null)
        {
            return _consumerInfoRepository.Find(firstName?.Trim(), lastName?.Trim(), employerId);
        }

        public IConsumerInfoDetail GetConsumerById(int consumerId)
        {
            return consumerId >= 0 ? _consumerInfoRepository.GetConsumerById(consumerId) : null;
        }

        public IConsumerWithEmployer GetConsumerShortInfoById(int consumerId)
        {
            return consumerId >= 0 ? _consumerInfoRepository.GetConsumerShortInfoById(consumerId) : null;
        }

        public IEnumerable<IState> GetAllStates()
        {
            return _addressRepository.GetAllStates();
        }

        public void SaveConsumerInfoDetail(IConsumerInfoDetail cunsomer)
        {
            try
            {
                _consumerInfoRepository.GetAll();
            }
            catch (Exception e)
            {
                var exc = $"{e.Message}{Environment.NewLine}{e.InnerException}";
            }
        }
    }
}