﻿using System.Collections.Generic;
using WexHealth.Core.Models.Interfaces;
using WexHealth.Core.Repositories.Interfaces;
using WexHealth.Core.Services.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.Services
{
    /// <summary>
    /// Represents service for employers
    /// </summary>
    public class EmployerService : IEmployerService
    {
        private readonly IEmployerRepository _employerRepository;

        /// <summary>
        /// Creates instance of Employer service.
        /// </summary>
        /// <param name="employerRepository">Name of repository</param>
        public EmployerService(IEmployerRepository employerRepository)
        {
            _employerRepository = employerRepository;
        }

        /// <summary>
        /// Returns Employers collection which is filtered by params
        /// </summary>
        /// <param name="employerName">will find by name</param>
        /// <param name="employerCode">will find by code (short name)</param>
        /// <returns>Collection of employers which filtered by employer's name or code</returns>
        public IEnumerable<IEmployer> Find(string employerName = null, string employerCode = null)
        {
            return _employerRepository.Find(employerName?.Trim(), employerCode?.Trim());
        }

        /// <summary>
        /// Gets list of employers
        /// </summary>
        /// <returns>Collection of <seealso cref="IEmployer"/></returns>
        public IEnumerable<IEmployer> GetAll()
        {
            return _employerRepository.GetAll();
        }

        /// <summary>
        /// Gets Detail Employer Info
        /// </summary>
        /// <returns>Single object<seealso cref="IEmployerInfoDetail"/></returns>
        public IEmployerInfoDetail GetEmployerDetailById(int employerId)
        {
            return _employerRepository.GetEmployerDetailById(employerId);
        }

        /// <summary>
        /// Add new employer
        /// </summary>
        /// <param name="employerInfoDetail"></param>
        public void AddNewEmployer(IEmployerInfoDetail employerInfoDetail)
        {
            _employerRepository.AddNewEmployer(employerInfoDetail);
        }

        /// <summary>
        /// Update exist employer
        /// </summary>
        /// <param name="employerInfoDetail"></param>
        public void UpdateEmployer(IEmployerInfoDetail employerInfoDetail)
        {
            _employerRepository.UpdateEmployer(employerInfoDetail);
        }

        /// <summary>
        /// Insert new Employer or Update exist employer
        /// </summary>
        /// <param name="employerInfoDetail"></param>
        public void SaveChanges(IEmployerInfoDetail employerInfoDetail)
        {
            _employerRepository.SaveChanges(employerInfoDetail);
        }
    }
}