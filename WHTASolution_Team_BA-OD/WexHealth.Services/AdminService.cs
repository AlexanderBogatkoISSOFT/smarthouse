﻿using System.Collections.Generic;
using WexHealth.Core.Models.Interfaces;
using WexHealth.Core.Repositories.Interfaces;
using WexHealth.Core.Services.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.Services
{
    public class AdminService : IAdminService
    {
        private readonly IAdminRepository _adminRepository;
        private readonly IAddressRepository _addressRepository;

        public AdminService(IAdminRepository adminRepository, IAddressRepository addressRepository)
        {
            _adminRepository = adminRepository;
            _addressRepository = addressRepository;
        }

        public IEnumerable<IState> GetStates()
        {
            return _addressRepository.GetAllStates();
        }

        public IAdministratorDetailInfo GetAdminInfoById(int adminId)
        {
            return _adminRepository.GetAdminInfoById(adminId);
        }

        public void CreateNewAdmin(IAdministratorDetailInfo administratorDetailInfo)
        {
            _adminRepository.CreateNewAdmin(administratorDetailInfo);
        }

        public void UpdateAdmin(IAdministratorDetailInfo administratorDetailInfo)
        {
            _adminRepository.UpdateAdmin(administratorDetailInfo);
        }

        public IEnumerable<IAdministratorInfo> GetAdmins()
        {
            return _adminRepository.GetAdmins();
        }

        public void SaveChanges(IAdministratorDetailInfo administratorDetailInfo)
        {
            _adminRepository.SaveChanges(administratorDetailInfo);
        }
    }
}