﻿INSERT dbo.Logo(data , change_date)
VALUES 
       (CAST(123456789101112 AS VARBINARY(MAX)), GETDATE()),
       (CAST(234567891011121 AS VARBINARY(MAX)), GETDATE()),
       (CAST(345678910111212 AS VARBINARY(MAX)), GETDATE()),
       (CAST(456789101112123 AS VARBINARY(MAX)), GETDATE()),
       (CAST(567891011121234 AS VARBINARY(MAX)), GETDATE()),
       (CAST(678910111212345 AS VARBINARY(MAX)), GETDATE()),
       (CAST(789101112123456 AS VARBINARY(MAX)), GETDATE()),
       (CAST(891011121234567 AS VARBINARY(MAX)), GETDATE()),
       (CAST(910111212345678 AS VARBINARY(MAX)), GETDATE()),
       (CAST(101112123456789 AS VARBINARY(MAX)), GETDATE()),
       (CAST(011121234567891 AS VARBINARY(MAX)), GETDATE()),
       (CAST(111212345678910 AS VARBINARY(MAX)), GETDATE()),
       (CAST(112123456789101 AS VARBINARY(MAX)), GETDATE()),
       (CAST(121234567891011 AS VARBINARY(MAX)), GETDATE()),
       (CAST(212345678910111 AS VARBINARY(MAX)), GETDATE())

GO

INSERT dbo.Employer(name, code, logo_id, change_date)
VALUES 
       ('Amato''s',                         'AMATO',     1,  GETDATE()),
       ('American Poolplayers Association', 'AMS',       2,  GETDATE()),
       ('Amerihost',                        'AHOST',     3,  GETDATE()),
       ('Ampm',                             'AMPM',      4,  GETDATE()),
       ('Applebee''s',                      'APPLEBEES', 5,  GETDATE()),
       ('Arby''s',                          'ARBYS',     6,  GETDATE()),
       ('Assist-2-Sell',                    'AS2',       7,  GETDATE()),
       ('The Athlete''s Foot',              'TAF',       8,  GETDATE()),
       ('Athletic Nation',                  'NATION',    9,  GETDATE()),
       ('Atlanta Bread Company',            'ABC',       10, GETDATE()),
       ('Auntie Anne''s',                   'AAS',       11, GETDATE()),
       ('Coherent Solutions',               'CS',        12, GETDATE()),
       ('IsSoft',                           'IS',        13, GETDATE()),
       ('WEX',                              'WEX',       14, GETDATE())

GO

INSERT dbo.BenefitsPackageStatus(status, date)
VALUES
	   (1,  GETDATE()),
	   (2,  GETDATE()),
	   (3,  GETDATE()),
	   (4,  GETDATE()),
	   (5,  GETDATE()),
	   (6,  GETDATE()),
	   (7,  GETDATE()),
	   (8,  GETDATE()),
	   (9,  GETDATE()),
	   (10,	GETDATE())

GO

INSERT dbo.BenefitsPackage(name, start_date, end_date, payroll_frequency, package_status_id, employer_id, change_date)
VALUES
	   ('BenefitPackageName-1',	 GETDATE(), GETDATE(), 1,	1,	1,	GETDATE()),
	   ('BenefitPackageName-2',	 GETDATE(), GETDATE(), 2,	2,	2,	GETDATE()),
	   ('BenefitPackageName-3',	 GETDATE(), GETDATE(), 3,	3,	3,	GETDATE()),
	   ('BenefitPackageName-4',	 GETDATE(), GETDATE(), 4,	4,	4,	GETDATE()),
	   ('BenefitPackageName-5',	 GETDATE(), GETDATE(), 5,	5,	5,	GETDATE()),
	   ('BenefitPackageName-6',	 GETDATE(), GETDATE(), 6,	6,	6,	GETDATE()),
	   ('BenefitPackageName-7',	 GETDATE(), GETDATE(), 7,	7,	7,	GETDATE()),
	   ('BenefitPackageName-8',	 GETDATE(), GETDATE(), 8,	8,	8,	GETDATE()),
	   ('BenefitPackageName-9',	 GETDATE(), GETDATE(), 9,	9,	9,	GETDATE()),
	   ('BenefitPackageName-10', GETDATE(), GETDATE(), 10,	10, 10,	GETDATE())

GO


INSERT dbo.BenefitsOffering(name, [plan], contribution_amount, benefits_package_id, change_date)
VALUES
		('BenefitsOfferingName-1',	1,	110,	1,	GETDATE()),
		('BenefitsOfferingName-2',	2,	120,	2,	GETDATE()),
		('BenefitsOfferingName-3',	3,	130,	3,	GETDATE()),
		('BenefitsOfferingName-4',	1,	110,	4,	GETDATE()),
		('BenefitsOfferingName-5',	2,	120,	5,	GETDATE()),
		('BenefitsOfferingName-6',	3,	130,	6,	GETDATE()),
		('BenefitsOfferingName-7',	1,	110,	7,	GETDATE()),
		('BenefitsOfferingName-8',	2,	120,	8,	GETDATE()),
		('BenefitsOfferingName-9',	3,	130,	9,	GETDATE())

GO


INSERT dbo.Individual(first_name, last_name, email, birthdate, ssn, change_date)
VALUES 
       ('Sergey',    'Babitskiy',   'BabitskiySergey@example.com',   '1970-01-01', '123456789', GETDATE()),
       ('Slava',     'Leventuyev',  'LeventuyevSlava@example.com',   '1970-01-01', '987654321', GETDATE()),
       ('Sergey',    'Gorbaba',     'GorbabaSergey@example.com',     '1970-01-01', '741258963', GETDATE()),
       ('Dmitry',    'Olekhnovich', 'OlekhnovichDmitry@example.com', '1970-01-01', '123456789', GETDATE()),
       ('Alexander', 'Bogatko',     'BogatkoAlexander@example.com',  '1970-01-01', '123456789', GETDATE())

GO

INSERT dbo.Participant(individual_id, employer_id)
VALUES
       (1, 1),
       (2, 2),
       (3, 3),
       (4, 4),
       (5, 5)

GO


INSERT dbo.Enrollment(election_amount, contribution_amount, participant_id, benefits_offering_id, change_date)
VALUES
	(1000,	2000,	1,	1,	GETDATE()),
	(1000,	2000,	2,	2,	GETDATE()),
	(1000,	2000,	3,	3,	GETDATE()),
	(1000,	2000,	4,	4,	GETDATE()),
	(1000,	2000,	5,	5,	GETDATE()),
	(1000,	2000,	3,	6,	GETDATE()),
	(1000,	2000,	2,	7,	GETDATE()),
	(1000,	2000,	4,	8,	GETDATE()),
	(1000,	2000,	1,	9,	GETDATE())


GO


INSERT dbo.ClaimStatus(status, notes, date)
VALUES
	(0, 'Pending',	GETDATE()),
	(1,	'Approved',	GETDATE()),
	(2,	'Denied',	GETDATE())

GO

INSERT dbo.Receipt(data, change_date)
VALUES
	(CAST(891011121234567 AS VARBINARY(MAX)), GETDATE()),
	(CAST(891011121234567 AS VARBINARY(MAX)), GETDATE())

GO


INSERT dbo.Claim(claim_number, amount, participant_id, benefits_offering_id, claim_status_id, receipt_id, date_of_service, change_date)
VALUES
	('1234537896',	3100,	1,	1,	1,	1,	GETDATE(), GETDATE()),
	('2234527895',	3200,	2,	2,	2,	2,	GETDATE(), GETDATE()),
	('3234517894',	3300,	3,	3,	3,	1,	GETDATE(), GETDATE()),
	('4234537893',	3400,	4,	4,	1,	2,	GETDATE(), GETDATE()),
	('5234527892',	3500,	5,	5,	2,	1,	GETDATE(), GETDATE()),
	('6234517891',	3600,	1,	6,	3,	2,	GETDATE(), GETDATE())

GO


INSERT dbo.Domain(name, change_date)
VALUES
	('SAM', GETDATE()),
	('OPT', GETDATE()),
	('KZR', GETDATE())

GO


INSERT dbo.Administrator(name, domain_id, change_date)
VALUES
	('Sample Admin',	1, GETDATE()),
	('Optum',			2, GETDATE()),
	('Kaizer',			3, GETDATE())

GO


INSERT dbo.SecurityLogin(user_name, password, individual_id, login_type, employer_id, domain_id, change_date)
VALUES
	('sbabitskiy',   'pswd', 1, 255, 1, 1, GETDATE()),
	('sleventuyev',  'pswd', 2, 255, 2, 1, GETDATE()),
	('sgorbaba',     'pswd', 3, 255, 3, 1, GETDATE()),
	('dolekhnovich', 'pswd', 4, 255, 4, 2, GETDATE()),
	('abogatko',     'pswd', 5, 255, 5, 2, GETDATE())

GO


INSERT dbo.State(name, short_name)
VALUES
	('State-1',		'ST-1'),
	('State-2',		'ST-2'),
	('State-3',		'ST-3'),
	('State-4',		'ST-4'),
	('State-5',		'ST-5'),
	('State-6',		'ST-6'),
	('Minnesota',	'ST-7')

GO


INSERT dbo.Address(state_id, city, street, zip_code, address_type, change_date)
VALUES
	(1,		'City-1',		'Street-1',					'123456789123',		1,		GETDATE()),
	(1,		'City-2',		'Street-2',					'223456789123',		1,		GETDATE()),
	(1,		'City-3',		'Street-3',					'323456789123',		1,		GETDATE()),
	(1,		'City-4',		'Street-4',					'423456789123',		1,		GETDATE()),
	(1,		'City-5',		'Street-5',					'523456789123',		1,		GETDATE()),
	(1,		'City-6',		'Street-6',					'623456789123',		1,		GETDATE()),
	(1,		'City-7',		'Street-7',					'723456789123',		1,		GETDATE()),
	(1,		'City-8',		'Street-8',					'823456789123',		1,		GETDATE()),
	(1,		'City-9',		'Street-9',					'923456789123',		1,		GETDATE()),
	(1,		'City-10',		'Street-10',				'123456789123',		1,		GETDATE()),
	(1,		'City-11',		'Street-11',				'223456789123',		1,		GETDATE()),
	(1,		'City-12',		'Street-12',				'123456789123',		1,		GETDATE()),
	(1,		'City-13',		'Street-13',				'323456789123',		1,		GETDATE()),
	(1,		'City-14',		'Street-14',				'423456789123',		1,		GETDATE()),
	(1,		'City-15',		'Street-15',				'523456789123',		1,		GETDATE()),
	(7,     'City-16',      'Street-16',				'523456789123',		1,		GETDATE()),
	(7,     'City-17',      'Street-17',				'423456789123',		1,		GETDATE()),
	(7,     'St. Paul',     'I-94 and McKnight Rd.',	'5514',				1,		GETDATE()),
	(7,     'St. Paul',     'I-90 and McKnight Rd.',	'5622',				1,		GETDATE()),
	(7,     'St. Paul',     'I-98 and McKnight Rd.',	'5371',				1,		GETDATE()),
	(1,		'City-A1',		'Street-A1',				'323456789123',		1,		GETDATE()),
	(1,		'City-A2',		'Street-A2',				'323456789123',		1,		GETDATE()),
	(1,		'City-A3',		'Street-A3',				'323456789123',		1,		GETDATE())

GO


INSERT dbo.AddressConnection(address_id,address_type, object_id, object_type)
VALUES
	--Participants
	(1,		1,		1,		2),
	(2,		1,		2,		2),
	(3,		1,		3,		2),
	(4,		1,		4,		2),
	(5,		1,		5,		2),
	--Employers
	(6,		1,		6,		1),
	(7,		1,		7,		1),
	(8,		1,		8,		1),
	(9,		1,		9,		1),
	(10,	1,		10,		1),
	(11,	1,		11,		1),
	(12,	1,		12,		1),
	(13,	1,		13,		1),
	(14,	1,		14,		1),
	(15,	1,		15,		1),
	(16,	1,		1,		1),
	(17,	1,		2,		1),
	(18,	1,		3,		1),
	(19,	1,		4,		1),
	(20,	1,		5,		1),
	--Admins
	(21,	1,		1,		0),
	(22,	1,		2,		0),
	(23,	1,		3,		0)

GO


INSERT dbo.Phone(number, change_date)
VALUES
	('123456789123',		GETDATE()),
	('223456789123',		GETDATE()),
	('323456789123',		GETDATE()),
	('423456789123',		GETDATE()),
	('523456789123',		GETDATE()),
	('623456789123',		GETDATE()),
	('723456789123',		GETDATE()),
	('823456789123',		GETDATE()),
	('923456789123',		GETDATE()),
	('123456789123',		GETDATE()),
	('223456789123',		GETDATE()),
	('123456789123',		GETDATE()),
	('323456789123',		GETDATE()),
	('423456789123',		GETDATE()),
	('523456789123',		GETDATE()),
	('523456789123',		GETDATE()),
	('523456789123',		GETDATE()),
	('523456789123',		GETDATE()),
	('523456789123',		GETDATE()),
	('523456789123',		GETDATE())

GO


INSERT dbo.PhoneConnection(phone_id, phone_type, object_id, object_type)
VALUES
	--Participants
	(1,		1,		1,		2),
	(2,		1,		2,		2),
	(3,		1,		3,		2),
	(4,		1,		4,		2),
	(5,		1,		5,		2),
	--Employers
	(6,		1,		6,		1),
	(7,		1,		7,		1),
	(8,		1,		8,		1),
	(9,		1,		9,		1),
	(10,	1,		10,		1),
	(11,	1,		11,		1),
	(12,	1,		12,		1),
	(13,	1,		13,		1),
	(14,	1,		14,		1),
	(15,	1,		15,		1),
	(16,	1,		1,		1),
	(17,	1,		2,		1),
	(18,	1,		3,		1),
	(19,	1,		4,		1),
	(20,	1,		5,		1),
	--Admins
	(15,	2,		1,		0),
	(16,	2,		2,		0),
	(3,		2,		3,		0)

GO

INSERT INTO BenefitsContract(employer_id, administrator_id)
VALUES
	(1,1),
	(2,1),
	(3,1),
	(4,1),
	(5,2),
	(6,2),
	(7,2),
	(8,2),
	(9,3),
	(10,3),
	(11,3),
	(12,3),
	(13,3)