﻿CREATE TABLE [dbo].[SecurityLogin](
	[login_id]      int          NOT NULL IDENTITY,
	[user_name]     varchar(50)  NOT NULL,
	[password]      varchar(40)  NOT NULL,
	[individual_id] int          NOT NULL,
	[login_type]    tinyint      NOT NULL,
	[employer_id]   int          NOT NULL,
	[domain_id]     smallint     NOT NULL,
	[change_date]   datetime2(4) NOT NULL DEFAULT GETDATE(),

    CONSTRAINT PK_SecurityLogin_login_id PRIMARY KEY CLUSTERED (login_id), 
    CONSTRAINT [FK_SecurityLogin_Domain] FOREIGN KEY (domain_id) REFERENCES [Domain](domain_id), 
    CONSTRAINT [FK_SecurityLogin_Employer] FOREIGN KEY (employer_id) REFERENCES [Employer](employer_id), 
    CONSTRAINT [FK_SecurityLogin_Individual] FOREIGN KEY (individual_id) REFERENCES [Individual](individual_id)  
)
GO