﻿CREATE TABLE [dbo].[SettingConnection]
(
	[setting_connection_id] INT				NOT NULL PRIMARY KEY, 
    [value]					VARCHAR(2000)	NOT NULL, 
    [setting_id]			INT				NOT NULL, 
    CONSTRAINT [FK_SettingConnection_Setting] FOREIGN KEY ([setting_id]) REFERENCES [dbo].[Setting]([setting_id]),
)