﻿CREATE TABLE [dbo].[Phone](
	[phone_id]    bigint       NOT NULL IDENTITY,
	[number]      varchar(15)  NOT NULL,
	[change_date] datetime2(4) NOT NULL DEFAULT GETDATE(),

    CONSTRAINT PK_Phone_phone_id PRIMARY KEY CLUSTERED (phone_id)  
)
GO