﻿CREATE TABLE [dbo].[PhoneConnection](
	[phone_id]    bigint  NOT NULL,
	[phone_type]  tinyint NOT NULL,
	[object_id]   int     NOT NULL,
	[object_type] tinyint NOT NULL,

    CONSTRAINT PK_PhoneConnection_phone_id$phone_type PRIMARY KEY CLUSTERED (phone_id, phone_type), 
    CONSTRAINT [FK_PhoneConnection_Phone] FOREIGN KEY (phone_id) REFERENCES [Phone](phone_id)  
)
GO