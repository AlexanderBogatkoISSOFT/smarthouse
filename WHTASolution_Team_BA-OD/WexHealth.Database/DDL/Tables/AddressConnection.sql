﻿CREATE TABLE [dbo].[AddressConnection](
	[address_id]   bigint  NOT NULL,
	[address_type] tinyint NOT NULL,
	[object_id]    int     NOT NULL,
	[object_type]  tinyint NOT NULL,

    CONSTRAINT PK_AddressConnection_address_id$address_type PRIMARY KEY CLUSTERED (address_id, address_type)
)
GO