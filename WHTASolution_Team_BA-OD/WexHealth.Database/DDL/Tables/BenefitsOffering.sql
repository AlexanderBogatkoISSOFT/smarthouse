﻿CREATE TABLE [dbo].[BenefitsOffering]
(
	[benefits_offering_id]	INT				NOT NULL PRIMARY KEY IDENTITY, 
    [name]					VARCHAR(100)	NOT NULL, 
    [plan]					TINYINT			NOT NULL, 
    [contribution_amount]	MONEY			NOT NULL, 
    [benefits_package_id]	INT				NOT NULL, 
    [change_date]			DATETIME2(4)	NOT NULL DEFAULT GETDATE(), 
    CONSTRAINT [FK_BenefitsOffering_BenefitsPackage] FOREIGN KEY ([benefits_package_id]) REFERENCES [BenefitsPackage]([benefits_package_id])
)
