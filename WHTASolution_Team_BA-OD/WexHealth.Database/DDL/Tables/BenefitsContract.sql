﻿--5
CREATE TABLE [dbo].[BenefitsContract](
	[employer_id]      int NOT NULL,
	[administrator_id] int NOT NULL

    CONSTRAINT PK_BenefitsContract_employer_id$administrator_id PRIMARY KEY CLUSTERED (employer_id,administrator_id), 
    CONSTRAINT [FK_BenefitsContract_Administrator] FOREIGN KEY (administrator_id) REFERENCES [Administrator](administrator_id),
    CONSTRAINT [FK_BenefitsContract_Employer] FOREIGN KEY (employer_id) REFERENCES [Employer](employer_id)
)
GO