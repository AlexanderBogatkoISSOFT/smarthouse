﻿CREATE TABLE [dbo].[Receipt]
(
	[receipt_id]	BIGINT			NOT NULL IDENTITY PRIMARY KEY, 
    [data]			VARBINARY(MAX)	NOT NULL, 
    [change_date]	DATETIME2(4)	NOT NULL DEFAULT GETDATE()
)
