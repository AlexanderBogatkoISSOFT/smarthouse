﻿CREATE TABLE [dbo].[Participant]
(
	[participant_id]	INT		NOT NULL PRIMARY KEY IDENTITY, 
    [individual_id]		INT		NOT NULL, 
    [employer_id]		INT		NOT NULL, 
    CONSTRAINT [FK_Partisipant_Individual] FOREIGN KEY ([individual_id]) REFERENCES [Individual]([individual_id]), 
    CONSTRAINT [FK_Partisipant_Employer] FOREIGN KEY ([employer_id]) REFERENCES [Employer]([employer_id])
)
