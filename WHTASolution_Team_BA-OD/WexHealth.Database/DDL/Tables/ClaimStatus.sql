﻿CREATE TABLE [dbo].[ClaimStatus]
(
	[claim_status_id]	BIGINT			NOT NULL IDENTITY PRIMARY KEY, 
    [status]			TINYINT			NOT NULL, 
    [notes]				VARCHAR(255)	NOT NULL, 
    [date]				DATETIME2(2)	NOT NULL
)