﻿CREATE TABLE [dbo].[Claim]
(
	[claim_id]				BIGINT			NOT NULL IDENTITY PRIMARY KEY, 
    [date_of_service]		DATE			NOT NULL, 
    [claim_number]			VARCHAR(30)		NOT NULL, 
    [amount]				MONEY			NOT NULL, 
    [participant_id]		INT				NOT NULL, 
    [benefits_offering_id]	INT				NOT NULL, 
    [claim_status_id]		BIGINT			NOT NULL, 
    [receipt_id]			BIGINT			NOT NULL, 
    [change_date]			DATETIME2(4)	NOT NULL DEFAULT GETDATE(), 
    CONSTRAINT [FK_Claim_Participant] FOREIGN KEY ([participant_id]) REFERENCES [Participant]([participant_id]), 
    CONSTRAINT [FK_Claim_BenefitsOffering] FOREIGN KEY ([benefits_offering_id]) REFERENCES [BenefitsOffering]([benefits_offering_id]), 
    CONSTRAINT [FK_Claim_Receipt] FOREIGN KEY ([receipt_id]) REFERENCES [Receipt]([receipt_id]), 
    CONSTRAINT [FK_Claim_ClaimStatus] FOREIGN KEY ([claim_status_id]) REFERENCES [ClaimStatus]([claim_status_id])

)
