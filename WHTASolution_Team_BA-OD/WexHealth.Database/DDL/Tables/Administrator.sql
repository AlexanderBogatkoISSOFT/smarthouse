﻿CREATE TABLE [dbo].[Administrator](
	[administrator_id] int          NOT NULL IDENTITY,
	[name]             varchar(50)  NOT NULL,
	[domain_id]        smallint     NOT NULL,
	[change_date]      datetime2(4) NOT NULL DEFAULT GETDATE(),

    CONSTRAINT PK_Administrator_administrator_id PRIMARY KEY CLUSTERED (administrator_id)  
)
GO