﻿CREATE TABLE [dbo].[Individual](
	[individual_id] int          NOT NULL IDENTITY,
	[first_name]    varchar(50)  NOT NULL,
	[last_name]     varchar(50)  NOT NULL,
	[email]         varchar(50)  NOT NULL,
	[birthdate]     date         NULL,
	[ssn]           varchar(9)   NULL,
	[change_date]   datetime2(4) NOT NULL DEFAULT GETDATE(),

    CONSTRAINT PK_Individual_individual_id PRIMARY KEY CLUSTERED (individual_id)  
)
GO