﻿CREATE TABLE [dbo].[Address](
	[address_id]   bigint       NOT NULL IDENTITY,
	[city]         varchar(30)  NOT NULL,
	[street]       varchar(100) NOT NULL,
	[zip_code]     varchar(12)  NOT NULL,
	[state_id]     tinyint      NOT NULL,
	[address_type] tinyint      NOT NULL,
	[change_date]  datetime2(4) NOT NULL DEFAULT GETDATE(),

    CONSTRAINT PK_Address_address_id PRIMARY KEY CLUSTERED (address_id),    
    CONSTRAINT [FK_AddressConnection_Address] FOREIGN KEY (address_id) REFERENCES [Address](address_id), 
    CONSTRAINT [FK_Address_State] FOREIGN KEY (state_id) REFERENCES [State](state_id)  
)
GO