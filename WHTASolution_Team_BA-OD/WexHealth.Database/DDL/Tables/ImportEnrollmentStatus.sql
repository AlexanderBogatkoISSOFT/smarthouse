﻿CREATE TABLE [dbo].[ImportEnrollmentStatus](
	[import_enrollment_status_id] int          NOT NULL,
	[status]                      tinyint      NOT NULL,
	[date]                        datetime2(4) NOT NULL,

    CONSTRAINT PK_ImportEnrollmentStatus_import_enrollment_status_id PRIMARY KEY CLUSTERED (import_enrollment_status_id)  
)
GO