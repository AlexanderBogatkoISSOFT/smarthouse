﻿CREATE TABLE [dbo].[ImportEnrollment](
	[import_enrollment_id]        int          NOT NULL,
	[employer_id]                 int          NOT NULL,
	[import_enrollment_status_id] int          NOT NULL,
	[initial_file_name]           int          NOT NULL,
	[error]                       varchar(MAX) NOT NULL,
	[change_date]                 datetime2(4) NOT NULL DEFAULT GETDATE(),

    CONSTRAINT PK_ImportEnrollment_import_enrollment_id PRIMARY KEY CLUSTERED (import_enrollment_id), 
    CONSTRAINT [FK_ImportEnrollment_ImportEnrollmentStatus] FOREIGN KEY (import_enrollment_status_id) REFERENCES [ImportEnrollmentStatus](import_enrollment_status_id), 
    CONSTRAINT [FK_ImportEnrollment_Employer] FOREIGN KEY (employer_id) REFERENCES [Employer](employer_id)  
)
GO