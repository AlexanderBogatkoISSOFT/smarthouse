﻿CREATE TABLE [dbo].[Employer](
	[employer_id] int          NOT NULL IDENTITY,
	[name]        varchar(50)  NOT NULL,
	[code]        varchar(10)  NOT NULL,
	[logo_id]     int          NOT NULL,
	[change_date] datetime2(4) NOT NULL DEFAULT GETDATE(),

    CONSTRAINT PK_Employer_employer_id PRIMARY KEY CLUSTERED (employer_id),
    CONSTRAINT FK_Employer_Logo FOREIGN KEY ([logo_id]) REFERENCES dbo.[Logo]([logo_id])
)
GO