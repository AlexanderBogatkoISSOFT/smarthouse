﻿CREATE TABLE [dbo].[State](
	[state_id]   tinyint     NOT NULL IDENTITY,
	[name]       varchar(20) NOT NULL,
	[short_name] char(100)   NOT NULL,

    CONSTRAINT PK_State_state_id PRIMARY KEY CLUSTERED (state_id)  
)
GO