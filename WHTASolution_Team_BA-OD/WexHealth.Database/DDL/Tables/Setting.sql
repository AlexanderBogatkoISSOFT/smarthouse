﻿CREATE TABLE [dbo].[Setting]
(
	[setting_id]		INT				NOT NULL PRIMARY KEY, 
    [name]				VARCHAR(50)		NOT NULL, 
    [default_value]		VARCHAR(2000)	NOT NULL, 
    [value_type]		VARCHAR(50)		NOT NULL, 
    [setting_category]	SMALLINT		NOT NULL, 
    [change_date]		DATETIME2(4)	NOT NULL DEFAULT GETDATE()
)
