﻿CREATE TABLE [dbo].[BenefitsPackage]
(
	[benefits_package_id]	INT				NOT NULL PRIMARY KEY IDENTITY, 
    [name]					VARCHAR(100)	NOT NULL, 
    [start_date]			DATE			NOT NULL, 
    [end_date]				DATE			NOT NULL, 
    [payroll_frequency]		TINYINT			NOT NULL, 
    [package_status_id]		INT				NOT NULL, 
    [employer_id]			INT				NOT NULL, 
    [change_date]			DATETIME2(4)	NOT NULL DEFAULT GETDATE(), 
    CONSTRAINT [FK_BenefitsPackage_Employer] FOREIGN KEY ([employer_id]) REFERENCES [Employer]([employer_id]), 
    CONSTRAINT [FK_BenefitsPackage_BenefitsPackageStatus] FOREIGN KEY ([package_status_id]) REFERENCES [BenefitsPackageStatus]([package_status_id])
)
