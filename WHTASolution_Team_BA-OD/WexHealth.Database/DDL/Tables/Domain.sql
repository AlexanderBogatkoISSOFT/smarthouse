﻿CREATE TABLE [dbo].[Domain](
	[domain_id]   smallint     NOT NULL IDENTITY,
	[name]        varchar(30)  NOT NULL,
	[change_date] datetime2(4) NOT NULL DEFAULT GETDATE(),

    CONSTRAINT PK_Domain_domain_id PRIMARY KEY CLUSTERED (domain_id)  
)
GO