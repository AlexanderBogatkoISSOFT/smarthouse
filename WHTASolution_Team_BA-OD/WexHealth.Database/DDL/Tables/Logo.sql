﻿CREATE TABLE [dbo].[Logo]
(
	[logo_id]		INT				NOT NULL IDENTITY, 
    [data]			VARBINARY(MAX)	NOT NULL, 
    [change_date]	DATETIME2(4)	NOT NULL
    
    CONSTRAINT PK_Logo_logo_id PRIMARY KEY CLUSTERED (logo_id) DEFAULT GETDATE(),    
)
