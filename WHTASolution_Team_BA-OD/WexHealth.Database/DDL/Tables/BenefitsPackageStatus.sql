﻿CREATE TABLE [dbo].[BenefitsPackageStatus]
(
	[package_status_id] INT			 NOT NULL PRIMARY KEY IDENTITY, 
    [status]            TINYINT      NOT NULL,
    [date]				DATETIME2(2) NOT NULL
)
