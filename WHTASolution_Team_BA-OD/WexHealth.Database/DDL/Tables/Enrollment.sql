﻿CREATE TABLE [dbo].[Enrollment]
(
	[enrollment_id]			INT			 NOT NULL IDENTITY PRIMARY KEY, 
    [election_amount]		MONEY		 NOT NULL, 
	[contribution_amount]	MONEY		 NOT NULL,
    [participant_id]		INT			 NOT NULL, 
    [benefits_offering_id]	INT			 NOT NULL, 
    [change_date]			DATETIME2(4) NOT NULL DEFAULT GETDATE(), 
    CONSTRAINT [FK_Enrollment_Participant] FOREIGN KEY ([participant_id]) REFERENCES [Participant]([participant_id]), 
    CONSTRAINT [FK_Enrollment_BenefitsOffering] FOREIGN KEY ([benefits_offering_id]) REFERENCES [BenefitsOffering]([benefits_offering_id])
)
