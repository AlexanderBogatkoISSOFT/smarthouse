﻿CREATE PROCEDURE dbo.usp_GetStates
/*

# Select list of states
# Created by [Dmitry Olekhnovich 28.02.2019]
# Last edit by [Dmitry Olekhnovich 28.02.2019]

EXEC dbo.usp_GetStates

*/
AS
    SELECT 
           st.state_id,
           [state_name] = st.[name],
           [state_shortName] = st.short_name
    FROM dbo.[State] st
GO
