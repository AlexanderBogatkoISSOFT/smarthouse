﻿CREATE PROCEDURE [dbo].[usp_InsertUpdateAdminInfo]
    @parAdminId     INT          = NULL,
    @parName        VARCHAR(50)  = NULL,
    @parAlias       VARCHAR(30)  = NULL,
    @parStreet      VARCHAR(100) = NULL,
    @parCity        VARCHAR(30)  = NULL,
    @parStateId     TINYINT      = NULL,
    @parZipCode     VARCHAR(12)  = NULL,
    @parPhoneNumber VARCHAR(15)  = NULL
AS
/*

# Find Admin By Id
# Created by [Alexander Bogatko 01.03.2019]
# Last edit by [Alexander Bogatko 01.03.2019]

EXEC [dbo].[usp_InsertUpdateAdminInfo] 1

*/
BEGIN TRY
    DECLARE @ErrorMessage VARCHAR(MAX),
            @DomainId       INT,
            @AddressId      BIGINT,
            @PhoneId        BIGINT,
            @Name           VARCHAR(50),
            @Alias          VARCHAR(30),
            @Street         VARCHAR(100),
            @City           VARCHAR(30),
            @StateId        TINYINT,    
            @ZipCode        VARCHAR(12),
            @PhoneNumber    VARCHAR(15)
    BEGIN TRAN
            IF EXISTS (SELECT 1 FROM dbo.Administrator A WHERE A.administrator_id = @parAdminId)
                BEGIN
                    WITH AdminInfo AS
                    (
                       SELECT Adm.administrator_id  AS  'AdminId',
                              Adm.name              AS  'Name',
                              D.name                AS  'Alias',
                              D.domain_id           AS  'DomainId',
                              Adr.street            AS  'Street',
                              Adr.city              AS  'City',
                              Adr.address_id        AS  'AddressId',
                              S.state_id            AS  'StateId',
                              Adr.zip_code          AS  'ZipCode',
                              P.number              AS  'PhoneNumber',
                              P.phone_id            AS  'PhoneId'
       
                       FROM dbo.Administrator Adm  JOIN dbo.Domain D
                                                       ON D.domain_id = Adm.domain_id
                                                   JOIN dbo.AddressConnection AC
                                                       ON  AC.object_id = Adm.administrator_id AND AC.object_type = 0
                                                   JOIN dbo.Address Adr
                                                       ON Adr.address_id = AC.address_id
                                                   JOIN dbo.State S
                                                       ON S.state_id = Adr.state_id
                                                   JOIN dbo.PhoneConnection PC
                                                       ON PC.object_id = Adm.administrator_id AND PC.object_type = 0
                                                   JOIN dbo.Phone P
                                                       ON P.phone_id = PC.phone_id
                    )
                    SELECT @Name = Name,
                           @Alias = Alias,
                           @DomainId = DomainId,
                           @AddressId = AddressId,
                           @StateId = StateId,
                           @Street = Street,
                           @City = City,
                           @PhoneId = PhoneId,
                           @PhoneNumber = PhoneNumber,
                           @ZipCode = ZipCode
                    FROM AdminInfo
                    Where AdminId = @parAdminId

                    IF (@parName != @Name AND @parName IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Administrator 
                                SET name = @parName,
                                change_date = GETDATE()
                            WHERE administrator_id = @parAdminId
                        END
                    IF (@parAlias != @Alias AND @parAlias IS NOT NULL)
                        EXEC usp_InsertUpdateDomain @domain_id = @DomainId, @Name = @parAlias
                    IF (@parPhoneNumber != @PhoneNumber AND @parPhoneNumber IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Phone 
                                SET number = @parPhoneNumber,
                                change_date = GETDATE()
                            WHERE  phone_id = @PhoneId
                        END
                    IF (@parStreet != @Street AND @parStreet IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Address
                                SET street = @parStreet,
                                change_date = GETDATE()
                            WHERE address_id = @AddressId
                        END
                    IF (@parCity != @City AND @parCity IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Address
                                SET city = @parCity,
                                change_date = GETDATE()
                            WHERE address_id = @AddressId
                        END
                    IF (@parStateId != @StateId AND @parStateId IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Address
                                SET state_id = @parStateId,
                                change_date = GETDATE()
                            WHERE address_id = @AddressId
                        END
                    IF (@parZipCode != @ZipCode AND @parZipCode IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Address
                                SET zip_code = @parZipCode,
                                change_date = GETDATE()
                            WHERE address_id = @AddressId
                        END
                END
            ELSE
                BEGIN
                    IF (@parAdminId IS NULL     AND
                        @parName    IS NOT NULL AND 
                        @parAlias   IS NOT NULL AND 
                        @parName    IS NOT NULL AND 
                        @parStreet  IS NOT NULL AND 
                        @parStateId IS NOT NULL AND
                        @parCity    IS NOT NULL AND
                        @parZipCode IS NOT NULL)
                    BEGIN
                        INSERT INTO dbo.Domain(name, change_date)
                        VALUES (@parAlias, GETDATE())
                        SELECT @DomainId = CAST(SCOPE_IDENTITY() AS INT)

                        INSERT INTO dbo.Administrator (name, domain_id, change_date)
                        VALUES (@parName, @DomainId, GETDATE())
                        SELECT @parAdminId = CAST(SCOPE_IDENTITY() AS INT)

                        INSERT INTO dbo.Phone (number, change_date)
                        VALUES (@parPhoneNumber, GETDATE())
                        SELECT @PhoneId = CAST(SCOPE_IDENTITY() AS BIGINT)
                        INSERT INTO dbo.PhoneConnection(phone_id, phone_type, object_id, object_type)
                        VALUES (@PhoneId, 1, @parAdminId, 0)

                        INSERT INTO dbo.Address (address_type, city, street, state_id, zip_code, change_date)
                        VALUES (2, @parCity, @parStreet, @parStateId, @parZipCode, GETDATE());
                        SELECT @AddressId = CAST(SCOPE_IDENTITY() AS BIGINT)
                        INSERT INTO dbo.AddressConnection (address_id, address_type, object_id, object_type)
                        VALUES (@AddressId, 2, @parAdminId, 0)

                    END
                END
    COMMIT TRAN
END TRY
BEGIN CATCH
    SET @ErrorMessage = ERROR_MESSAGE()
    RAISERROR(@ErrorMessage, 14, 1, 'Can''not inser/update value.')
    ROLLBACK TRAN
END CATCH