﻿CREATE PROCEDURE [dbo].[usp_GetEmployers]
@employer_name VARCHAR(50) = NULL,
@employer_code VARCHAR(10) = NULL
/*

# Find full list of employers names
# Created by [Dmitry Olekhnovich 25.02.2019]
# Last edit by [Dmitry Olekhnovich 25.02.2019]

EXEC [dbo].[usp_GetEmployers]

*/
AS

    SET @employer_name = ISNULL(@employer_name, '') + '%'
    SET @employer_code = ISNULL(@employer_code, '') + '%'

    SELECT 
           emp.employer_id,
           [employer_name] = emp.[name],
           [employer_code] = emp.code,
           [employer_logoId] = emp.logo_id
    
    FROM dbo.Employer emp

    WHERE LOWER(emp.[name]) like LOWER(@employer_name)
    and LOWER(emp.code) like LOWER(@employer_code)
GO