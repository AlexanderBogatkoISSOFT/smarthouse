﻿CREATE PROCEDURE [dbo].[usp_GetConsumers]
/*

# Select cunsomers dataset
# Created by [Dmitry Olekhnovich 19.02.2019]
# Last edit by [Dmitry Olekhnovich 28.02.2019]

EXEC [dbo].[usp_GetConsumers]

*/

AS
    SELECT  prt.participant_id,
            [individual_firstName] = ind.first_name,
            [individual_lastName] = ind.last_name,
            [individual_ssn] = ind.ssn
    FROM dbo.Participant prt

        JOIN dbo.Individual ind
            ON prt.individual_id = ind.individual_id
GO