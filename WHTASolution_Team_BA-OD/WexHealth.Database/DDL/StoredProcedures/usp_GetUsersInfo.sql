﻿CREATE PROCEDURE [dbo].[usp_GetUsersInfo]
    @parEmployerId	INT = NULL,
	@parAdminId		INT = NULL
AS
/*

# Get Users Info by Employer Id
# Created by [Alexander Bogatko 04.03.2019]
# Last edit by [Alexander Bogatko 05.03.2019]

*/
    SELECT SL.login_id          AS  'LoginId',
           I.first_name         AS  'FirstName',
           I.last_name          AS  'LastName',
           I.email              AS  'Email',
		   SL.user_name	        AS  'UserName',
		   SL.password	        AS  'Password',
		   SL.employer_id       AS  'EmployerId'
    
    FROM dbo.SecurityLogin SL JOIN dbo.Individual I
                                ON I.individual_id = SL.individual_id
                              JOIN dbo.BenefitsContract BC
                                ON BC.employer_id = SL.employer_id
    WHERE (BC.administrator_id = @parAdminId AND @parEmployerId IS NULL) 
	   OR (BC.employer_id = @parEmployerId AND @parAdminId IS NULL)

GO