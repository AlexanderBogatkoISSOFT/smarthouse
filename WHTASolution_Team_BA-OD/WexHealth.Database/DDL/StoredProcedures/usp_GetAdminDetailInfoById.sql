﻿CREATE PROCEDURE [dbo].[usp_GetAdminDetailInfoById]
    @parAdminId INT
AS
/*

# Insert or Update Admin
# Created by [Alexander Bogatko 01.03.2019]
# Last edit by [Alexander Bogatko 01.03.2019]


*/
BEGIN
SELECT Adm.name     AS  'Name',
       D.name       AS  'Alias',
       Adr.street   AS  'Street',
       Adr.city     AS  'City',
       S.state_id   AS  'StateId',
       Adr.zip_code AS  'ZipCode',
       P.number     AS  'PhoneNumber'
       
FROM dbo.Administrator Adm  JOIN dbo.Domain D
                                ON D.domain_id = Adm.domain_id
                            JOIN dbo.AddressConnection AC
                                ON  AC.object_id = Adm.administrator_id AND AC.object_type = 0
                            JOIN dbo.Address Adr
                                ON Adr.address_id = AC.address_id
                            JOIN dbo.State S
                                ON S.state_id = Adr.state_id
                            JOIN dbo.PhoneConnection PC
                                ON PC.object_id = Adm.administrator_id AND PC.object_type = 0
                            JOIN dbo.Phone P
                                ON P.phone_id = PC.phone_id
WHERE Adm.administrator_id = @parAdminId
END