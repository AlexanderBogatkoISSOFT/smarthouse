﻿CREATE PROCEDURE dbo.usp_GetEmployerInfoById
	@parEmployerId			INT
AS
BEGIN
    SELECT  E.name				AS 'Name',
            E.code				AS 'Code',
            A.street			AS 'Street',
            A.city				AS 'City',
            ST.state_id			AS 'StateId',
            A.zip_code			AS 'ZipCode',
            P.number			AS 'Phone',
			BC.administrator_id AS 'AdminId'


    FROM    dbo.Employer E  JOIN dbo.AddressConnection AC
                                ON E.employer_id = AC.object_id
                                and AC.object_type = 1
                            JOIN dbo.Address A
                                ON A.address_id = AC.address_id
                            JOIN dbo.State ST
                                ON A.state_id = ST.state_id
                            JOIN dbo.PhoneConnection PC
                                ON E.employer_id = PC.object_id
                                and PC.object_type = 1     
                            JOIN dbo.Phone P
                                ON P.phone_id = PC.phone_id
							JOIN dbo.BenefitsContract BC
								ON BC.employer_id = E.employer_id

   WHERE E.employer_id = @parEmployerId
END
GO