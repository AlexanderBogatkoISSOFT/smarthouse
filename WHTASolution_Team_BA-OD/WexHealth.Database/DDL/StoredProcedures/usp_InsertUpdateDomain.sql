﻿CREATE PROCEDURE [dbo].[usp_InsertUpdateDomain]
@domain_id smallint NULL,
@name varchar(30)

/*

# Delete row from table by Id
# Created by [Dmitry Olekhnovich 11.02.2019]
# Last edit by [Dmitry Olekhnovich 20.02.2019]

EXEC [dbo].[usp_InsertUpdateDomain] NULL, 'Test'

*/

AS
BEGIN TRY
    DECLARE @msg varchar(max)

    BEGIN TRANSACTION
        IF EXISTS (SELECT 1 FROM dbo.Domain d WHERE @domain_id = d.domain_id)
            UPDATE dbo.Domain
                SET [name] = @name,
                    [change_date] = GETDATE()
            WHERE domain_id = @domain_id
        ELSE
            INSERT dbo.Domain
            VALUES (@name, GETDATE())

    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    SET @msg = ERROR_MESSAGE()
    RAISERROR(@msg, 14, 1, 'Can''not inser/update value.')
    ROLLBACK TRANSACTION
END CATCH
GO