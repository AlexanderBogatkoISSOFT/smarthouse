﻿CREATE PROCEDURE [dbo].[usp_DeleteDomain]
@domain_id smallint

/*

# Delete row from table by Id
# Created by [Dmitry Olekhnovich 11.02.2019]
# Last edit by [Dmitry Olekhnovich 20.02.2019]

EXEC [dbo].[usp_DeleteDomain] 1

*/

AS
BEGIN TRY
    DECLARE @msg varchar(max)

    BEGIN TRANSACTION
        DELETE dbo.Domain
        WHERE domain_id = @domain_id
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    SET @msg = ERROR_MESSAGE()
    RAISERROR(@msg, 14, 1)
    ROLLBACK TRANSACTION
END CATCH
GO