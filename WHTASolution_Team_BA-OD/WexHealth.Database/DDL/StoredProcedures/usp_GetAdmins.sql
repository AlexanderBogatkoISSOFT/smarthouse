﻿CREATE PROCEDURE [dbo].[usp_GetAdmins]
AS
/*

# Insert or Update Admin
# Created by [Alexander Bogatko 01.03.2019]
# Last edit by [Alexander Bogatko 01.03.2019]


*/
BEGIN
SELECT Adm.administrator_id AS 'AdminId',
       Adm.name             AS  'Name',
       D.name               AS  'Alias'
       
FROM dbo.Administrator Adm  JOIN dbo.Domain D
                                ON D.domain_id = Adm.domain_id
                            JOIN dbo.AddressConnection AC
                                ON  AC.object_id = Adm.administrator_id AND AC.object_type = 0
                            JOIN dbo.Address Adr
                                ON Adr.address_id = AC.address_id
                            JOIN dbo.State S
                                ON S.state_id = Adr.state_id
                            JOIN dbo.PhoneConnection PC
                                ON PC.object_id = Adm.administrator_id AND PC.object_type = 0
                            JOIN dbo.Phone P
                                ON P.phone_id = PC.phone_id
END