﻿CREATE PROCEDURE [dbo].[usp_UpdateUserInfoById]
    @parLoginId             INT,
    @parFirstName           VARCHAR(50) = NULL,
    @parLastName            VARCHAR(50) = NULL,
    @parUserName            VARCHAR(50) = NULL,
    @parEmail               VARCHAR(50) = NULL,
    @parPassword            VARCHAR(40) = NULL
AS
/*

# Update Individual & SecurityLogin Info byId
# Created by [Alexander Bogatko 04.03.2019]
# Last edit by [Alexander Bogatko 04.03.2019]

*/
BEGIN TRY
    DECLARE @ErrorMessage   VARCHAR(MAX),
            @IndividualId   INT,
            @FirstName      VARCHAR(50),
            @LastName       VARCHAR(50),
            @UserName       VARCHAR(50),
            @Email          VARCHAR(50),
            @Password       VARCHAR(40)

   BEGIN TRAN
            IF EXISTS (SELECT 1 FROM dbo.SecurityLogin SL WHERE SL.login_id = @parLoginId)
                BEGIN
                    WITH UserInfo AS
                    (
                       SELECT SL.login_id           AS  'LoginId',
                              SL.individual_id      AS  'IndividualId',
                              SL.user_name          AS  'UserName',
                              SL.password           AS  'Password',
                              I.first_name          AS  'FirstName',
                              I.last_name           AS  'LastName',
                              I.email               AS  'Email'
       
                       FROM dbo.SecurityLogin SL JOIN dbo.Individual I
                                                    ON I.individual_id = SL.individual_id    
                    )
                    SELECT @UserName = UserName,
                           @Password = Password,
                           @FirstName = FirstName,
                           @LastName = LastName,
                           @IndividualId = IndividualId
                    FROM UserInfo
                    Where LoginId = @parLoginId
                    --Security Login
                    IF (@parUserName != @UserName AND @parUserName IS NOT NULL)
                        BEGIN
                            UPDATE dbo.SecurityLogin
                                SET user_name = @parUserName,
                                change_date = GETDATE()
                            WHERE login_id = @parLoginId
                        END

                    IF (@parPassword != @Password AND @parPassword IS NOT NULL)
                        BEGIN
                            UPDATE dbo.SecurityLogin
                                SET password = @parPassword,
                                change_date = GETDATE()
                            WHERE login_id = @parLoginId
                        END
                    --Individual
                    IF (@parFirstName != @FirstName AND @parFirstName IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Individual
                                SET first_name = @parFirstName,
                                change_date = GETDATE()
                            WHERE individual_id = @IndividualId
                        END

                    IF (@parLastName != @LastName AND @parLastName IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Individual
                                SET last_name = @parLastName,
                                change_date = GETDATE()
                            WHERE individual_id = @IndividualId
                        END

                    IF (@parEmail != @Email AND @parEmail IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Individual
                                SET email = @parEmail,
                                change_date = GETDATE()
                            WHERE individual_id = @IndividualId
                        END
                END
    COMMIT TRAN
END TRY
BEGIN CATCH
    SET @ErrorMessage = ERROR_MESSAGE()
    RAISERROR(@ErrorMessage, 14, 1, 'Can''not insert value.')
    ROLLBACK TRAN
END CATCH

GO