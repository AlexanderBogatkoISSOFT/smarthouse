﻿CREATE PROCEDURE [dbo].[usp_UpdateClaim]
    @parClaimId        BIGINT,
    @parDateOfService  DATE = NULL,
    @parPlan           TINYINT = NULL,
    @parAmount         MONEY = NULL

/*

# Update Claim By Id
# Created by [Alexander Bogatko 28.02.2019]
# Last edit by [Alexander Bogatko 28.02.2019]

EXEC [dbo].[usp_UpdateClaim] 1

*/

AS
BEGIN TRY
    DECLARE @ErrorMessage       VARCHAR(MAX),
            @BenefitsOfferingId INT
    BEGIN TRAN

        SELECT @BenefitsOfferingId = C.benefits_offering_id FROM dbo.Claim C WHERE C.claim_id = @parClaimId;

        UPDATE dbo.Claim
            SET amount = @parAmount,
                date_of_service = @parDateOfService,
                [change_date] = GETDATE()
        WHERE claim_id = @parClaimId


        UPDATE dbo.BenefitsOffering
            SET [plan] = @parPlan,
                change_date = GETDATE()
        WHERE benefits_offering_id = @BenefitsOfferingId
    COMMIT TRAN
END TRY
BEGIN CATCH
        SET @ErrorMessage = ERROR_MESSAGE()
        RAISERROR(@ErrorMessage, 14, 1, 'Can''not update value.')
    ROLLBACK TRAN
END CATCH