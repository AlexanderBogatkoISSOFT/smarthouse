﻿CREATE PROCEDURE [dbo].[usp_GetDomain]
@domain_id smallint

/*

# Select one row from table by Id
# Created by [Dmitry Olekhnovich 11.02.2019]
# Last edit by [Dmitry Olekhnovich 20.02.2019]

EXEC [dbo].[usp_GetDomain] 1

*/

AS
    SELECT  d.domain_id
           ,[domain_name] = d.[name]
           ,[domain_change_date] = d.change_date
    FROM dbo.Domain d
    WHERE d.domain_id = @domain_id
GO