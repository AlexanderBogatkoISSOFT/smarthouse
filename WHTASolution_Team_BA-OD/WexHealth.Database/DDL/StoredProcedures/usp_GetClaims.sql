﻿CREATE PROCEDURE [dbo].[usp_GetClaims]
@claim_number VARCHAR(30) = NULL,
@employer_id INT = NULL
/*

# Select cunsomers dataset
# Created by [Dmitry Olekhnovich 19.02.2019]
# Last edit by [Dmitry Olekhnovich 25.02.2019]

EXEC [dbo].[usp_GetClaims]

*/

AS

    SET @claim_number = ISNULL(@claim_number, '') + '%'

    SELECT  clm.claim_id,
            clm.claim_number,
            [consumer_fullName] = ind.first_name + ' ' + ind.last_name,
            [employer_fullName] = emp.[name] + ' (' + emp.code + ') ',
            [claim_dateOfService] = clm.date_of_service,
            [claim_amount] = clm.amount,
            [benefitsOffering_plan] = bof.[plan],
            [consumer_id] = prt.participant_id,
            emp.employer_id
    FROM dbo.Claim clm

        JOIN dbo.Participant prt
            ON clm.participant_id = prt.participant_id
        JOIN dbo.Individual ind
            ON prt.individual_id = ind.individual_id
        JOIN dbo.Employer emp
            ON prt.employer_id = emp.employer_id
        JOIN dbo.BenefitsOffering bof
            ON clm.benefits_offering_id = bof.benefits_offering_id
    WHERE clm.claim_number LIKE @claim_number
    and (emp.employer_id = @employer_id OR @employer_id IS NULL)
GO