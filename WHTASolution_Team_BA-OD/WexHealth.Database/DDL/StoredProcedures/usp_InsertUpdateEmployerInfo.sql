﻿CREATE PROCEDURE [dbo].[usp_InsertUpdateEmployerInfo]
    @parAdminId     INT             = NULL,
    @parEmployerId  INT             = NULL,
    @parName        VARCHAR(50)     = NULL,
    @parCode        VARCHAR(10)     = NULL,
    @parStreet      VARCHAR(100)    = NULL,
    @parCity        VARCHAR(30)     = NULL,
    @parStateId     TINYINT         = NULL,
    @parZipCode     VARCHAR(12)     = NULL,
    @parPhoneNumber VARCHAR(15)     = NULL,
    @parLogoData    VARBINARY(MAX)  = NULL       
AS
/*

# Update/Insert Employer Info
# Created by [Alexander Bogatko 05.03.2019]
# Last edit by [Alexander Bogatko 05.03.2019]

Update
EXEC usp_InsertUpdateEmployerInfo @parEmployerId = 1, 
                                  @parName       = 'HelloWorld'

Insert
DECLARE @Data VARBINARY(MAX) = CAST(123456789101112 AS VARBINARY(MAX))
EXEC usp_InsertUpdateEmployerInfo @parAdminId       = 1, 
                                  @parName          = 'HelloWorldTest',
                                  @parCode          = 'Code',      
                                  @parStreet        = 'StreetTest',    
                                  @parCity          = 'TestCity',    
                                  @parStateId       = 1,   
                                  @parZipCode       = '4444' ,  
                                  @parPhoneNumber   = '7788',
                                  @parLogoData      = @Data
*/
BEGIN TRY
    DECLARE @ErrorMessage   VARCHAR(MAX),
            @EmployerId     INT,
            @LogoId         INT,
            @AddressId      BIGINT,
            @PhoneId        BIGINT,
            @Name           VARCHAR(50),
            @Code           VARCHAR(10),
            @LogoData       VARBINARY(MAX),
            @Street         VARCHAR(100),
            @City           VARCHAR(30),
            @StateId        TINYINT,    
            @ZipCode        VARCHAR(12),
            @PhoneNumber    VARCHAR(15),
            @AddressType    TINYINT = 1,
            @ObjectType     TINYINT = 1
    BEGIN TRAN
            IF EXISTS (SELECT 1 FROM dbo.Employer E WHERE E.employer_id = @parEmployerId)
                BEGIN

                    SELECT @EmployerId   = E.employer_id,
                           @Name         = E.name,             
                           @Code         = E.code,             
                           @LogoId       = L.logo_id,        
                           @LogoData     = L.data,         
                           @Street       = Adr.street,       
                           @City         = Adr.city,           
                           @AddressId    = Adr.address_id,
                           @StateId      = S.state_id,      
                           @ZipCode      = Adr.zip_code,    
                           @PhoneNumber  = P.number,    
                           @PhoneId      = P.phone_id   
       
                    FROM dbo.Employer E  JOIN dbo.Logo L
                                             ON L.logo_id = E.employer_id    
                                         JOIN dbo.AddressConnection AC
                                             ON  AC.object_id = E.employer_id AND AC.object_type = @ObjectType
                                         JOIN dbo.Address Adr
                                             ON Adr.address_id = AC.address_id
                                         JOIN dbo.State S
                                             ON S.state_id = Adr.state_id
                                         JOIN dbo.PhoneConnection PC
                                             ON PC.object_id = E.employer_id AND PC.object_type = @ObjectType
                                         JOIN dbo.Phone P
                                             ON P.phone_id = PC.phone_id
                    WHERE E.employer_id = @parEmployerId

                    IF (@parName != @Name AND @parName IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Employer
                                SET name = @parName,
                                change_date = GETDATE()
                            WHERE employer_id = @parEmployerId
                        END

                    IF (@parCode != @Code AND @parCode IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Employer
                                SET code = @parCode,
                                change_date = GETDATE()
                            WHERE employer_id = @parEmployerId
                        END
                    --Logo
                    IF (@parLogoData != @LogoData AND @parLogoData IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Logo
                                SET data = @parLogoData,
                                change_date = GETDATE()
                            WHERE logo_id = @LogoId
                        END
                    --Phone
                    IF (@parPhoneNumber != @PhoneNumber AND @parPhoneNumber IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Phone 
                                SET number = @parPhoneNumber,
                                change_date = GETDATE()
                            WHERE  phone_id = @PhoneId
                        END
                    --Address
                    IF (@parStreet != @Street AND @parStreet IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Address
                                SET street = @parStreet,
                                change_date = GETDATE()
                            WHERE address_id = @AddressId
                        END

                    IF (@parCity != @City AND @parCity IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Address
                                SET city = @parCity,
                                change_date = GETDATE()
                            WHERE address_id = @AddressId
                        END

                    IF (@parStateId != @StateId AND @parStateId IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Address
                                SET state_id = @parStateId,
                                change_date = GETDATE()
                            WHERE address_id = @AddressId
                        END

                    IF (@parZipCode != @ZipCode AND @parZipCode IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Address
                                SET zip_code = @parZipCode,
                                change_date = GETDATE()
                            WHERE address_id = @AddressId
                        END
                END
                --Logo
            ELSE
                BEGIN
                    IF (@parEmployerId  IS NULL AND
                        @parAdminId     IS NOT NULL AND 
                        @parName        IS NOT NULL AND 
                        @parCode        IS NOT NULL AND 
                        @parStreet      IS NOT NULL AND 
                        @parCity        IS NOT NULL AND
                        @parStateId     IS NOT NULL AND
                        @parZipCode     IS NOT NULL AND
                        @parPhoneNumber IS NOT NULL AND
                        @parLogoData    IS NOT NULL)

                    INSERT INTO dbo.Logo(data, change_date)
                    VALUES (@parLogoData, GETDATE())
                    SELECT @LogoId = CAST(SCOPE_IDENTITY() AS INT)
		               --Employer
                    INSERT INTO dbo.Employer(name, code, logo_id, change_date)
                    VALUES (@parName, @parCode, @LogoId, GETDATE())
                    SELECT @EmployerId = CAST(SCOPE_IDENTITY() AS INT)
		               --BenefitsContract
                    INSERT INTO dbo.BenefitsContract(employer_id, administrator_id)
                    VALUES (@EmployerId, @parAdminId)
		               --Phone
                    INSERT INTO dbo.Phone (number, change_date)
                    VALUES (@parPhoneNumber, GETDATE())
                    SELECT @PhoneId = CAST(SCOPE_IDENTITY() AS BIGINT)
                    INSERT INTO dbo.PhoneConnection(phone_id, phone_type, object_id, object_type)
                    VALUES (@PhoneId, @AddressType, @EmployerId, @ObjectType)
		               --Address
                    INSERT INTO dbo.Address (address_type, city, street, state_id, zip_code, change_date)
                    VALUES (@AddressType, @parCity, @parStreet, @parStateId, @parZipCode, GETDATE());
                    SELECT @AddressId = CAST(SCOPE_IDENTITY() AS BIGINT)
                    INSERT INTO dbo.AddressConnection (address_id, address_type, object_id, object_type)
                    VALUES (@AddressId, @AddressType, @EmployerId, @ObjectType)
                END
    COMMIT TRAN
END TRY
BEGIN CATCH
    SET @ErrorMessage = ERROR_MESSAGE()
    RAISERROR(@ErrorMessage, 14, 1, 'Can''not insert/update value.')
    ROLLBACK TRAN
END CATCH