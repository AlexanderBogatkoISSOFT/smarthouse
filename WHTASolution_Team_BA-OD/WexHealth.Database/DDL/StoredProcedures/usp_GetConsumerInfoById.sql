﻿CREATE PROCEDURE [dbo].[usp_GetConsumerInfoById]
@participant_id INT
/*

# Select cunsomer's personal info, phone & address
# Created by [Dmitry Olekhnovich 21.02.2019]
# Last edit by [Dmitry Olekhnovich 28.02.2019]

EXEC [dbo].[usp_GetConsumerInfoById]

*/
AS
    SELECT 
           prt.participant_id,
           [individual_firstName] = ind.first_name,
           [individual_lastName] = ind.last_name,
           [individual_ssn] = ind.ssn,
    
           [phone_number] = phn.number,
    
           [securityLogin_name] = scl.[user_name],
           [securityLogin_password] = scl.[password],
    
           [address_street] = adr.street,
           [address_city] = adr.city,
           [address_zipCode] = adr.zip_code,
           st.state_id
    
    FROM dbo.Participant prt

        JOIN dbo.Individual ind
            ON prt.individual_id = ind.individual_id

        JOIN dbo.SecurityLogin scl
            ON ind.individual_id = scl.individual_id
    
        JOIN dbo.AddressConnection adrc
            ON ind.individual_id = adrc.[object_id]
            and adrc.object_type = 2
    
        JOIN dbo.[Address] adr
            ON adrc.address_id = adr.address_id
    
        JOIN dbo.[State] st
            ON adr.state_id = st.state_id
    
        JOIN dbo.PhoneConnection phnc
            ON ind.individual_id = phnc.[object_id]
            and phnc.object_type = 2
        
        JOIN dbo.Phone phn
            ON phnc.phone_id = phn.phone_id
    
    WHERE prt.participant_id = @participant_id
GO
