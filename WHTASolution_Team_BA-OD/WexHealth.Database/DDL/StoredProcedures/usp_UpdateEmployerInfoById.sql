﻿CREATE PROCEDURE [dbo].[usp_UpdateEmployerInfoById]
    @parEmployerId  INT,
    @parName        VARCHAR(50)    = NULL,
    @parCode        VARCHAR(10)    = NULL,
    @parStreet      VARCHAR(100)   = NULL,
    @parCity        VARCHAR(30)    = NULL,
    @parStateId     TINYINT        = NULL,
    @parZipCode     VARCHAR(12)    = NULL,
    @parPhoneNumber VARCHAR(15)    = NULL,
    @parLogoData    VARBINARY(MAX) = NULL       
AS
/*

# Update Employer Info by Id
# Created by [Alexander Bogatko 04.03.2019]
# Last edit by [Alexander Bogatko 04.03.2019]

EXEC [dbo].[usp_UpdateEmployerInfoById] 1

*/
BEGIN TRY
    DECLARE @ErrorMessage   VARCHAR(MAX),
            @EmployerId     INT,
            @LogoId         INT,
            @AddressId      BIGINT,
            @PhoneId        BIGINT,
            @Name           VARCHAR(50),
            @Code           VARCHAR(10),
            @LogoData       VARBINARY(MAX),
            @Street         VARCHAR(100),
            @City           VARCHAR(30),
            @StateId        TINYINT,    
            @ZipCode        VARCHAR(12),
            @PhoneNumber    VARCHAR(15),
            @AddressType    TINYINT = 1,
            @ObjectType     TINYINT = 1
    BEGIN TRAN
            IF EXISTS (SELECT 1 FROM dbo.Employer E WHERE E.employer_id = @parEmployerId)
                BEGIN
                    WITH EmployerInfo AS
                    (
                       SELECT E.employer_id         AS  'EmployerId',
                              E.name                AS  'Name',
                              E.code                AS  'Code',
                              L.logo_id             AS  'LogoId',
                              L.data                AS  'LogoData',
                              Adr.street            AS  'Street',
                              Adr.city              AS  'City',
                              Adr.address_id        AS  'AddressId',
                              S.state_id            AS  'StateId',
                              Adr.zip_code          AS  'ZipCode',
                              P.number              AS  'PhoneNumber',
                              P.phone_id            AS  'PhoneId'
       
                       FROM dbo.Employer E  JOIN dbo.Logo L
                                                ON L.logo_id = E.employer_id    
                                            JOIN dbo.AddressConnection AC
                                                ON  AC.object_id = E.employer_id AND AC.object_type = @ObjectType
                                            JOIN dbo.Address Adr
                                                ON Adr.address_id = AC.address_id
                                            JOIN dbo.State S
                                                ON S.state_id = Adr.state_id
                                            JOIN dbo.PhoneConnection PC
                                                ON PC.object_id = E.employer_id AND PC.object_type = @ObjectType
                                            JOIN dbo.Phone P
                                                ON P.phone_id = PC.phone_id
                    )
                    SELECT @Name = Name,
                           @Code = Code,
                           @LogoId = LogoId,
                           @LogoData = LogoData,
                           @AddressId = AddressId,
                           @StateId = StateId,
                           @Street = Street,
                           @City = City,
                           @PhoneId = PhoneId,
                           @PhoneNumber = PhoneNumber,
                           @ZipCode = ZipCode
                    FROM EmployerInfo
                    Where EmployerId = @parEmployerId
                    --Employer
                    IF (@parName != @Name AND @parName IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Employer
                                SET name = @parName,
                                change_date = GETDATE()
                            WHERE employer_id = @parEmployerId
                        END

                    IF (@parCode != @Code AND @parCode IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Employer
                                SET code = @parCode,
                                change_date = GETDATE()
                            WHERE employer_id = @parEmployerId
                        END
                    --Logo
                    IF (@parLogoData != @LogoData AND @parLogoData IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Logo
                                SET data = @parLogoData,
                                change_date = GETDATE()
                            WHERE logo_id = @LogoId
                        END
                    --Phone
                    IF (@parPhoneNumber != @PhoneNumber AND @parPhoneNumber IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Phone 
                                SET number = @parPhoneNumber,
                                change_date = GETDATE()
                            WHERE  phone_id = @PhoneId
                        END
                    --Address
                    IF (@parStreet != @Street AND @parStreet IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Address
                                SET street = @parStreet,
                                change_date = GETDATE()
                            WHERE address_id = @AddressId
                        END

                    IF (@parCity != @City AND @parCity IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Address
                                SET city = @parCity,
                                change_date = GETDATE()
                            WHERE address_id = @AddressId
                        END

                    IF (@parStateId != @StateId AND @parStateId IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Address
                                SET state_id = @parStateId,
                                change_date = GETDATE()
                            WHERE address_id = @AddressId
                        END

                    IF (@parZipCode != @ZipCode AND @parZipCode IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Address
                                SET zip_code = @parZipCode,
                                change_date = GETDATE()
                            WHERE address_id = @AddressId
                        END
                END
    COMMIT TRAN
END TRY
BEGIN CATCH
    SET @ErrorMessage = ERROR_MESSAGE()
    RAISERROR(@ErrorMessage, 14, 1, 'Can''not inser/update value.')
    ROLLBACK TRAN
END CATCH

GO