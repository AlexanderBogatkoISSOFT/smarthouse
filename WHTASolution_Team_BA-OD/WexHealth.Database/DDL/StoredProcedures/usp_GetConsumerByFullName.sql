﻿CREATE PROCEDURE [dbo].[usp_GetConsumerByFullName] 
@first_name VARCHAR(50) = NULL,
@last_name VARCHAR(50) = NULL,
@employer_id INT = NULL,
@participant_id INT = NULL
/*

# Select consumers by their first_name, last_name or employer if something from this exists
# Created by [Dmitry Olekhnovich 20.02.2019]
# Last edit by [Dmitry Olekhnovich 28.02.2019]

EXEC [dbo].[usp_GetConsumerByFullName]

EXEC [dbo].[usp_GetConsumerByFullName] @first_name = 'Slava'

EXEC [dbo].[usp_GetConsumerByFullName] @employer_id = 1

*/
AS

    SET @first_name = ISNULL(@first_name, '') + '%'
    SET @last_name = ISNULL(@last_name, '') + '%'

    SELECT 
           ind.individual_id,
           [individual_firstName] = ind.first_name,
           [individual_lastName] = ind.last_name,
           [individual_ssn] = ind.ssn,
           
           [employer_name] = emp.[name],
           [employer_code] = emp.[code],
           prt.participant_id,
           emp.employer_id
    
    FROM dbo.Individual ind

        JOIN dbo.Participant prt
            ON ind.individual_id = prt.participant_id

        JOIN dbo.Employer emp
            ON prt.employer_id = emp.employer_id

    WHERE ind.first_name like @first_name
    and ind.last_name like @last_name
    and (emp.employer_id = @employer_id OR @employer_id IS NULL)
    and (prt.participant_id = @participant_id OR @participant_id IS NULL)
GO
