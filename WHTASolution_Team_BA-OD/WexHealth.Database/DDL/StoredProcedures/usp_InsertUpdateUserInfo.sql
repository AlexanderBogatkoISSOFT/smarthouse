﻿CREATE PROCEDURE [dbo].[usp_InsertUpdateUserInfo]
    @parLoginId             INT         = NULL,
    @parEmployerId          INT         = NULL,
    @parLoginType           TINYINT     = NULL,
    @parFirstName           VARCHAR(50) = NULL,
    @parLastName            VARCHAR(50) = NULL,
    @parUserName            VARCHAR(50) = NULL,
    @parEmail               VARCHAR(50) = NULL,
    @parPassword            VARCHAR(40) = NULL
AS
/*

# Insert/Update Individual
# Created by [Alexander Bogatko 05.03.2019]
# Last edit by [Alexander Bogatko 05.03.2019]

*/
BEGIN TRY
    DECLARE @ErrorMessage   VARCHAR(MAX),
            @IndividualId   INT,
            @FirstName      VARCHAR(50),
            @LastName       VARCHAR(50),
            @UserName       VARCHAR(50),
            @Email          VARCHAR(50),
            @Password       VARCHAR(40),
			@DomainId		INT

   BEGIN TRAN
            IF EXISTS (SELECT 1 FROM dbo.SecurityLogin SL WHERE SL.login_id = @parLoginId)
                BEGIN
                    SELECT @IndividualId = SL.individual_id,
                           @UserName = SL.user_name,
                           @Password = SL.password,        
                           @FirstName = I.first_name,        
                           @LastName = I.last_name,       
                           @Email = I.email
       
                    FROM dbo.SecurityLogin SL JOIN dbo.Individual I
                                                 ON I.individual_id = SL.individual_id
                    WHERE  SL.login_id = @parLoginId                             

                    --Security Login
                    IF (@parUserName != @UserName AND @parUserName IS NOT NULL)
                        BEGIN
                            UPDATE dbo.SecurityLogin
                                SET user_name = @parUserName,
                                change_date = GETDATE()
                            WHERE login_id = @parLoginId
                        END

                    IF (@parPassword != @Password AND @parPassword IS NOT NULL)
                        BEGIN
                            UPDATE dbo.SecurityLogin
                                SET password = @parPassword,
                                change_date = GETDATE()
                            WHERE login_id = @parLoginId
                        END
                    --Individual
                    IF (@parFirstName != @FirstName AND @parFirstName IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Individual
                                SET first_name = @parFirstName,
                                change_date = GETDATE()
                            WHERE individual_id = @IndividualId
                        END

                    IF (@parLastName != @LastName AND @parLastName IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Individual
                                SET last_name = @parLastName,
                                change_date = GETDATE()
                            WHERE individual_id = @IndividualId
                        END

                    IF (@parEmail != @Email AND @parEmail IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Individual
                                SET email = @parEmail,
                                change_date = GETDATE()
                            WHERE individual_id = @IndividualId
                        END
                END
            ELSE
                BEGIN

                    IF (@parLoginId    IS NULL AND
                        @parEmployerId IS NOT NULL AND
                        @parLoginType  IS NOT NULL AND
                        @parFirstName  IS NOT NULL AND
                        @parLastName   IS NOT NULL AND
                        @parUserName   IS NOT NULL AND
                        @parEmail      IS NOT NULL AND
                        @parPassword   IS NOT NULL)
                    BEGIN            
                        SELECT @DomainId = D.domain_id 
                        FROM dbo.BenefitsContract BC    JOIN dbo.Administrator A
                                                            ON  A.administrator_id = BC.administrator_id
                                                        JOIN dbo.Domain D
                                                            ON D.domain_id = A.administrator_id              
                        WHERE BC.employer_id = @parEmployerId


                        --Individual
                        INSERT INTO dbo.Individual(first_name, last_name, email, change_date)
                        VALUES  (@parFirstName, @parLastName, @parEmail, GETDATE())
                        SELECT @IndividualId = CAST(SCOPE_IDENTITY() AS INT)
                        --SecurityLogin
                        INSERT INTO dbo.SecurityLogin(user_name, password, individual_id, login_type, employer_id, domain_id, change_date)
                        VALUES (@parUserName, @parPassword, @IndividualId, @parLoginType, @parEmployerId, @DomainId, GETDATE())
                    END
                END
    COMMIT TRAN
END TRY
BEGIN CATCH
    SET @ErrorMessage = ERROR_MESSAGE()
    RAISERROR(@ErrorMessage, 14, 1, 'Can''not insert value.')
    ROLLBACK TRAN
END CATCH