﻿CREATE PROCEDURE [dbo].[usp_InsertUserInfo]
    @parFirstName           VARCHAR(50),
    @parLastName            VARCHAR(50),
    @parUserName            VARCHAR(50),
    @parEmail               VARCHAR(50),
    @parPassword            VARCHAR(40),
    @parEmployerId          INT,
    @parLoginType           TINYINT
AS
/*

# Insert Individual & SecurityLogin Info
# Created by [Alexander Bogatko 04.03.2019]
# Last edit by [Alexander Bogatko 04.03.2019]

EXEC usp_InsertUserInfo

*/
BEGIN TRY
    DECLARE @ErrorMessage   VARCHAR(MAX),
            @IndividualId   INT,
			@DomainId		INT

    BEGIN TRAN
		
		SELECT @DomainId = D.domain_id 
        FROM dbo.BenefitsContract BC    JOIN dbo.Administrator A
                                            ON  A.administrator_id = BC.administrator_id
                                        JOIN dbo.Domain D
                                            ON D.domain_id = A.administrator_id              
        WHERE BC.employer_id = @parEmployerId


        --Individual
        INSERT INTO dbo.Individual(first_name, last_name, email, change_date)
        VALUES  (@parFirstName, @parLastName, @parEmail, GETDATE())
        SELECT @IndividualId = CAST(SCOPE_IDENTITY() AS INT)
        --SecurityLogin
        INSERT INTO dbo.SecurityLogin(user_name, password, individual_id, login_type, employer_id, domain_id, change_date)
        VALUES (@parUserName, @parPassword, @IndividualId, @parLoginType, @parEmployerId, @DomainId, GETDATE())

    COMMIT TRAN
END TRY
BEGIN CATCH
    SET @ErrorMessage = ERROR_MESSAGE()
    RAISERROR(@ErrorMessage, 14, 1, 'Can''not insert value.')
    ROLLBACK TRAN
END CATCH

GO