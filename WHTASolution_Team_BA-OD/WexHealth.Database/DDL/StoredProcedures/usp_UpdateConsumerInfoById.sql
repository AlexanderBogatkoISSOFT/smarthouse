﻿CREATE PROCEDURE [dbo].[usp_UpdateConsumerInfoById]
    @parConsumerId          INT,
    @parFirstName           VARCHAR(50) = NULL,
    @parLastName            VARCHAR(50) = NULL,
    @parSSN                 VARCHAR(9)  = NULL,
    @parUserName            VARCHAR(50) = NULL,
    @parPassword            VARCHAR(40) = NULL,
    @parStreet              VARCHAR(100)= NULL,
    @parCity                VARCHAR(30) = NULL,
    @parStateId             TINYINT     = NULL,
    @parZipCode             VARCHAR(12) = NULL,
    @parPhoneNumber         VARCHAR(15) = NULL
AS
/*

# Update ConsumerInfo By Id
# Created by [Alexander Bogatko 04.03.2019]
# Last edit by [Alexander Bogatko 04.03.2019]

EXEC [dbo].[usp_UpdateConsumerInfoById] 1

*/
BEGIN TRY
    DECLARE @ErrorMessage   VARCHAR(MAX),
            @LoginId        INT,
            @IndividualId   INT,
            @AddressId      BIGINT,
            @PhoneId        BIGINT,
            @FirstName      VARCHAR(50),
            @LastName       VARCHAR(50),
            @SSN            VARCHAR(9),
            @UserName       VARCHAR(50),
            @Password       VARCHAR(40),
            @Street         VARCHAR(100),
            @City           VARCHAR(30),
            @StateId        TINYINT,    
            @ZipCode        VARCHAR(12),
            @PhoneNumber    VARCHAR(15),
            @ObjectType     TINYINT = 2
    BEGIN TRAN
            IF EXISTS (SELECT 1 FROM dbo.Participant P WHERE P.participant_id = @parConsumerId)
                BEGIN
                    WITH ConsumerInfo AS
                    (
                       SELECT Par.participant_id    AS  'ConsumerId',
                              I.first_name          AS  'FirstName',
                              I.last_name           AS  'LastName',
                              I.ssn                 AS  'SSN',
                              I.individual_id       AS  'IndividualId',
                              SL.user_name          AS  'UserName',
                              SL.password           AS  'Password',
                              SL.login_id           AS  'LoginId',
                              Adr.street            AS  'Street',
                              Adr.city              AS  'City',
                              Adr.address_id        AS  'AddressId',
                              S.state_id            AS  'StateId',
                              Adr.zip_code          AS  'ZipCode',
                              P.number              AS  'PhoneNumber',
                              P.phone_id            AS  'PhoneId'
       
                       FROM dbo.Participant Par     JOIN dbo.Individual I
                                                        ON I.individual_id = Par.individual_id
                                                    JOIN dbo.SecurityLogin SL
                                                        ON SL.individual_id = I.individual_id    
                                                    JOIN dbo.AddressConnection AC
                                                        ON  AC.object_id = Par.participant_id AND AC.object_type = @ObjectType
                                                    JOIN dbo.Address Adr
                                                        ON Adr.address_id = AC.address_id
                                                    JOIN dbo.State S
                                                        ON S.state_id = Adr.state_id
                                                    JOIN dbo.PhoneConnection PC
                                                        ON PC.object_id = Par.participant_id AND PC.object_type = @ObjectType
                                                    JOIN dbo.Phone P
                                                        ON P.phone_id = PC.phone_id
                    )
                    SELECT @FirstName = FirstName,
                           @LastName = LastName,
                           @SSN = SSN,
                           @IndividualId = IndividualId,
                           @UserName = UserName,
                           @Password = Password,
                           @LoginId = LoginId,
                           @AddressId = AddressId,
                           @StateId = StateId,
                           @Street = Street,
                           @City = City,
                           @PhoneId = PhoneId,
                           @PhoneNumber = PhoneNumber,
                           @ZipCode = ZipCode
                    FROM ConsumerInfo
                    Where ConsumerId = @parConsumerId
                    --Individual
                    IF (@parFirstName != @FirstName AND @parFirstName IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Individual 
                                SET first_name = @parFirstName,
                                change_date = GETDATE()
                            WHERE individual_id = @IndividualId
                        END

                    IF (@parLastName != @LastName AND @parLastName IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Individual 
                                SET last_name = @parLastName,
                                change_date = GETDATE()
                            WHERE individual_id = @IndividualId
                        END

                    IF (@parSSN != @SSN AND @parSSN IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Individual 
                                SET ssn = @parSSN,
                                change_date = GETDATE()
                            WHERE individual_id = @IndividualId
                        END
                    --SecurityLogin
                    IF (@parUserName != @UserName AND @parUserName IS NOT NULL)
                        BEGIN
                            UPDATE dbo.SecurityLogin 
                                SET user_name = @parUserName,
                                change_date = GETDATE()
                            WHERE login_id = @LoginId
                        END

                    IF (@parPassword != @Password AND @parPassword IS NOT NULL)
                        BEGIN
                            UPDATE dbo.SecurityLogin 
                                SET password = @parPassword,
                                change_date = GETDATE()
                            WHERE login_id = @LoginId
                        END
                    --Phone
                    IF (@parPhoneNumber != @PhoneNumber AND @parPhoneNumber IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Phone 
                                SET number = @parPhoneNumber,
                                change_date = GETDATE()
                            WHERE  phone_id = @PhoneId
                        END
                    --Address
                    IF (@parStreet != @Street AND @parStreet IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Address
                                SET street = @parStreet,
                                change_date = GETDATE()
                            WHERE address_id = @AddressId
                        END

                    IF (@parCity != @City AND @parCity IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Address
                                SET city = @parCity,
                                change_date = GETDATE()
                            WHERE address_id = @AddressId
                        END

                    IF (@parStateId != @StateId AND @parStateId IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Address
                                SET state_id = @parStateId,
                                change_date = GETDATE()
                            WHERE address_id = @AddressId
                        END

                    IF (@parZipCode != @ZipCode AND @parZipCode IS NOT NULL)
                        BEGIN
                            UPDATE dbo.Address
                                SET zip_code = @parZipCode,
                                change_date = GETDATE()
                            WHERE address_id = @AddressId
                        END
                END
    COMMIT TRAN
END TRY
BEGIN CATCH
    SET @ErrorMessage = ERROR_MESSAGE()
    RAISERROR(@ErrorMessage, 14, 1, 'Can''not update value.')
    ROLLBACK TRAN
END CATCH

GO