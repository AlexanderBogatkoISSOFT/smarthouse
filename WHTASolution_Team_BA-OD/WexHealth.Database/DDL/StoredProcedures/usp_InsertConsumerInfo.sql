﻿CREATE PROCEDURE [dbo].[usp_InsertConsumerInfo]
    @parFirstName           VARCHAR(50),
    @parLastName            VARCHAR(50),
    @parSSN                 VARCHAR(9)  = NULL,
    @parUserName            VARCHAR(50),
    @parEmail               VARCHAR(50),
    @parPassword            VARCHAR(40),
    @parEmployerId          INT,
    @parStreet              VARCHAR(100),
    @parCity                VARCHAR(30),
    @parStateId             TINYINT,
    @parZipCode             VARCHAR(12),
    @parPhoneNumber         VARCHAR(15)
AS
/*

# Insert Consumer Info
# Created by [Alexander Bogatko 04.03.2019]
# Last edit by [Alexander Bogatko 04.03.2019]

EXEC usp_InsertConsumerInfo @parFirstName = 'TestFirstName',
                            @parLastName = 'TestLastName',
                            @parSSN = '123456789',
                            @parUserName = 'UserNameT',
                            @parPassword = '123',
                            @parEmail = 'Test@gmail.com',
                            @parEmployerId = '1',
                            @parStreet = 'TestStreet',
                            @parCity = 'TestCity',
                            @parStateId = 1,
                            @parZipCode = '7788',
                            @parPhoneNumber = '5553535'

*/
BEGIN TRY
    DECLARE @ErrorMessage   VARCHAR(MAX),
            @LoginId        INT,
            @ParticipantId  INT,
            @IndividualId   INT,
            @AddressId      BIGINT,
            @DomainId       SMALLINT,
            @PhoneId        BIGINT,
            @FirstName      VARCHAR(50),
            @LastName       VARCHAR(50),
            @SSN            VARCHAR(9),
            @UserName       VARCHAR(50),
            @Password       VARCHAR(40),
            @Street         VARCHAR(100),
            @City           VARCHAR(30),
            @StateId        TINYINT,    
            @ZipCode        VARCHAR(12),
            @PhoneNumber    VARCHAR(15),
            @LoginType      TINYINT = 2,
            @AddressType    TINYINT = 1,
            @ObjectType     TINYINT = 2

    BEGIN TRAN

        SELECT @DomainId = D.domain_id 
        FROM dbo.BenefitsContract BC    JOIN dbo.Administrator A
                                            ON  A.administrator_id = BC.administrator_id
                                        JOIN dbo.Domain D
                                            ON D.domain_id = A.administrator_id              
        WHERE BC.employer_id = @parEmployerId
        --Individual
        INSERT INTO dbo.Individual(first_name, last_name, email, ssn, change_date)
        VALUES  (@parFirstName, @parLastName, @parEmail, @parSSN, GETDATE())
        SELECT @IndividualId = CAST(SCOPE_IDENTITY() AS INT)
		--SecurityLogin
        INSERT INTO dbo.SecurityLogin(user_name, password, individual_id, login_type, employer_id, domain_id, change_date)
        VALUES (@parUserName, @parPassword, @IndividualId, @LoginType, @parEmployerId, @DomainId, GETDATE())
		--Participant
        INSERT INTO dbo.Participant(individual_id, employer_id)
        VALUES (@IndividualId, @parEmployerId)
        SELECT @ParticipantId = CAST(SCOPE_IDENTITY() AS INT)
		--Phone
        INSERT INTO dbo.Phone (number, change_date)
        VALUES (@parPhoneNumber, GETDATE())
        SELECT @PhoneId = CAST(SCOPE_IDENTITY() AS BIGINT)
        INSERT INTO dbo.PhoneConnection(phone_id, phone_type, object_id, object_type)
        VALUES (@PhoneId, @AddressType, @ParticipantId, @ObjectType)
		--Address
        INSERT INTO dbo.Address (address_type, city, street, state_id, zip_code, change_date)
        VALUES (@AddressType, @parCity, @parStreet, @parStateId, @parZipCode, GETDATE());
        SELECT @AddressId = CAST(SCOPE_IDENTITY() AS BIGINT)
        INSERT INTO dbo.AddressConnection (address_id, address_type, object_id, object_type)
        VALUES (@AddressId, @AddressType, @ParticipantId, @ObjectType)

    COMMIT TRAN
END TRY
BEGIN CATCH
    SET @ErrorMessage = ERROR_MESSAGE()
    RAISERROR(@ErrorMessage, 14, 1, 'Can''not insert value.')
    ROLLBACK TRAN
END CATCH

GO