﻿CREATE PROCEDURE [dbo].[usp_GetEmployersNames]
/*

# Selects full list of employers' names for admin with the code
# Created by [Dmitry Olekhnovich 20.02.2019]
# Last edit by [Dmitry Olekhnovich 25.02.2019]

EXEC [dbo].[usp_GetEmployersNames]

*/
AS
    SELECT 
           emp.employer_id,
           [employer_fullname] = emp.[name] + ' (' + emp.code + ')',
           [employer_name] = emp.[name],
           [employer_code] = emp.code
    
    FROM dbo.Employer emp
GO