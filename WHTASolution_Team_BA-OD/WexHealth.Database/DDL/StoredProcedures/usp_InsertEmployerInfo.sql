﻿CREATE PROCEDURE [dbo].[usp_InsertEmployerInfo]
    @parAdminId     INT,
    @parName        VARCHAR(50),
    @parCode        VARCHAR(10),
    @parStreet      VARCHAR(100),
    @parCity        VARCHAR(30),
    @parStateId     TINYINT,
    @parZipCode     VARCHAR(12),
    @parPhoneNumber VARCHAR(15),
    @parLogoData    VARBINARY(MAX)

AS
/*

# Insert Employer Info by Admin Id 
# Created by [Alexander Bogatko 04.03.2019]
# Last edit by [Alexander Bogatko 04.03.2019]

DECLARE @Data VARBINARY(MAX) = CAST(123456789101112 AS VARBINARY(MAX))
EXEC usp_InsertEmployerInfo 1, 'TestEmployer', 'TestCode', 'Str', 'CT', 1, '5555', '5553535', @Data

*/
BEGIN TRY
    DECLARE @ErrorMessage   VARCHAR(MAX),
            @LoginId        INT,
            @LogoId         INT,
            @EmployerId     INT,
            @AddressId      BIGINT,
            @PhoneId        BIGINT,
            @Street         VARCHAR(100),
            @City           VARCHAR(30),
            @StateId        TINYINT,    
            @ZipCode        VARCHAR(12),
            @PhoneNumber    VARCHAR(15),
            @AddressType    TINYINT = 1,
            @ObjectType     TINYINT = 1

    BEGIN TRAN
		--Logo
        INSERT INTO dbo.Logo(data, change_date)
        VALUES (@parLogoData, GETDATE())
        SELECT @LogoId = CAST(SCOPE_IDENTITY() AS INT)
		--Employer
        INSERT INTO dbo.Employer(name, code, logo_id, change_date)
        VALUES (@parName, @parCode, @LogoId, GETDATE())
        SELECT @EmployerId = CAST(SCOPE_IDENTITY() AS INT)
		--BenefitsContract
        INSERT INTO dbo.BenefitsContract(employer_id, administrator_id)
        VALUES (@EmployerId, @parAdminId)
		--Phone
        INSERT INTO dbo.Phone (number, change_date)
        VALUES (@parPhoneNumber, GETDATE())
        SELECT @PhoneId = CAST(SCOPE_IDENTITY() AS BIGINT)
        INSERT INTO dbo.PhoneConnection(phone_id, phone_type, object_id, object_type)
        VALUES (@PhoneId, @AddressType, @EmployerId, @ObjectType)
		--Address
        INSERT INTO dbo.Address (address_type, city, street, state_id, zip_code, change_date)
        VALUES (@AddressType, @parCity, @parStreet, @parStateId, @parZipCode, GETDATE());
        SELECT @AddressId = CAST(SCOPE_IDENTITY() AS BIGINT)
        INSERT INTO dbo.AddressConnection (address_id, address_type, object_id, object_type)
        VALUES (@AddressId, @AddressType, @EmployerId, @ObjectType)

    COMMIT TRAN
END TRY
BEGIN CATCH
    SET @ErrorMessage = ERROR_MESSAGE()
    RAISERROR(@ErrorMessage, 14, 1, 'Can''not inser/update value.')
    ROLLBACK TRAN
END CATCH

GO