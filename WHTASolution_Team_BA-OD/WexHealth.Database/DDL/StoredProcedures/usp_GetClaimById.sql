﻿CREATE PROCEDURE dbo.usp_GetClaimById
	@parClaimId			INT

/*

# Find Claim By Id
# Created by [Alexander Bogatko 28.02.2019]
# Last edit by [Alexander Bogatko 28.02.2019]

EXEC [dbo].[usp_GetClaimById] 1

*/

AS
BEGIN
    SELECT  C.claim_number                      AS 'ClaimNumber',
            I.first_name + ' ' + I.last_name    AS 'ConsumerName', 
            C.participant_id                    AS 'ConsumerId',
            C.date_of_service                   AS 'DateOfService',
            CS.status                           AS 'Status',
            C.amount                            AS 'Amount',
            BO.[plan]                           AS 'Plan',
            Ph.number                           AS 'PhoneNumber'

    FROM Claim C    JOIN Participant P
                        ON P.participant_id = C.participant_id
                    JOIN Individual I
                        ON I.individual_id = P.individual_id
                    JOIN ClaimStatus CS
                        ON CS.claim_status_id = C.claim_status_id
                    JOIN BenefitsOffering BO
                        ON BO.benefits_offering_id = C.benefits_offering_id
                    JOIN dbo.PhoneConnection PC
                        ON P.participant_id = PC.object_id  and PC.object_type = 2    
                    JOIN dbo.Phone Ph
                        ON Ph.phone_id = PC.phone_id
    WHERE C.claim_id = @parClaimId
END