﻿using System.Collections.Generic;
using WexHealth.IDomainModels;

namespace WexHealth.IRepositories
{
    public interface IConsumerInfoRepository
    {
        IEnumerable<IConsumerInfo> GetAll();
    }
}