﻿using System.Security.Principal;
using System.Windows.Forms;
using WexHealth.Core.Views;

namespace TPASetupTool
{
    public abstract class ViewBase<TPresenter> : Form, IView
    {
        protected TPresenter _presenter;

        protected ViewBase(IPrincipal principal)
        {
            Principal = principal;
#warning Thread.CurrentPrincipal;
        }

        public IPrincipal Principal { get; }

    }
}
