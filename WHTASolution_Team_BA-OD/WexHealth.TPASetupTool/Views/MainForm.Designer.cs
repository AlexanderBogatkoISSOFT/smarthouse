﻿namespace TPASetupTool.Views
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statAdmin = new System.Windows.Forms.StatusStrip();
            this.mnsMain = new System.Windows.Forms.MenuStrip();
            this.tsmFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmChangeAdministrator = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmExit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmProject = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmGeneral = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmUsers = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmManageUsers = new System.Windows.Forms.ToolStripMenuItem();
            this.mnsMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // statAdmin
            // 
            this.statAdmin.Location = new System.Drawing.Point(0, 448);
            this.statAdmin.Name = "statAdmin";
            this.statAdmin.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.statAdmin.Size = new System.Drawing.Size(638, 22);
            this.statAdmin.TabIndex = 5;
            this.statAdmin.Text = "statusStrip1";
            // 
            // mnsMain
            // 
            this.mnsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmFile,
            this.tsmProject,
            this.tsmUsers});
            this.mnsMain.Location = new System.Drawing.Point(0, 0);
            this.mnsMain.Name = "mnsMain";
            this.mnsMain.Size = new System.Drawing.Size(638, 24);
            this.mnsMain.TabIndex = 0;
            this.mnsMain.Text = "menuStrip1";
            // 
            // tsmFile
            // 
            this.tsmFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmChangeAdministrator,
            this.toolStripSeparator1,
            this.tsmExit});
            this.tsmFile.Name = "tsmFile";
            this.tsmFile.Size = new System.Drawing.Size(37, 20);
            this.tsmFile.Text = "File";
            // 
            // tsmChangeAdministrator
            // 
            this.tsmChangeAdministrator.Name = "tsmChangeAdministrator";
            this.tsmChangeAdministrator.Size = new System.Drawing.Size(191, 22);
            this.tsmChangeAdministrator.Text = "Change Administrator";
            this.tsmChangeAdministrator.Click += new System.EventHandler(this.tsmChangeAdministrator_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(188, 6);
            // 
            // tsmExit
            // 
            this.tsmExit.Name = "tsmExit";
            this.tsmExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.tsmExit.Size = new System.Drawing.Size(191, 22);
            this.tsmExit.Text = "Exit";
            this.tsmExit.Click += new System.EventHandler(this.tsmExit_Click);
            // 
            // tsmProject
            // 
            this.tsmProject.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmGeneral,
            this.toolStripSeparator2,
            this.tsmSettings});
            this.tsmProject.Name = "tsmProject";
            this.tsmProject.Size = new System.Drawing.Size(56, 20);
            this.tsmProject.Text = "Project";
            // 
            // tsmGeneral
            // 
            this.tsmGeneral.Name = "tsmGeneral";
            this.tsmGeneral.Size = new System.Drawing.Size(116, 22);
            this.tsmGeneral.Text = "General";
            this.tsmGeneral.Click += new System.EventHandler(this.tsmGeneral_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(113, 6);
            // 
            // tsmSettings
            // 
            this.tsmSettings.Name = "tsmSettings";
            this.tsmSettings.Size = new System.Drawing.Size(116, 22);
            this.tsmSettings.Text = "Settings";
            this.tsmSettings.Click += new System.EventHandler(this.tsmSettings_Click);
            // 
            // tsmUsers
            // 
            this.tsmUsers.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmManageUsers});
            this.tsmUsers.Name = "tsmUsers";
            this.tsmUsers.Size = new System.Drawing.Size(47, 20);
            this.tsmUsers.Text = "Users";
            // 
            // tsmManageUsers
            // 
            this.tsmManageUsers.Name = "tsmManageUsers";
            this.tsmManageUsers.Size = new System.Drawing.Size(148, 22);
            this.tsmManageUsers.Text = "Manage Users";
            this.tsmManageUsers.Click += new System.EventHandler(this.tsmManageUsers_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 470);
            this.Controls.Add(this.statAdmin);
            this.Controls.Add(this.mnsMain);
            this.MainMenuStrip = this.mnsMain;
            this.Name = "MainForm";
            this.Text = "TPA Setup";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.mnsMain.ResumeLayout(false);
            this.mnsMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStripMenuItem tsmChangeAdministrator;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tsmExit;
        private System.Windows.Forms.ToolStripMenuItem tsmGeneral;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem tsmSettings;
        private System.Windows.Forms.ToolStripMenuItem tsmManageUsers;
        protected System.Windows.Forms.StatusStrip statAdmin;
        protected System.Windows.Forms.MenuStrip mnsMain;
        protected System.Windows.Forms.ToolStripMenuItem tsmFile;
        protected System.Windows.Forms.ToolStripMenuItem tsmProject;
        protected System.Windows.Forms.ToolStripMenuItem tsmUsers;
    }
}

