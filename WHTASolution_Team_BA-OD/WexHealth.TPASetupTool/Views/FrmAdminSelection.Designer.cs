﻿namespace TPASetupTool.Views
{
    partial class FrmAdminSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUpdate = new System.Windows.Forms.Button();
            this.cmbAdministrators = new System.Windows.Forms.ComboBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.lblForCmbAdministrators = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(261, 31);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 10;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // cmbAdministrators
            // 
            this.cmbAdministrators.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAdministrators.FormattingEnabled = true;
            this.cmbAdministrators.Location = new System.Drawing.Point(12, 31);
            this.cmbAdministrators.Name = "cmbAdministrators";
            this.cmbAdministrators.Size = new System.Drawing.Size(243, 21);
            this.cmbAdministrators.TabIndex = 5;
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(12, 65);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(324, 23);
            this.btnCreate.TabIndex = 15;
            this.btnCreate.Text = "Create New Administrator";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // lblForCmbAdministrators
            // 
            this.lblForCmbAdministrators.AutoSize = true;
            this.lblForCmbAdministrators.Location = new System.Drawing.Point(12, 13);
            this.lblForCmbAdministrators.Name = "lblForCmbAdministrators";
            this.lblForCmbAdministrators.Size = new System.Drawing.Size(151, 13);
            this.lblForCmbAdministrators.TabIndex = 0;
            this.lblForCmbAdministrators.Text = "Select Administrator to update:";
            // 
            // FrmAdminSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 102);
            this.Controls.Add(this.lblForCmbAdministrators);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.cmbAdministrators);
            this.Controls.Add(this.btnUpdate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAdminSelection";
            this.Text = "Select Administrator";
            this.Load += new System.EventHandler(this.FrmAdminSelection_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.ComboBox cmbAdministrators;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label lblForCmbAdministrators;
    }
}