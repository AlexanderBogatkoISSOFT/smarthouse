﻿using System.Collections.Generic;
using System.Windows.Forms;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Views.Tools.TPASetupTool.Interfaces;

namespace TPASetupTool.Views
{
    public partial class FrmAdminSelection : Form, IAdminSelectionView //ViewBase<IAdminSelectionPresenter>
    {
        private IAdminSelectionPresenter _presenter;

        public int SelectedAdministrator
        {
            get => (int)cmbAdministrators.SelectedItem;
            set => cmbAdministrators.SelectedItem = value;
        }

        public IDictionary<int, string> Administrators
        {
            get => cmbAdministrators.DataSource as IDictionary<int, string>;
            set
            {
                cmbAdministrators.DataSource = value;
                cmbAdministrators.ValueMember = "Key";
                cmbAdministrators.DisplayMember = "Value";
            }
        }

#warning Injection FrmAdminSelection
        public FrmAdminSelection(IAdminSelectionPresenter presenter)// : base(null)
        {
            InitializeComponent();

            _presenter = presenter;//AutofacConfig.Instance.AdminSelectionPresenter;
            //_presenter.AdminSelectionView = this;
        }

        private void btnCreate_Click(object sender, System.EventArgs e)
        {
            var frm = new FrmEditAdministratorData(null);
            frm.Show();
            Close();
        }

        private void btnUpdate_Click(object sender, System.EventArgs e)
        {
            var admin = _presenter?.GetSelectedAdministrator();
            var frm = new FrmEditAdministratorData(null);
            frm.Show();
            Close();
        }

        private void FrmAdminSelection_Load(object sender, System.EventArgs e)
        {
#warning IsPostBack in WinForms?
            _presenter?.InitView(false);
        }
    }
}
