﻿namespace TPASetupTool
{
    partial class FrmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grbCreateConsumers = new System.Windows.Forms.GroupBox();
            this.rbtnPreventCreateConsumers = new System.Windows.Forms.RadioButton();
            this.rbtnAllowCreateConsumers = new System.Windows.Forms.RadioButton();
            this.grbFileClaims = new System.Windows.Forms.GroupBox();
            this.rbtnPreventFileClaims = new System.Windows.Forms.RadioButton();
            this.rbtnAllowFileClaims = new System.Windows.Forms.RadioButton();
            this.grbReports = new System.Windows.Forms.GroupBox();
            this.rbtnPreventReports = new System.Windows.Forms.RadioButton();
            this.rbtnAllowReports = new System.Windows.Forms.RadioButton();
            this.btnSave = new System.Windows.Forms.Button();
            this.grbCreateConsumers.SuspendLayout();
            this.grbFileClaims.SuspendLayout();
            this.grbReports.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbCreateConsumers
            // 
            this.grbCreateConsumers.Controls.Add(this.rbtnPreventCreateConsumers);
            this.grbCreateConsumers.Controls.Add(this.rbtnAllowCreateConsumers);
            this.grbCreateConsumers.Location = new System.Drawing.Point(12, 12);
            this.grbCreateConsumers.Name = "grbCreateConsumers";
            this.grbCreateConsumers.Size = new System.Drawing.Size(136, 50);
            this.grbCreateConsumers.TabIndex = 0;
            this.grbCreateConsumers.TabStop = false;
            this.grbCreateConsumers.Text = "Create Consumers:";
            // 
            // rbtnPreventCreateConsumers
            // 
            this.rbtnPreventCreateConsumers.AutoSize = true;
            this.rbtnPreventCreateConsumers.Checked = true;
            this.rbtnPreventCreateConsumers.Location = new System.Drawing.Point(62, 19);
            this.rbtnPreventCreateConsumers.Name = "rbtnPreventCreateConsumers";
            this.rbtnPreventCreateConsumers.Size = new System.Drawing.Size(62, 17);
            this.rbtnPreventCreateConsumers.TabIndex = 5;
            this.rbtnPreventCreateConsumers.TabStop = true;
            this.rbtnPreventCreateConsumers.Text = "Prevent";
            this.rbtnPreventCreateConsumers.UseVisualStyleBackColor = true;
            // 
            // rbtnAllowCreateConsumers
            // 
            this.rbtnAllowCreateConsumers.AutoSize = true;
            this.rbtnAllowCreateConsumers.Location = new System.Drawing.Point(6, 19);
            this.rbtnAllowCreateConsumers.Name = "rbtnAllowCreateConsumers";
            this.rbtnAllowCreateConsumers.Size = new System.Drawing.Size(50, 17);
            this.rbtnAllowCreateConsumers.TabIndex = 0;
            this.rbtnAllowCreateConsumers.Text = "Allow";
            this.rbtnAllowCreateConsumers.UseVisualStyleBackColor = true;
            // 
            // grbFileClaims
            // 
            this.grbFileClaims.Controls.Add(this.rbtnPreventFileClaims);
            this.grbFileClaims.Controls.Add(this.rbtnAllowFileClaims);
            this.grbFileClaims.Location = new System.Drawing.Point(154, 12);
            this.grbFileClaims.Name = "grbFileClaims";
            this.grbFileClaims.Size = new System.Drawing.Size(136, 50);
            this.grbFileClaims.TabIndex = 5;
            this.grbFileClaims.TabStop = false;
            this.grbFileClaims.Text = "File Claims:";
            // 
            // rbtnPreventFileClaims
            // 
            this.rbtnPreventFileClaims.AutoSize = true;
            this.rbtnPreventFileClaims.Checked = true;
            this.rbtnPreventFileClaims.Location = new System.Drawing.Point(62, 19);
            this.rbtnPreventFileClaims.Name = "rbtnPreventFileClaims";
            this.rbtnPreventFileClaims.Size = new System.Drawing.Size(62, 17);
            this.rbtnPreventFileClaims.TabIndex = 5;
            this.rbtnPreventFileClaims.TabStop = true;
            this.rbtnPreventFileClaims.Text = "Prevent";
            this.rbtnPreventFileClaims.UseVisualStyleBackColor = true;
            // 
            // rbtnAllowFileClaims
            // 
            this.rbtnAllowFileClaims.AutoSize = true;
            this.rbtnAllowFileClaims.Location = new System.Drawing.Point(6, 19);
            this.rbtnAllowFileClaims.Name = "rbtnAllowFileClaims";
            this.rbtnAllowFileClaims.Size = new System.Drawing.Size(50, 17);
            this.rbtnAllowFileClaims.TabIndex = 0;
            this.rbtnAllowFileClaims.Text = "Allow";
            this.rbtnAllowFileClaims.UseVisualStyleBackColor = true;
            // 
            // grbReports
            // 
            this.grbReports.Controls.Add(this.rbtnPreventReports);
            this.grbReports.Controls.Add(this.rbtnAllowReports);
            this.grbReports.Location = new System.Drawing.Point(296, 12);
            this.grbReports.Name = "grbReports";
            this.grbReports.Size = new System.Drawing.Size(136, 50);
            this.grbReports.TabIndex = 10;
            this.grbReports.TabStop = false;
            this.grbReports.Text = "Reports:";
            // 
            // rbtnPreventReports
            // 
            this.rbtnPreventReports.AutoSize = true;
            this.rbtnPreventReports.Checked = true;
            this.rbtnPreventReports.Location = new System.Drawing.Point(62, 19);
            this.rbtnPreventReports.Name = "rbtnPreventReports";
            this.rbtnPreventReports.Size = new System.Drawing.Size(62, 17);
            this.rbtnPreventReports.TabIndex = 5;
            this.rbtnPreventReports.TabStop = true;
            this.rbtnPreventReports.Text = "Prevent";
            this.rbtnPreventReports.UseVisualStyleBackColor = true;
            // 
            // rbtnAllowReports
            // 
            this.rbtnAllowReports.AutoSize = true;
            this.rbtnAllowReports.Location = new System.Drawing.Point(6, 19);
            this.rbtnAllowReports.Name = "rbtnAllowReports";
            this.rbtnAllowReports.Size = new System.Drawing.Size(50, 17);
            this.rbtnAllowReports.TabIndex = 0;
            this.rbtnAllowReports.Text = "Allow";
            this.rbtnAllowReports.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(12, 68);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // FrmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 102);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.grbFileClaims);
            this.Controls.Add(this.grbReports);
            this.Controls.Add(this.grbCreateConsumers);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSettings";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.FrmSettings_Load);
            this.grbCreateConsumers.ResumeLayout(false);
            this.grbCreateConsumers.PerformLayout();
            this.grbFileClaims.ResumeLayout(false);
            this.grbFileClaims.PerformLayout();
            this.grbReports.ResumeLayout(false);
            this.grbReports.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbCreateConsumers;
        private System.Windows.Forms.RadioButton rbtnPreventCreateConsumers;
        private System.Windows.Forms.RadioButton rbtnAllowCreateConsumers;
        private System.Windows.Forms.GroupBox grbFileClaims;
        private System.Windows.Forms.RadioButton rbtnPreventFileClaims;
        private System.Windows.Forms.RadioButton rbtnAllowFileClaims;
        private System.Windows.Forms.GroupBox grbReports;
        private System.Windows.Forms.RadioButton rbtnPreventReports;
        private System.Windows.Forms.RadioButton rbtnAllowReports;
        private System.Windows.Forms.Button btnSave;
    }
}