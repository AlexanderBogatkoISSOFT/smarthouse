﻿using System.Windows.Forms;
using WexHealth.Core.Presenters.Common.Interfaces;
using WexHealth.Core.Views.Common.Interfaces;

namespace TPASetupTool
{
    public partial class FrmSettings : Form, ISettingsView //ViewBase<ISettingsPresenter>
    {
        private ISettingsPresenter _presenter;

        public FrmSettings(ISettingsPresenter presenter)
        {
            InitializeComponent();

            _presenter = presenter;
        }

        private void FrmSettings_Load(object sender, System.EventArgs e)
        {
#warning IsPostBack in WinForms? (x5)
            _presenter?.InitView(false);
        }
    }
}
