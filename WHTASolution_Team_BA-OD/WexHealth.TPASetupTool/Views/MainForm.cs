﻿using System;
using System.Windows.Forms;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Views.Tools.TPASetupTool.Interfaces;

namespace TPASetupTool.Views
{
    public partial class MainForm : Form, IMainFormView //ViewBase<IMainFormPresenter>
    {
        private IMainFormPresenter _presenter;

#warning Injection MainForm
        public MainForm(IMainFormPresenter presenter = null)
        {
            _presenter = presenter;
            InitializeComponent();
        }

        private void tsmChangeAdministrator_Click(object sender, EventArgs e)
        {
            new FrmAdminSelection(null).ShowDialog();

        }

        private void tsmExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tsmGeneral_Click(object sender, EventArgs e)
        {
            new FrmEditAdministratorData(null).ShowDialog();
        }

        private void tsmSettings_Click(object sender, EventArgs e)
        {
            new FrmSettings(null).ShowDialog(null);
        }

        private void tsmManageUsers_Click(object sender, EventArgs e)
        {
            new FrmManageUsers(null).ShowDialog(null);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
#warning IsPostBack in WinForms? (x2)
            _presenter?.InitView(false);
        }
    }
}
