﻿using System.Windows.Forms;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Views.Tools.TPASetupTool.Interfaces;

namespace TPASetupTool.Views
{
    public partial class FrmManageUsers : Form, IManageUsersView
    {
        private IManageUsersPresenter _presenter;

        public FrmManageUsers(IManageUsersPresenter presenter)
        {
            InitializeComponent();

            _presenter = presenter;
        }

        private void FrmManageUsers_Load(object sender, System.EventArgs e)
        {
#warning IsPostBack in WinForms? (x4)
            _presenter?.InitView(false);
        }
    }
}
