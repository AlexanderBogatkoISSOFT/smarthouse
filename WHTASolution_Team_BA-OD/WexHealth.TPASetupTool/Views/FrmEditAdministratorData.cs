﻿using System.Windows.Forms;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Views.Tools.TPASetupTool.Interfaces;

namespace TPASetupTool.Views
{
    public partial class FrmEditAdministratorData : Form, IEditAdministratorDataView
    {
        private IEditAdministratorDataPresenter _presenter;

#warning Injection FrmEditAdministratorData
        public FrmEditAdministratorData(IEditAdministratorDataPresenter presenter)
        {
            InitializeComponent();

            _presenter = presenter;
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void FrmEditAdministratorData_Load(object sender, System.EventArgs e)
        {
#warning IsPostBack in WinForms? (x3)
            _presenter?.InitView(false);

        }
    }
}
