﻿using WexHealth.Core.Views.Tools.TPASetupTool.Interfaces;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Presenters.Common;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Presenters.Admin
{
    public class AdminSelectionPresenter : ViewPresenterBase<IAdminSelectionView>, IAdminSelectionPresenter
    {
        public IAdminSelectionView AdminSelectionView { get; set; }

        public AdminSelectionPresenter()
        {
        }

        protected override void InternalInitView(bool isPostBack)
        {
        }

        public IAdministrator GetSelectedAdministrator()
        {
            throw new System.NotImplementedException();
        }
    }
}
