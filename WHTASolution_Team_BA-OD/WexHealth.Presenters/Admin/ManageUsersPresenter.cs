﻿using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Views.Tools.TPASetupTool.Interfaces;
using WexHealth.Presenters.Common;

namespace WexHealth.Presenters.Admin
{
    public class ManageUsersPresenter : ViewPresenterBase<IManageUsersView>, IManageUsersPresenter
    {
        public IManageUsersView AdminSelectionView { get; set; }

        public ManageUsersPresenter()
        {
        }

        protected override void InternalInitView(bool isPostBack)
        {
        }
    }
}
