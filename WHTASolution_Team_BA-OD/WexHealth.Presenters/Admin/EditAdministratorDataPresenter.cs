﻿using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Views.Tools.TPASetupTool.Interfaces;
using WexHealth.Presenters.Common;

namespace WexHealth.Presenters.Admin
{
    public class EditAdministratorDataPresenter : ViewPresenterBase<IEditAdministratorDataView>, IEditAdministratorDataPresenter
    {
        public IEditAdministratorDataView AdminSelectionView { get; set; }

        public EditAdministratorDataPresenter()
        {
        }

        protected override void InternalInitView(bool isPostBack)
        {
        }
    }
}
