﻿using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Views.Tools.TPASetupTool.Interfaces;
using WexHealth.Presenters.Common;

namespace WexHealth.Presenters.Admin
{
    public class MainFormPresenter : ViewPresenterBase<IMainFormView>, IMainFormPresenter
    {
        public IMainFormView MainFormView { get; set; }

        public MainFormPresenter()
        {
        }

        protected override void InternalInitView(bool isPostBack)
        {
        }
    }
}
