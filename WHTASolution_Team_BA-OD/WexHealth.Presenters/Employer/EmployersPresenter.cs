﻿using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Services.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;
using WexHealth.Presenters.Common;

namespace WexHealth.Presenters.Employer
{
    /// <summary>
    /// Represents functionality for interraction with model
    /// </summary>
    public class EmployersPresenter : ViewPresenterBase<IEmployersView>, IEmployersPresenter
    {
        private readonly IEmployerService _employerService;
        public IEmployersView EmployersView { get; set; }
        /// <summary>
        /// Creates instance for <seealso cref="IEmployerView"/> view.
        /// </summary>
        /// <param name="employerService">Employer service</param>
        public EmployersPresenter(IEmployerService employerService)
        {
            _employerService = employerService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            if (isPostBack)
            {
                var employers = _employerService.Find(EmployersView.EmployerName, EmployersView.EmployerCode);
                EmployersView.Employers = employers;
            }
            else
            {
                var employers = _employerService.GetAll();
                EmployersView.Employers = employers;
            }
        }
    }
}