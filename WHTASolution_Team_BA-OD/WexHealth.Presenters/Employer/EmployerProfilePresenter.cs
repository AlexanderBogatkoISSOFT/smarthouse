﻿using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Services.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;
using WexHealth.Presenters.Common;

namespace WexHealth.Presenters.Employer
{
    public class EmployerProfilePresenter : ViewPresenterBase<IEmployerProfileView>, IEmployerProfilePresenter
    {
        private readonly IEmployerService _employerService;
        public IEmployerProfileView EmployerProfileView { get; set; }

        public EmployerProfilePresenter(IEmployerService employerService)
        {
            _employerService = employerService;
        }

        protected override void InternalInitView(bool isPostBack)
        {

        }

        public void LoadEmployerInfo(int employerId)
        {
            var employer = _employerService.GetEmployerDetailById(employerId);
            EmployerProfileView.Name = employer.EmployerName;
            EmployerProfileView.Code = employer.EmployerCode;
            EmployerProfileView.City = employer.City;
            EmployerProfileView.Phone = employer.Phone;
            EmployerProfileView.Street = employer.Street;
            EmployerProfileView.ZipCode = employer.ZipCode;
        }
    }
}