﻿using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;
using WexHealth.Presenters.Common;

namespace WexHealth.Presenters.Employer
{
    public class EmployerPresenter : ViewPresenterBase<IEmployerView>, IEmployerPresenter
    {
        public IEmployerView EmployerView { get; set; }

        protected override void InternalInitView(bool isPostBack)
        {
        }
    }
}