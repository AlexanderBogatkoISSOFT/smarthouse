﻿using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;
using WexHealth.Presenters.Common;

namespace WexHealth.Presenters.Home
{
    public class HomePresenter : ViewPresenterBase<IHomeView>
    {
        protected override void InternalInitView(bool isPostBack)
        {
            //View.Text = "Hello WexHealth!";
        }
    }
}