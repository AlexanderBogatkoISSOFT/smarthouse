﻿using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;
using WexHealth.Presenters.Common;

namespace WexHealth.Presenters
{
    public class SignInPresenter : ViewPresenterBase<ISignInView>, ISignInPresenter
    {
        public ISignInView SignInView { get; set; }

        public SignInPresenter()
        {
            
        }

        protected override void InternalInitView(bool isPostBack)
        {
        }
    }
}