﻿using System;
using System.Linq;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Services.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;
using WexHealth.Presenters.Common;
using WexHealth.ViewModels;

namespace WexHealth.Presenters.Consumer
{
    public class ConsumerUpdatePresenter : ViewPresenterBase<IConsumerUpdateView>, IConsumerUpdatePresenter
    {
        private readonly IConsumerService _consumerService;
        public IConsumerUpdateView ConsumerUpdateView { get; set; }

        public ConsumerUpdatePresenter(IConsumerService consumerService)
        {
            _consumerService = consumerService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            try
            {
                var cunsomerDetailedInfo = _consumerService.GetConsumerById(ConsumerUpdateView.ConsumerId);
                var cunsomerInfo = _consumerService.GetConsumerShortInfoById(ConsumerUpdateView.ConsumerId);

                ConsumerUpdateView.ConsumerInfo = $"{cunsomerInfo.FirstName} {cunsomerInfo.LastName} ({cunsomerInfo.SSN})";
                ConsumerUpdateView.EmployerInfo = $"Employer: {cunsomerInfo.EmployerName} ({cunsomerInfo.EmployerCode})";

                ConsumerUpdateView.FirstName = cunsomerDetailedInfo.FirstName;
                ConsumerUpdateView.LastName = cunsomerDetailedInfo.LastName;
                ConsumerUpdateView.SSN = cunsomerDetailedInfo.SSN;
                ConsumerUpdateView.Phone = cunsomerDetailedInfo.Phone;
                ConsumerUpdateView.Street = cunsomerDetailedInfo.Street;
                ConsumerUpdateView.City = cunsomerDetailedInfo.City;
                ConsumerUpdateView.ZipCode = cunsomerDetailedInfo.ZipCode;
                ConsumerUpdateView.UserName = cunsomerDetailedInfo.UserName;
                ConsumerUpdateView.Password = cunsomerDetailedInfo.Password;
                ConsumerUpdateView.SelectedState = cunsomerDetailedInfo.StateId;

                ConsumerUpdateView.States = _consumerService.GetAllStates().ToDictionary(k => k.StateId, v => v.Name);
            }
            catch (Exception e)
            {
                var str = e.Message + Environment.NewLine + e.InnerException;
            }
        }

        public void SaveChanges()
        {
            var consumer = new ConsumerInfoDetail()
            {
                ConsumerId = ConsumerUpdateView.ConsumerId,
                StateId = (byte)ConsumerUpdateView.SelectedState,
                FirstName = ConsumerUpdateView.FirstName,
                LastName = ConsumerUpdateView.LastName,
                SSN = ConsumerUpdateView.SSN,
                Phone = ConsumerUpdateView.Phone,
                Street = ConsumerUpdateView.Street,
                City = ConsumerUpdateView.City,
                ZipCode = ConsumerUpdateView.ZipCode,
                UserName = ConsumerUpdateView.UserName,
                Password = ConsumerUpdateView.Password
            };

#warning Release saving
            //_consumerService.SaveConsumerInfoDetail(consumer);
        }
    }
}
