﻿using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Services.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;
using WexHealth.Presenters.Common;

namespace WexHealth.Presenters.Consumer
{
    public class ConsumerPresenter : ViewPresenterBase<IConsumerView>, IConsumerPresenter
    {
        private readonly IConsumerService _consumerService;
        public IConsumerView ConsumerView { get; set; }

        public ConsumerPresenter(IConsumerService consumerService)
        {
            _consumerService = consumerService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            var cunsomerInfo = _consumerService.GetConsumerShortInfoById(ConsumerView.ConsumerId);
            
            ConsumerView.ConsumerInfo = $"{cunsomerInfo.FirstName} {cunsomerInfo.LastName} ({cunsomerInfo.SSN})";
            ConsumerView.EmployerInfo = $"Employer: {cunsomerInfo.EmployerName} ({cunsomerInfo.EmployerCode})";
        }
    }
}