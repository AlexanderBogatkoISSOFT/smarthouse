﻿using System.Collections.Generic;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Services.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;
using WexHealth.Presenters.Common;

namespace WexHealth.Presenters.Consumer
{
    public class ConsumersPresenter : ViewPresenterBase<IConsumersView>, IConsumersPresenter
    {
        private readonly IConsumerService _consumerService;
        public IConsumersView ConsumersView { get; set; }

        public ConsumersPresenter(IConsumerService consumerService)
        {
            _consumerService = consumerService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            var employers = _consumerService.GetEmployers();
            var formattedEmployers = new Dictionary<int, string>
            {
                [-1] = "All"
            };
            foreach (var item in employers)
            {
                formattedEmployers.Add(item.EmployerId, $"{item.Name} ({item.Code})");
            }

            ConsumersView.EmployersDictionary = formattedEmployers;

            if (isPostBack)
            {
                var consumers = _consumerService.Find(ConsumersView.FirstName, ConsumersView.LastName, ConsumersView.SelectedEmployerId == -1 ? null : (int?)ConsumersView.SelectedEmployerId);
                ConsumersView.Consumers = consumers;
            }
            else
            {
                var consumers = _consumerService.GetAll();
                ConsumersView.Consumers = consumers;
            }
        }
    }
}