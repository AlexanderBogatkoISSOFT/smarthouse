﻿using System;
using System.Security.Principal;
using WexHealth.Core.Exceptions;
using WexHealth.Core.Presenters.Interfaces;

namespace WexHealth.Presenters.Common
{
    public abstract class ViewPresenterBase<TView> : IViewPresenterBase
        where TView : class
    {
        private bool _initialized;

        public IPrincipal Principal { get; set; }

        protected readonly bool _readOnlyAccess;

        public event EventHandler UnauthorizedAccess;

        public event EventHandler PreAuthorizationFailed;

        ///// <summary>
        ///// Creates a new presenter that presents data to a view object.
        ///// </summary>
        ///// <exception cref="ArgumentNullException">Throws when <paramref name="view"/> is null.</exception>
        ///// <param name="view">The view to present.</param>
        ///// <param name="readOnlyAccess">Whether the user has read-only access to the view.</param>
        //protected ViewPresenterBase(TView view, bool readOnlyAccess)
        //{
        //    View = view ?? throw new ArgumentNullException("view", "View cannot be null.");
        //    _readOnlyAccess = readOnlyAccess;
        //}

        //protected ViewPresenterBase(TView view, IPrincipal principal)
        //    : this(view, false)
        //{
        //    Principal = principal;
        //}

        /// <summary>
        /// Initializes the view and ensures that the view is only ever initialized once.
        /// </summary>
        /// <exception cref="ViewAlreadyInitializedException">Throws when an attempt is made to initialize the view more than once.</exception>
        /// <param name="isPostBack">False if first load, true if subsequent load.</param>
        public virtual void InitView(bool isPostBack)
        {
            CheckInitialized();
            
            if (PreAuthorization(isPostBack)) // check any pre-authorization conditions
            {
                if (IsViewingAuthorized(isPostBack)) // check and authorization conditions
                {
                    InternalInitView(isPostBack);
                }
                else
                {
                    OnUnauthorizedAccess(EventArgs.Empty);
                }
            }
            else
            {
                OnPreAuthorizationFailed(EventArgs.Empty);
            }

            CompleteInitialize();
        }

        /// <summary>
        /// Called before <see cref="IsViewingAuthorized"/> to determine whether to continue the view initialization process.
        /// </summary>
        /// <param name="isPostBack">False if first load, true if subsequent load.</param>
        /// <returns>True if the initialize process should continue, false otherwise.</returns>
        protected virtual bool PreAuthorization(bool isPostBack)
        {
            // NOTE: no base implementation
            return true;
        }

        /// <summary>
        /// Checks whether a user is able to see the view for the current presenter.
        /// </summary>
        /// <returns>True if access is granted, false if access is not granted.</returns>
        protected virtual bool IsViewingAuthorized(bool isPostBack)
        {
            return true;
        }

        /// <summary>
        /// Raises the <see cref="UnauthorizedAccess"/> event when it has been determined that the user is not authorized to access the current <see cref="IValidatingView"/>.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnUnauthorizedAccess(EventArgs e)
        {
            UnauthorizedAccess?.Invoke(this, e);
        }

        /// <summary>
        /// Called after it is verified that the view has not already been initialized.
        /// </summary>
        /// <param name="isPostBack">
        /// Whether or not this is the first time loading the view. 
        /// False means this is the first time, true means this is a subsequent load.
        /// </param>
        protected abstract void InternalInitView(bool isPostBack);

        protected void CheckInitialized()
        {
            if (_initialized)
            {
#warning CheckInitialized Method turned off
                //    throw new ViewAlreadyInitializedException("An attempt was made to initialize the presenter multiple times.");
            }
        }

        protected void CompleteInitialize()
        {
            _initialized = true;
        }

        public bool IsInRole(string permissionKey)
        {
            return null != Principal && Principal.IsInRole(permissionKey);
        }

        /// <summary>
        /// Raises <see cref="PreAuthorizationFailed"/> event when it has been determined that pre-authorization step failed
        /// </summary>
        /// <param name="eventArgs"></param>
        protected virtual void OnPreAuthorizationFailed(EventArgs eventArgs)
        {
            PreAuthorizationFailed?.Invoke(this, eventArgs);
        }
    }
}