﻿using System;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Services.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;
using WexHealth.Presenters.Common;

namespace WexHealth.Presenters.Claims
{
    public class ClaimUpdatePresenter : ViewPresenterBase<IClaimView>, IClaimUpdatePresenter
    {
        public IClaimView ClaimUpdateView { get; set; }
        private readonly IClaimService _claimService;

        public ClaimUpdatePresenter(IClaimService claimService)
        {
            _claimService = claimService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            var claimInfo = _claimService.GetClaimInfoById(ClaimUpdateView.ClaimId);
            ClaimUpdateView.ConsumerId = claimInfo.ConsumerId;
            ClaimUpdateView.ConsumerName = claimInfo.ConsumerName;
            ClaimUpdateView.DateOfService = claimInfo.DateOfService;
            ClaimUpdateView.PhoneNumber = claimInfo.ConsumerPhoneNumber;
            ClaimUpdateView.ConsumerId = claimInfo.ConsumerId;
            ClaimUpdateView.Plan = claimInfo.Plan.ToString();
            ClaimUpdateView.Amount = claimInfo.Amount.ToString();
        }


        public void SaveChanges()
        {
            _claimService.UpdateClaim(ClaimUpdateView.ClaimId, DateTime.Now, 500, 2);
        }
    }
}