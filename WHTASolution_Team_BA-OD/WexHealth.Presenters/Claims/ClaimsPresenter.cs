﻿using System.Collections.Generic;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Services.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;
using WexHealth.Presenters.Common;

namespace WexHealth.Presenters.Claims
{
    public class ClaimsPresenter : ViewPresenterBase<IClaimsView>, IClaimsPresenter
    {
        private readonly IClaimService _claimService;
        public IClaimsView ClaimsView { get; set; }

        public ClaimsPresenter(IClaimService claimService)
        {
            _claimService = claimService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            var employers = _claimService.GetEmployers();
            var formattedEmployers = new Dictionary<int, string>
            {
                [-1] = "All"
            };
            foreach (var item in employers)
            {
                formattedEmployers.Add(item.EmployerId, $"{item.Name} ({item.Code})");
            }

            ClaimsView.EmployersDictionary = formattedEmployers;
            ClaimsView.Claims = _claimService.GetAll();

            if (isPostBack)
            {
                var claims = _claimService.Find(ClaimsView.ClaimNumber, null);
                ClaimsView.Claims = claims;
            }
            else
            {
                var claims = _claimService.GetAll();
                ClaimsView.Claims = claims;
            }
        }
    }
}