﻿using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Services.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;
using WexHealth.Presenters.Common;

namespace WexHealth.Presenters.Claims
{
    public class ClaimPresenter : ViewPresenterBase<IClaimView>, IClaimPresenter
    {
        public IClaimView ClaimView { get; set; }
        private readonly IClaimService _claimService;

        public ClaimPresenter(IClaimService claimService)
        {
            _claimService = claimService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            var claimInfo = _claimService.GetClaimInfoById(ClaimView.ClaimId);
            ClaimView.ConsumerId = claimInfo.ConsumerId;
            ClaimView.ConsumerName = claimInfo.ConsumerName;
            ClaimView.DateOfService = claimInfo.DateOfService;
            ClaimView.PhoneNumber = claimInfo.ConsumerPhoneNumber;
            ClaimView.ConsumerId = claimInfo.ConsumerId;
            ClaimView.Plan = claimInfo.Plan.ToString();
            ClaimView.Amount = claimInfo.Amount.ToString();
        }
        
    }
}