using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;

namespace WexHealth.AdminPortal
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var settings = new FriendlyUrlSettings
            {
                AutoRedirectMode = RedirectMode.Permanent
            };
            routes.EnableFriendlyUrls(settings);
            //Main
            routes.MapPageRoute("login", "Login", "~/Views/SingInView.aspx");
            //routes.MapPageRoute("logout", "Logout", "");
            routes.MapPageRoute("home", "", "~/Views/Home/HomeView.aspx");
            //Consumers
            routes.MapPageRoute("consumers", "Consumers", "~/Views/Consumer/ConsumersView.aspx");
            routes.MapPageRoute("consumer", "Consumers/Consumer/{ConsumerId}", "~/Views/Consumer/ConsumerView.aspx");
            routes.MapPageRoute("consumer-update", "Consumers/Consumer/UpdateProfile/{ConsumerId}", "~/Views/Consumer/ConsumerUpdateView.aspx");
            routes.MapPageRoute("consumer-enrollments", "Consumers/Consumer/Enrollments/{ConsumerId}", "~/Views/Consumer/ConsumerEnrollView.aspx");
            //Employers
            routes.MapPageRoute("employers", "Employers", "~/Views/Employer/EmployersView.aspx");
            routes.MapPageRoute("employers-add-new", "Employers/AddNewEmployer", "~/Views/Employer/EmployerProfileView.aspx");
            routes.MapPageRoute("employer", "Employers/Employer/{EmployerId}", "~/Views/Employer/EmployerView.aspx");
            routes.MapPageRoute("employer-manage-profile", "Employers/Employer/ManageProfile/{EmployerId}", "~/Views/Employer/EmployerManageProfileView.aspx");
            routes.MapPageRoute("employer-manage-plans", "Employers/Employer/ManagePlans/{EmployerId}", "~/Views/Employer/EmployerUpdatePlansView.aspx");
            routes.MapPageRoute("plan-update", "Employers/Employer/ManagePlans/Update/{PlanId}", "~/Views/Employer/EmployerUpdatePlansView.aspx");
            routes.MapPageRoute("employer-manage-rules", "Employers/Employer/ManageConsumerPortalRules/{EmployerId}", "~/Views/Employer/EmployerManageRulesView.aspx");
            routes.MapPageRoute("employer-add-new-consumer", "Employers/Employer/AddNewConsumer/{EmployerId}", "~/Views/Employer/EmployerView.aspx");
            routes.MapPageRoute("employer-manage-users", "Employers/Employer/ManageUsers/{EmployerId}", "~/Views/Employer/EmployerManageUsersView.aspx");
            //Claims
            routes.MapPageRoute("claims", "Claims", "~/Views/Claims/ClaimsView.aspx");
            routes.MapPageRoute("claim", "Claims/Claim/{ClaimId}", "~/Views/Claims/ClaimView.aspx");
            routes.MapPageRoute("claim-edit", "Claims/Claim/Edit/{ClaimId}", "~/Views/Claims/ClaimUpdateView.aspx");
            //Reports
            routes.MapPageRoute("reports", "Reports", "~/Views/Reports/ReportsView.aspx");
            //routes.MapPageRoute("reports-consumer-list", "Reports/ConsumerList/{ReportId}", "");
            //routes.MapPageRoute("reports-employer-list", "Reports/EmployerList/{ReportId}", "");
            //Setup
            routes.MapPageRoute("setup", "Setup", "~/Views/Setup/SetupView.aspx");
        }
    }
}