﻿using Autofac;
using System.Configuration;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Repositories.Interfaces;
using WexHealth.Core.Services.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;
using WexHealth.Presenters;
using WexHealth.Presenters.Claims;
using WexHealth.Presenters.Consumer;
using WexHealth.Presenters.Employer;
using WexHealth.Repositories;
using WexHealth.Services;
using WexHealth.ViewModels;

namespace WexHealth.AdminPortal
{
    public class AutofacConfig
    {
        private static IContainer _container;
        private static ContainerBuilder _builder;
        public static AutofacConfig Instance { get; }
        private static string _connectionString;

        public IEmployersPresenter EmployersPresenter { get; private set; }
        public IEmployerProfilePresenter EmployerProfilePresenter { get; private set; }
        public IEmployerPresenter EmployerPresenter { get; private set; }
        public IConsumerUpdatePresenter ConsumerUpdatePresenter { get; private set; }
        public IConsumersPresenter ConsumersPresenter { get; private set; }
        public IConsumerPresenter ConsumerPresenter { get; private set; }
        public IClaimsPresenter ClaimsPresenter { get; private set; }
        public IClaimPresenter ClaimPresenter { get; private set; }
        public IClaimUpdatePresenter ClaimUpdatePresenter { get; private set; }
        public ISignInPresenter SignInPresenter { get; private set; }


        static AutofacConfig()
        {
            Instance = new AutofacConfig();
            _builder = new ContainerBuilder();
        }

        /// <summary>
        /// Method, that register all dependencies
        /// </summary>
        /// <param name="connectionName"></param>
        public void RegisterDependencies(string connectionName)
        {
            _connectionString = ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
            RegisterRepositories();
            RegisterServices();
            RegisterViewModels();
            RegisterPresenters();

            _container = _builder.Build();

            using (var scope = _container.BeginLifetimeScope())
            {
                ResolvePresenters(scope);
            }
        }

        /// <summary>
        /// Method, where u can register Services
        /// </summary>
        private static void RegisterServices()
        {
            _builder.RegisterType<ConsumerService>().As<IConsumerService>();
            _builder.RegisterType<EmployerService>().As<IEmployerService>();
            _builder.RegisterType<ClaimService>().As<IClaimService>();
        }

        /// <summary>
        /// Method, where u can register ViewModels
        /// </summary>
        private static void RegisterViewModels()
        {
            _builder.RegisterType<ClaimInfo>().As<IClaimInfo>();
            _builder.RegisterType<ConsumerInfo>().As<IConsumerInfo>();
            _builder.RegisterType<ConsumerInfoDetail>().As<IConsumerInfoDetail>();
            _builder.RegisterType<DataReport>().As<IDataReport>();
            _builder.RegisterType<EmployerInfo>().As<IEmployerInfo>();
            _builder.RegisterType<EmployerInfoDetail>().As<IEmployerInfoDetail>();
            _builder.RegisterType<EnrollmentInfo>().As<IEnrollmentInfo>();
        }

        /// <summary>
        /// Method, where u can register Repositories
        /// </summary>
        private static void RegisterRepositories()
        {
            _builder.RegisterType<EmployerRepository>().As<IEmployerRepository>().WithParameter("connectionString", _connectionString);
            _builder.RegisterType<ClaimInfoRepository>().As<IClaimInfoRepository>().WithParameter("connectionString", _connectionString);
            _builder.RegisterInstance(new ConsumerInfoRepository(_connectionString)).As<IConsumerInfoRepository>();
            //_builder.RegisterType<ConsumerInfoRepository>().As<IConsumerInfoRepository>().WithParameter("connectionString", _connectionString);
            _builder.RegisterType<AddressRepository>().As<IAddressRepository>().WithParameter("connectionString", _connectionString);
        }

        private static void RegisterPresenters()
        {
            _builder.RegisterType<EmployersPresenter>().As<IEmployersPresenter>();
            _builder.RegisterType<EmployerPresenter>().As<IEmployerPresenter>();
            _builder.RegisterType<EmployerProfilePresenter>().As<IEmployerProfilePresenter>();
            _builder.RegisterType<ConsumerUpdatePresenter>().As<IConsumerUpdatePresenter>();
            _builder.RegisterType<ConsumerPresenter>().As<IConsumerPresenter>();
            _builder.RegisterType<ConsumersPresenter>().As<IConsumersPresenter>();
            _builder.RegisterType<ClaimsPresenter>().As<IClaimsPresenter>();
            _builder.RegisterType<ClaimPresenter>().As<IClaimPresenter>();
            _builder.RegisterType<ClaimUpdatePresenter>().As<IClaimUpdatePresenter>();
            _builder.RegisterType<SignInPresenter>().As<ISignInPresenter>();
        }

        private void ResolvePresenters(IComponentContext scope)
        {
            EmployersPresenter = scope.Resolve<IEmployersPresenter>();
            EmployerProfilePresenter = scope.Resolve<IEmployerProfilePresenter>();
            EmployerPresenter = scope.Resolve<IEmployerPresenter>();
            ConsumersPresenter = scope.Resolve<IConsumersPresenter>();
            ConsumerUpdatePresenter = scope.Resolve<IConsumerUpdatePresenter>();
            ConsumerPresenter = scope.Resolve<IConsumerPresenter>();
            ClaimsPresenter = scope.Resolve<IClaimsPresenter>();
            ClaimPresenter = scope.Resolve<IClaimPresenter>();
            SignInPresenter = scope.Resolve<ISignInPresenter>();
            ClaimUpdatePresenter = scope.Resolve<IClaimUpdatePresenter>();
        }
    }
}