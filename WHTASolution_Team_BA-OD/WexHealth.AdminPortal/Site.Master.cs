﻿using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WexHealth.AdminPortal
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void SignOut(object sender, CommandEventArgs e)
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }
    }
}