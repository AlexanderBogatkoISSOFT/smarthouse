﻿using System;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;

namespace WexHealth.AdminPortal
{
    public class Global : HttpApplication
    {

        public void Application_Start(object sender, EventArgs e)
        {
           // ContainerManager.Instance.RegisterDependencies("DefaultConnection");
            AutofacConfig.Instance.RegisterDependencies("DefaultConnection");
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}