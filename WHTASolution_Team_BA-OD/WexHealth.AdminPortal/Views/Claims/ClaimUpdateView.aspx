﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ClaimUpdateView.aspx.cs" Inherits="WexHealth.AdminPortal.Views.Claims.ClaimUpdateView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Label runat="server" ID="lbl_ClaimNumber"/>
    <label>Detail</label>
    <div class="container-vertical">
        <asp:HyperLink runat="server" ID="hl_ConsumerName" />
        <asp:Label CssClass="label" runat="server" ID="lb_Phone"/>
        <asp:TextBox CssClass="text-input" runat="server" ID="tbxDate"/>
        <asp:DropDownList CssClass="dropdown" runat="server" ID="ddPlan"/>
        <asp:TextBox CssClass="text-input" runat="server" ID="tbxAmount"/>
        
        <div class="container-horizontal">
            <asp:Button runat="server" OnClick="OnSubmitButtonClick" Text="Submit"/>
            <asp:Button runat="server" OnClick="OnCancelButtonClick" Text="Cancel"/>
        </div>
    </div>
</asp:Content>