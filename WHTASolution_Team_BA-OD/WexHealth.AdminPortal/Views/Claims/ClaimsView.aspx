﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ClaimsView.aspx.cs" Inherits="WexHealth.AdminPortal.Views.Claims.ClaimsView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>Search Criteria</p>
    <div class="container-horizontal">
        <div class="container-vertical">
            <label>ClaimNumber</label>
            <asp:TextBox runat="server" CssClass="text-input" ID="tbx_ClaimNumber"/>
        </div>
        <div class="container-vertical">
            <label>Employer</label>
            <asp:DropDownList runat="server" CssClass="dropdown" ID="ddEmployers"/>
        </div>
    </div>
    <asp:ListView runat="server"></asp:ListView>
    <asp:ListView runat="server" ID="lv_Claims">
        <ItemTemplate>
            <div class="container-horizontal">
                <asp:HyperLink runat="server" Text=<%#Eval("ClaimNumber")%> NavigateUrl=<%#Page.GetRouteUrl("claim", new { ClaimId = Eval("ClaimId") })%>/>
                <asp:HyperLink runat="server" Text=<%#Eval("ConsumerName")%> NavigateUrl=<%#Page.GetRouteUrl("consumer", new { ConsumerId = Eval("ConsumerId") })%>/>
                <asp:HyperLink runat="server" Text=<%#Eval("EmployerName")%> NavigateUrl=<%#Page.GetRouteUrl("employer", new { EmployerId = Eval("EmployerId") })%>/>
                <asp:Label runat="server" Text=<%# Eval("DateOfService") %>/>
                <asp:Label runat="server" Text=<%#Eval("Plan") %>/>
                <asp:Label runat="server" Text=<%#Eval("Amount") %>/>
            </div>
        </ItemTemplate>
    </asp:ListView>
</asp:Content>