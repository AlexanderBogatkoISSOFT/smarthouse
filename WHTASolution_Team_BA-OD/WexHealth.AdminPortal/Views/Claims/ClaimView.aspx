﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ClaimView.aspx.cs" Inherits="WexHealth.AdminPortal.Views.Claims.ClaimView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Label runat="server" ID="lbl_ClaimNumber"/>
    <label>Detail</label>
    <div class="container-vertical">
        <asp:HyperLink runat="server" ID="hl_EditClaim" Text="Edit"/>
        <asp:HyperLink runat="server" ID="hl_ConsumerName" />
        <asp:Label runat="server" ID="lb_Phone"/>
        <asp:Label runat="server" ID="lb_Date"/>
        <asp:Label runat="server" ID="lb_Plan"/>
        <asp:Label runat="server" ID="lb_Amount"/>
    </div>
</asp:Content>