﻿using System;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.AdminPortal.Views.Claims
{
    public partial class ClaimView : ViewBase<IClaimPresenter>, IClaimView
    {
        public string ConsumerName
        {
            get => hl_ConsumerName.Text;
            set => hl_ConsumerName.Text = value;
        }

        public int ConsumerId
        {
            get => ConsumerId;
            set => hl_ConsumerName.NavigateUrl=  Page.GetRouteUrl("consumer", new {ConsumerId = value.ToString()});
        }

        public int ClaimId => int.Parse(Page.RouteData.Values["ClaimId"].ToString());

        public string PhoneNumber
        {
            get => lb_Phone.Text;
            set => lb_Phone.Text = value;
        }

        public DateTime DateOfService
        {
            get => DateTime.Parse(lb_Date.Text);
            set => lb_Date.Text = value.ToString();
        }
        public string Plan
        {
            get => lb_Plan.Text;
            set => lb_Plan.Text = value;
        }

        public string Amount
        {
            get => lb_Amount.Text;
            set => lb_Amount.Text = value;
        }

        public ClaimView()
        {
            _presenter = AutofacConfig.Instance.ClaimPresenter;
            _presenter.ClaimView = this;
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            hl_EditClaim.NavigateUrl = GetRouteUrl("claim-edit", new { ClaimId = ClaimId.ToString() });
            _presenter.InitView(IsPostBack);
        }   
    }
}