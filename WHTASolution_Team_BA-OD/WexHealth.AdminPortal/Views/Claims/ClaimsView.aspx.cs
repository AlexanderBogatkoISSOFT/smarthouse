﻿using System;
using System.Collections.Generic;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.AdminPortal.Views.Claims
{
    public partial class ClaimsView : ViewBase<IClaimsPresenter>, IClaimsView
    {
        public string ClaimNumber
        {
            get => tbx_ClaimNumber.Text;
            set => tbx_ClaimNumber.Text = value;
        }

        public string SelectedEmployer { get; set; }

        public IDictionary<int, string> EmployersDictionary
        {
            get => ddEmployers.DataSource as IDictionary<int, string>;
            set
            {
                ddEmployers.DataSource = value;
                ddEmployers.DataValueField = "Key";
                ddEmployers.DataTextField = "Value";
                ddEmployers.DataBind();
            }
        }

        public int SelectedEmployerId
        {
            get => int.TryParse(ViewState["SelectedEmployerId"]?.ToString(), out var id) ? id : -1;
            set => ViewState["SelectedEmployerId"] = value;
        }

        public IEnumerable<IClaimInfo> Claims
        {
            get => lv_Claims.DataSource as IEnumerable<IClaimInfo>;
            set
            {
                lv_Claims.DataSource = value;
                lv_Claims.DataBind();
            }
        }

        public ClaimsView()
        {
            _presenter = AutofacConfig.Instance.ClaimsPresenter;
            _presenter.ClaimsView = this;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                SelectedEmployerId = int.Parse(ddEmployers.SelectedValue);
            }
            _presenter.InitView(IsPostBack);
        }
    }
}