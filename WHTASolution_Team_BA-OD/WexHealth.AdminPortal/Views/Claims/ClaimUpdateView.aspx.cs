﻿using System;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.AdminPortal.Views.Claims
{
    public partial class ClaimUpdateView : ViewBase<IClaimUpdatePresenter>, IClaimView
    {
        public string ConsumerName
        {
            get => hl_ConsumerName.Text;
            set => hl_ConsumerName.Text = value;
        }

        public int ConsumerId
        {
            get => ConsumerId;
            set => hl_ConsumerName.NavigateUrl = Page.GetRouteUrl("consumer", new { ConsumerId = value.ToString() });
        }

        public int ClaimId => int.Parse(Page.RouteData.Values["ClaimId"].ToString());

        public string PhoneNumber
        {
            get => lb_Phone.Text;
            set => lb_Phone.Text = value;
        }

        public DateTime DateOfService
        {
            get => DateTime.Parse(tbxDate.Text);
            set => tbxDate.Text = value.ToString();
        }
        public string Plan
        {
            get => ddPlan.Text;
            set => ddPlan.Text = value;
        }

        public string Amount
        {
            get => tbxAmount.Text;
            set => tbxAmount.Text = value;
        }

        public ClaimUpdateView()
        {
            _presenter = AutofacConfig.Instance.ClaimUpdatePresenter;
            _presenter.ClaimUpdateView = this;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(IsPostBack);
        }

        protected void OnCancelButtonClick(object sender, EventArgs e)
        {

        }

        protected void OnSubmitButtonClick(object sender, EventArgs e)
        {
            _presenter.SaveChanges();
            Response.Redirect(Page.GetRouteUrl("claim", new { ClaimId = ClaimId.ToString() }));
        }
    }
}