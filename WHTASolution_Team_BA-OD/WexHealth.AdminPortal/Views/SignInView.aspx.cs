﻿using System;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;
using static System.Web.Security.FormsAuthentication;

namespace WexHealth.AdminPortal.Views
{
    public partial class SignInView : ViewBase<ISignInPresenter>, ISignInView
    {
        public string Login
        {
            get => tbx_Login.Text;
            set => tbx_Login.Text = value;
        }

        public string Password
        {
            get => tbx_Password.Text;
            set => tbx_Password.Text = value;
        }

        public SignInView()
        {
            _presenter = AutofacConfig.Instance.SignInPresenter;
            _presenter.SignInView = this;

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(IsPostBack);
        }

        protected void SignInClick(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            #pragma warning disable 618
            if (Authenticate(Login, Password))
            #pragma warning restore 618
            {
                RedirectFromLoginPage(tbx_Login.Text, false);
            }
        }
    }
}