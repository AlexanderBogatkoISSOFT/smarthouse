﻿using System.Security.Principal;
using System.Web;
using System.Web.UI;
using WexHealth.Core.Views;

namespace WexHealth.AdminPortal.Views
{
    public abstract class ViewBase<TPresenter> : Page, IView
    {
        protected TPresenter _presenter;

        protected ViewBase()
        {
            Principal = HttpContext.Current.User;
        }

        public IPrincipal Principal { get; }
    }
}