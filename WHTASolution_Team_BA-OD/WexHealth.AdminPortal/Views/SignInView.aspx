﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SignInView.aspx.cs" Inherits="WexHealth.AdminPortal.Views.SignInView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-vertical">
        <label class="label">Email:</label>
        <asp:TextBox runat="server" CssClass="text-input" ID="tbx_Login"/>
        <label class="label">Password:</label>
        <asp:TextBox runat="server" CssClass="text-input" ID="tbx_Password" TextMode="Password"/>
        <asp:Button runat="server" ValidationGroup="SignIn-VG" CssClass="btn" OnClick="SignInClick" Text="SignIn" />
        <asp:Label CssClass="label" runat="server" ID="lbl_ErrorMessage" ForeColor="Red" />
            <asp:RegularExpressionValidator 
                ID="REV_Validator"
                runat="server" 
                ControlToValidate="tbx_Login"
                ErrorMessage="Not a valid login" 
                SetFocusOnError="True" 
                ValidationExpression="\w+([-+.']\w+)*"
                ValidationGroup="SignIn-VG"
                ForeColor="Red">
            </asp:RegularExpressionValidator>
    </div>
</asp:Content>
