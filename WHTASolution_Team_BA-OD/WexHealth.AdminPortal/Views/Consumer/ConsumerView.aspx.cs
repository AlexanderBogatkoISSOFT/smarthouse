﻿using System;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.AdminPortal.Views.Consumer
{
    public partial class ConsumerView : ViewBase<IConsumerPresenter>, IConsumerView
    {
        public string ConsumerInfo
        {
            get => lblConsumerInfo.Text;
            set => lblConsumerInfo.Text = value;
        }

        public string EmployerInfo
        {
            get => lblEmployerInfo.Text;
            set => lblEmployerInfo.Text = value;
        }

        public int ConsumerId
        {
            get
            {
                if (!int.TryParse(Page.RouteData.Values["ConsumerId"]?.ToString(), out var id))
                {
                    #warning ShowErrorPage(404);
                    return -1;
                }

                return id;
            }
        }

        public ConsumerView()
        {
            _presenter = AutofacConfig.Instance.ConsumerPresenter;
            _presenter.ConsumerView = this;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(IsPostBack);
        }
    }
}