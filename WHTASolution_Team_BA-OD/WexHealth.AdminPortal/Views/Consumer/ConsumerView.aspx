﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConsumerView.aspx.cs" Inherits="WexHealth.AdminPortal.Views.Consumer.ConsumerView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-vertical">
        <asp:Label runat="server" ID="lblConsumerInfo" />
        <asp:Label runat="server" ID="lblEmployerInfo" />
        <a href="<%= ResolveUrl(GetRouteUrl("consumer-update", new { ConsumerId })) %>">Update Profile</a>
        <a href="<%= ResolveUrl(GetRouteUrl("consumer-enrollments", new { ConsumerId })) %>">Enroll Or Update Enrollment</a>
        <a href="<%= ResolveUrl(GetRouteUrl("claims", new { ConsumerId })) %>">Manage Claims</a>
    </div>
</asp:Content>
