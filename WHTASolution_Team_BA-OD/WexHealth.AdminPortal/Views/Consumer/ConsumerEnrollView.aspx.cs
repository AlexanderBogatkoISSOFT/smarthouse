﻿using System;
using System.Web.UI;

namespace WexHealth.AdminPortal.Views.Consumer
{
    public partial class ConsumerEnrollView : Page
    {
        public string ConsumerInfo
        {
            get => lblConsumerInfo.Text;
            set => lblConsumerInfo.Text = value;
        }

        public string EmployerInfo
        {
            get => lblEmployerInfo.Text;
            set => lblEmployerInfo.Text = value;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}