﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConsumersView.aspx.cs" Inherits="WexHealth.AdminPortal.Views.Consumer.ConsumersView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>SearchCriteria</p>
    <div class="container-horizontal">
        <div class="container-vertical">
            <p class="label">LastName</p>
            <asp:TextBox runat="server" CssClass="text-input" ID="txtLastName"></asp:TextBox>
        </div>
        <div class="container-vertical">
            <p class="label">FirstName</p>
            <asp:TextBox runat="server" CssClass="text-input" ID="txtFirstName"></asp:TextBox>
        </div>
        <div class="container-vertical">
            <p class="label">Employer</p>
            <asp:DropDownList runat="server" CssClass="dropdown" ID="ddEmployers" />
        </div>

        <div class="container-vertical">
            <div style="height: 4.1rem; width: 2rem;"></div>
            <asp:Button runat="server" CssClass="btn" Text="Search" OnClick="OnClick"/>
        </div>

    </div>
    <asp:ListView runat="server" ID="lvConsumers">
        <ItemTemplate>
            <div class="container-horizontal container-large border card">
                <asp:HyperLink CssClass="container-small" runat="server" NavigateUrl='<%# GetRouteUrl("consumer", new { ConsumerId = Eval("ConsumerId") }) %>' Text='<%#Eval("FirstName") %>' />
                <asp:Label runat="server" CssClass="label container-small" Text='<%# Eval("LastName") %>' />
                <asp:Label runat="server" CssClass="label container-small" Text='<%#Eval("SSN") %>' />
            </div>
        </ItemTemplate>
    </asp:ListView>
</asp:Content>
