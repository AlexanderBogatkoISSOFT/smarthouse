﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConsumerUpdateView.aspx.cs" Inherits="WexHealth.AdminPortal.Views.Consumer.ConsumerUpdateView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:Label runat="server" ID="lblConsumerInfo" />
    <asp:Label runat="server" ID="lblEmployerInfo" />

    <a href="<%= ResolveUrl(GetRouteUrl("consumers", new { })) %>">Select Different Consumer</a>
    <div class="container-horizontal">
        <div class="container-vertical">
            <asp:Label runat="server" Text="FirstName" />
            <asp:TextBox runat="server" CssClass="text-input" ID="txtFirstName" />

            <asp:Label runat="server" Text="LastName" />
            <asp:TextBox runat="server" CssClass="text-input" ID="txtLastName" />

            <asp:Label runat="server" Text="SSN" />
            <asp:TextBox runat="server" CssClass="text-input" ID="txtSSN" />

            <asp:Label runat="server" Text="Phone" />
            <asp:TextBox runat="server" CssClass="text-input" ID="txtPhone" />

            <asp:Label runat="server" Text="Street" />
            <asp:TextBox runat="server" CssClass="text-input" ID="txtStreet" />
        </div>
        <div class="container-vertical">
            <asp:Label runat="server" Text="City" />
            <asp:TextBox runat="server" CssClass="text-input" ID="txtCity" />

            <asp:Label runat="server" Text="State" />
            <asp:DropDownList runat="server" ID="ddStates" CssClass="dropdown" />

            <asp:Label runat="server" Text="Zip Code" />
            <asp:TextBox runat="server" CssClass="text-input" ID="txtZipCode" />

            <asp:Label runat="server" Text="Username" />
            <asp:TextBox runat="server" CssClass="text-input" ID="txtUsername" />

            <asp:Label runat="server" Text="Password" />
            <asp:TextBox runat="server" CssClass="text-input" ID="txtPassword"  /><%--TextMode="Password"--%>
        </div>
    </div>

    <asp:Button CssClass="btn" runat="server" Text="Submit" />
    <a href="<%= ResolveUrl(GetRouteUrl("consumer", new { ConsumerId })) %>" class="btn">Cancel</a>
</asp:Content>
