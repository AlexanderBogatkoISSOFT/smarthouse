﻿using System;
using System.Collections.Generic;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;
using WexHealth.Core.Presenters.Interfaces;

namespace WexHealth.AdminPortal.Views.Consumer
{
    public partial class ConsumerUpdateView : ViewBase<IConsumerUpdatePresenter>, IConsumerUpdateView
    {
        public string ConsumerInfo
        {
            get => lblConsumerInfo.Text;
            set => lblConsumerInfo.Text = value;
        }

        public string EmployerInfo
        {
            get => lblEmployerInfo.Text;
            set => lblEmployerInfo.Text = value;
        }

        public int ConsumerId
        {
            get
            {
                if (!int.TryParse(Page.RouteData.Values["ConsumerId"].ToString(), out var id))
                {
                    return -1;
                }

                return id;
            }
        }

        #region IConsumerUpdateView

        public string FirstName
        {
            get => txtFirstName.Text;
            set => txtFirstName.Text = value;
        }

        public string LastName
        {
            get => txtLastName.Text;
            set => txtLastName.Text = value;
        }

        public string SSN
        {
            get => txtSSN.Text;
            set => txtSSN.Text = value;
        }

        public string Phone
        {
            get => txtPhone.Text;
            set => txtPhone.Text = value;
        }

        public string Street
        {
            get => txtStreet.Text;
            set => txtStreet.Text = value;
        }

        public string City
        {
            get => txtCity.Text;
            set => txtCity.Text = value;
        }

        public IDictionary<byte, string> States
        {
            get => ddStates.DataSource as IDictionary<byte, string>;
            set
            {
                ddStates.DataSource = value;
                ddStates.DataValueField = "Key";
                ddStates.DataTextField = "Value";
                ddStates.DataBind();
            }
        }

        public byte? SelectedState
        {
            get => byte.TryParse(ddStates.SelectedValue, out var id) ? (byte?)id : null;
            set => ddStates.SelectedValue = value.ToString();
        }

        public string ZipCode
        {
            get => txtZipCode.Text;
            set => txtZipCode.Text = value;
        }

        public string UserName
        {
            get => txtUsername.Text;
            set => txtUsername.Text = value;
        }

        public string Password
        {
            get => txtPassword.Text;
            set => txtPassword.Text = value;
        }
        #endregion

        public ConsumerUpdateView()
        {
            _presenter = AutofacConfig.Instance.ConsumerUpdatePresenter;
            _presenter.ConsumerUpdateView = this;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
#warning Redirect to all consumers or filtered consumer?
            //var id = int.Parse((string)Page.RouteData.Values["ConsumerId"]);
            //hlToConsumers.NavigateUrl = GetRouteUrl("consumers", new { });

            _presenter.InitView(IsPostBack);
        }
    }
}