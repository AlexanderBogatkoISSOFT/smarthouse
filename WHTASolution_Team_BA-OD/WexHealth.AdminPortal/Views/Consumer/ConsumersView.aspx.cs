﻿using System;
using System.Collections.Generic;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.AdminPortal.Views.Consumer
{
    public partial class ConsumersView : ViewBase<IConsumersPresenter>, IConsumersView
    {
        public string FirstName
        {
            get => txtFirstName.Text;
            set => txtFirstName.Text = value;
        }

        public string LastName
        {
            get => txtLastName.Text;
            set => txtLastName.Text = value;
        }
        
        public IEnumerable<IConsumerInfo> Consumers
        {
            get => lvConsumers.DataSource as ICollection<IConsumerInfo>;
            set
            {
                lvConsumers.DataSource = value;
                lvConsumers.DataBind();
            }
        }

        public IDictionary<int, string> EmployersDictionary
        {
            get => ddEmployers.DataSource as IDictionary<int, string>;
            set
            {
                ddEmployers.DataSource = value;
                ddEmployers.DataValueField = "Key";
                ddEmployers.DataTextField = "Value";
                ddEmployers.DataBind();
            }
        }

        public int SelectedEmployerId
        {
            get => int.TryParse(ViewState["btnSearch"]?.ToString(), out var id) ? id : -1;
            set => ViewState["btnSearch"] = value;
        }

        public ConsumersView()
        {
            _presenter = AutofacConfig.Instance.ConsumersPresenter;
            _presenter.ConsumersView = this;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                SelectedEmployerId = int.Parse(ddEmployers.SelectedValue);
            }
            _presenter.InitView(IsPostBack);
        }

        protected void OnClick(object sender, EventArgs e)
        {
            ddEmployers.SelectedIndex = SelectedEmployerId;
        }
    }
}