﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployersView.aspx.cs" Inherits="WexHealth.AdminPortal.Views.Employer.EmployersView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <p>SearchCriteria</p>
    <div class="container-horizontal">
        <div class="container-vertical">
            <p class="label">Employer Name</p>
            <asp:TextBox runat="server" ID="txtEmployerName" CssClass="text-input"></asp:TextBox>
        </div>
        <div class="container-vertical">
            <p class="label">Employer Code</p>
            <asp:TextBox runat="server" ID="txtEmployerCode" CssClass="text-input"></asp:TextBox>
        </div>

        <div class="container-vertical">
            <div style="height: 4.1rem; width: 2rem;" ></div> 
            <asp:Button runat="server" CssClass="btn" Text="Search" />
        </div>
    </div>
    <asp:ListView runat="server" ID="lv_Employers">
        <ItemTemplate>
            <div class="container-horizontal container-large border card">
                <asp:HyperLink runat="server" Text='<%#Eval("Name")%>' NavigateUrl='<%#Page.GetRouteUrl("employer", new { EmployerId = Eval("EmployerId") })%>' />
                <asp:Label CssClass="label" runat="server" Text='<%# Eval("Code") %>' />
            </div>
        </ItemTemplate>
    </asp:ListView>
</asp:Content>