﻿using System;
using System.Collections.Generic;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.AdminPortal.Views.Employer
{
    public partial class EmployerManageProfileView : ViewBase<IEmployerProfilePresenter>, IEmployerProfileView
    {
        public string Name
        {
            get => tbx_Name.Text;
            set => tbx_Name.Text = value;
        }

        public string Code
        {
            get => tbx_Code.Text;
            set => tbx_Code.Text = value;
        }

        public string Street
        {
            get => tbx_Street.Text;
            set => tbx_Street.Text = value;
        }

        public string City
        {
            get => tbx_City.Text;
            set => tbx_City.Text = value;
        }

        public IEnumerable<string> States
        {
            get => ddl_States.DataSource as List<string>;
            set
            {
                ddl_States.DataSource = value;
                ddl_States.DataBind();
            }
        }

        public string SelectedState { get; set; }

        public string ZipCode
        {
            get => tbx_ZipCode.Text;
            set => tbx_ZipCode.Text = value;
        }

        public string Phone
        {
            get => tbx_Phone.Text;
            set => tbx_Phone.Text = value;
        }

        public EmployerManageProfileView()
        {
            _presenter = AutofacConfig.Instance.EmployerProfilePresenter;
            _presenter.EmployerProfileView = this;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(IsPostBack);

            var id = int.Parse((string)Page.RouteData.Values["EmployerId"]);
            _presenter.LoadEmployerInfo(id);
        }

        public string AdminInfo { get; set; }
    }
}