﻿using System;
using System.Collections.Generic;
using WexHealth.Core.Models.Interfaces;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.AdminPortal.Views.Employer
{
    public partial class EmployersView : ViewBase<IEmployersPresenter>, IEmployersView
    {
        public string EmployerName
        {
            get => txtEmployerName.Text;
            set => txtEmployerName.Text = value;
        }

        public string EmployerCode
        {
            get => txtEmployerCode.Text;
            set => txtEmployerCode.Text = value;
        }

        public IEnumerable<IEmployer> Employers
        {
            get => lv_Employers.DataSource as IEnumerable<IEmployer>;
            set
            {
                lv_Employers.DataSource = value;
                lv_Employers.DataBind();
            } 
        }

        public EmployersView()
        {
            _presenter = AutofacConfig.Instance.EmployersPresenter;
            _presenter.EmployersView = this;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(IsPostBack);
        }
    }
}