﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployerView.aspx.cs" Inherits="WexHealth.AdminPortal.Views.Employer.EmployerView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-vertical">
         <asp:Label runat="server" ID="lbl_AdminInfo"/>
         <asp:HyperLink runat="server" ID="hl_ManageProfile" Text="Manage Profile"/>
         <asp:HyperLink runat="server" ID="hl_ManagePlans" Text="Manage Plans"/>
         <asp:HyperLink runat="server" ID="hl_ManageConsumerPortalRules" Text="Manage Consumer Portal Rules"/>
         <asp:HyperLink runat="server" ID="hl_ManageConsumers" Text="Manage Consumers"/>
         <asp:HyperLink runat="server" ID="hl_AddNewConsumer" Text="Add New Consumer"/>
         <asp:HyperLink runat="server" ID="hl_ManageUsers" Text="Manage Users"/>
    </div>
</asp:Content>