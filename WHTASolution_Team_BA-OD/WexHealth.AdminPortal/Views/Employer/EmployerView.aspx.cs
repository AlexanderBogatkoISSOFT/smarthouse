﻿using System;
using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.AdminPortal.Views.Employer
{
    public partial class EmployerView : ViewBase<IEmployerPresenter>, IEmployerView
    {
        public string AdminInfo
        {
            get => lbl_AdminInfo.Text;
            set => lbl_AdminInfo.Text = value;
        }

        public EmployerView()
        {
            _presenter = AutofacConfig.Instance.EmployerPresenter;
            _presenter.EmployerView = this;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var id = int.Parse((string)Page.RouteData.Values["EmployerID"]);
            hl_ManageProfile.NavigateUrl = GetRouteUrl("employer-manage-profile", new { EmployerId = id });
            hl_ManagePlans.NavigateUrl = GetRouteUrl("employer-manage-plans", new { EmployerId = id });
            hl_ManageConsumerPortalRules.NavigateUrl = GetRouteUrl("employer-manage-rules", new { EmployerId = id });
            hl_ManageConsumers.NavigateUrl = GetRouteUrl("consumers", new { EmployerId = id });
            hl_AddNewConsumer.NavigateUrl = GetRouteUrl("employer-add-new-consumer", new { EmployerId = id });
            hl_ManageUsers.NavigateUrl = GetRouteUrl("employer-manage-users", new { EmployerId = id });
        }
    }
}