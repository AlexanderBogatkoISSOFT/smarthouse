﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployerManageProfileView.aspx.cs" Inherits="WexHealth.AdminPortal.Views.Employer.EmployerManageProfileView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-horizontal">
        <div class="container-vertical">
            <asp:Label runat="server" Text="Name"/>
            <asp:TextBox runat="server" ID="tbx_Name" CssClass="text-input"/>

            <asp:Label runat="server" Text="Code"/>
            <asp:TextBox runat="server" ID="tbx_Code" CssClass="text-input"/>

            <asp:Label runat="server" Text="Phone"/>
            <asp:TextBox runat="server" ID="tbx_Phone" CssClass="text-input"/>
        </div>
        <div class="container-vertical">
            <asp:Label runat="server" Text="Street"/>
            <asp:TextBox runat="server" ID="tbx_Street" CssClass="text-input"/>

            <asp:Label runat="server" Text="City"/>
            <asp:TextBox runat="server" ID="tbx_City" CssClass="text-input"/>

            <asp:Label runat="server" Text="State"/>
            <asp:DropDownList runat="server" ID="ddl_States" CssClass="dropdown"/>

            <asp:Label runat="server" Text="Zip Code"/>
            <asp:TextBox runat="server" ID="tbx_ZipCode" CssClass="text-input"/>
        </div>
    </div>
    <asp:Button class="btn" runat="server" Text="Submit"/>
    <asp:Button class="btn" runat="server" Text="Cancel"/>
</asp:Content>
