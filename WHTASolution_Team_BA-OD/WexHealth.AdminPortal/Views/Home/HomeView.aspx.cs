﻿using System;
using WexHealth.Presenters.Home;
using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.AdminPortal.Views.Home
{
    public partial class HomeView : ViewBase<object>, IHomeView
    {
        private readonly HomePresenter _homePresenter;

        public string Text
        {
            get => lb_Text.Text;
            set => lb_Text.Text = value;
        }

        public HomeView()
        {
            _homePresenter = new HomePresenter();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _homePresenter.InitView(IsPostBack);
        }
    }
}