﻿using System;

namespace WexHealth.Core.Models.Interfaces
{
    public interface IClaimStatus
    {
        long ClaimStatusId { get; set; }
        byte Status { get; set; }
        string Notes { get; set; }
        DateTime Date { get; set; }
    }
}