﻿namespace WexHealth.Core.Models.Interfaces
{
    public interface ISettingConnection
    {
        int SettingConnectionId { get; set; }
        string Value { get; set; }
        int SettingId { get; set; }
    }
}