﻿using System;

namespace WexHealth.Core.Models.Interfaces
{
    public interface IBenefitsPackage
    {
        int BenefitsPackageId { get; set; }
        string Name { get; set; }
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
        byte PayrollFrequency { get; set; }
        int PackageStatusId { get; set; }
        int EmployerId { get; set; }
    }
}