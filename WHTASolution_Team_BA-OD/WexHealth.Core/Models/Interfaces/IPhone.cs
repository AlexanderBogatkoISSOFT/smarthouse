﻿using System;

namespace WexHealth.Core.Models.Interfaces
{
    public interface IPhone
    {
        long PhoneId { get; set; }
        string Number { get; set; }
        DateTime ChangeDate { get; set; }
    }
}