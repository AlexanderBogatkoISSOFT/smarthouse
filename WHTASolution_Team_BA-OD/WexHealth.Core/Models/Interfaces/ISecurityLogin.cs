﻿using System;

namespace WexHealth.Core.Models.Interfaces
{
    public interface ISecurityLogin
    {
        int LoginId { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        int IndividualId { get; set; }
        byte LoginType { get; set; }
        int EmployerId { get; set; }
        short DomainId { get; set; }
        DateTime ChangeDate { get; set; }
    }
}