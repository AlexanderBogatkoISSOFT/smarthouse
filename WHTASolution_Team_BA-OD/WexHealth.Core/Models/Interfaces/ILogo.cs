﻿using System;

namespace WexHealth.Core.Models.Interfaces
{
    public interface ILogo
    {
        int LogoId { get; set; }
        byte[] Data  { get; set; }
        DateTime ChangeDate { get; set; }
    }
}