﻿using System;

namespace WexHealth.Core.Models.Interfaces
{
    public interface IImportEnrollment
    {
        int ImportEnrollmentId { get; set; }
        int EmployerId { get; set; }
        int ImportEnrollmentStatusId { get; set; }
        int InitialFileName { get; set; }
        string Error { get; set; }
        DateTime ChangeDate { get; set; }
    }
}