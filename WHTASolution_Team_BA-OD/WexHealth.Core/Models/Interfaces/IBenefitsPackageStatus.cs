﻿using System;

namespace WexHealth.Core.Models.Interfaces
{
    public interface IBenefitsPackageStatus
    {
        int PackageStatusId { get; set; }
        DateTime Date { get; set; }
    }
}