﻿namespace WexHealth.Core.Models.Interfaces
{
    public interface IBenefitsContract
    {
        int EmployerId { get; set; }
        int AdministratorId { get; set; }
    }
}