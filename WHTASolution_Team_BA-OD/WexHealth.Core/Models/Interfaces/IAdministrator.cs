﻿using System;

namespace WexHealth.Core.Models.Interfaces
{
    public interface IAdministrator
    {
        int AdministratorId { get; set; }
        string Name { get; set; }
        short DomainId { get; set; }
        DateTime ChangeDate { get; set; }
    }
}