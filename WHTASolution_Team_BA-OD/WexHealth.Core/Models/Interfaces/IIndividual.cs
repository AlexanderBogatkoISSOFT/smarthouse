﻿using System;

namespace WexHealth.Core.Models.Interfaces
{
    public interface IIndividual
    {
        int IndividualId { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Email { get; set; }
        DateTime? Birthdate { get; set; }
        byte LoginType { get; set; }
        string Ssn { get; set; }
        DateTime ChangeDate { get; set; }
    }
}