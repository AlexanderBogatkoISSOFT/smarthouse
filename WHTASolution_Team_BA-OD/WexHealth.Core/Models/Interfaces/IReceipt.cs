﻿using System;

namespace WexHealth.Core.Models.Interfaces
{
    public interface IReceipt
    {
        long ReceiptId { get; set; }
        byte[] Data { get; set; }
        DateTime ChangeDate { get; set; }
    }
}