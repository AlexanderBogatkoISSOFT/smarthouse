﻿using System;

namespace WexHealth.Core.Models.Interfaces
{
    public interface IEnrollment
    {
        int EnrollmentId { get; set; }
        decimal ElectionAmount { get; set; }
        decimal ContributionAmount { get; set; }
        int ParticipantId { get; set; }
        int BenefitsOfferingId { get; set; }
        DateTime ChangeDate { get; set; }
    }
}