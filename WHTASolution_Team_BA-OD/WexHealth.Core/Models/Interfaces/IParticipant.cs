﻿namespace WexHealth.Core.Models.Interfaces
{
    public interface IParticipant
    {
        int ParticipantId { get; set; }
        int IndividualId { get; set; }
        int EmployerId { get; set; }
    }
}