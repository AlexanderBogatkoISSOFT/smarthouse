﻿using System;

namespace WexHealth.Core.Models.Interfaces
{
    public interface IEmployer
    {
        int EmployerId { get; set; }
        string Name { get; set; }
        string Code { get; set; }
        int LogoId { get; set; }
        DateTime ChangeDate { get; set; }
    }
}