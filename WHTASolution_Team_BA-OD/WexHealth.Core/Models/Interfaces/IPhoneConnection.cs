﻿namespace WexHealth.Core.Models.Interfaces
{
    public interface IPhoneConnection
    {
        long PhoneId { get; set; }
        byte PhoneType { get; set; }
        int Object { get; set; }
        byte ObjectType { get; set; }
    }
}