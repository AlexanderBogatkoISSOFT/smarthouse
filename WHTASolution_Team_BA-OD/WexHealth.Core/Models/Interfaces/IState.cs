﻿namespace WexHealth.Core.Models.Interfaces
{
    public interface IState
    {
        byte StateId { get; set; }
        string Name { get; set; }
        string ShortName { get; set; }
    }
}
