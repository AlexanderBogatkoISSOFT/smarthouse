﻿using System;

namespace WexHealth.Core.Models.Interfaces
{
    public interface ISetting
    {
        int SettingId { get; set; }
        string Name { get; set; }
        string DefaultValue { get; set; }
        string ValueType { get; set; }
        short SettingCategory { get; set; }
        DateTime ChangeDate { get; set; }
    }
}