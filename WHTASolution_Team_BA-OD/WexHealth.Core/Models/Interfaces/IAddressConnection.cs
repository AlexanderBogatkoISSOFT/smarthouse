﻿namespace WexHealth.Core.Models.Interfaces
{
    public interface IAddressConnection
    {
        long AddressId { get; set; }
        byte StateId { get; set; }
        int ObjectId { get; set; }
        byte ObjectType { get; set; }
    }
}