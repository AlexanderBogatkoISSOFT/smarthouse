﻿using System;

namespace WexHealth.Core.Models.Interfaces
{
    public interface IBenefitsOffering
    {
        int BenefitsOfferingId { get; set; }
        string Name { get; set; }
        byte Plan { get; set; }
        decimal ContributionAmount { get; set; }
        int BenefitsPackageId { get; set; }
        DateTime ChangeDate { get; set; }
    }
}