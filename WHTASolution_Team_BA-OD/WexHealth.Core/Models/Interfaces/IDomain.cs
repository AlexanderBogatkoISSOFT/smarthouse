﻿using System;

namespace WexHealth.Core.Models.Interfaces
{
    public interface IDomain
    {
        short DomainId { get; set; }
        string Name { get; set; }
        DateTime ChangeDate { get; set; }
    }
}