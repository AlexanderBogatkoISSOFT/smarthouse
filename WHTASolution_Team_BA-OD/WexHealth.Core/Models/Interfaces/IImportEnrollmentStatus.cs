﻿using System;

namespace WexHealth.Core.Models.Interfaces
{
    public interface IImportEnrollmentStatus
    {
        int ImportEnrollmentStatusId { get; set; }
        byte Status { get; set; }
        DateTime Date { get; set; }
    }
}