﻿using System;

namespace WexHealth.Core.Models.Interfaces
{
    public interface IAddress
    {
        long AddressId { get; set; }
        string City { get; set; }
        string Street { get; set; }
        string ZipCode { get; set; }
        byte StateId { get; set; }
        byte AddressType { get; set; }
        DateTime ChangeDate { get; set; }
    }
}