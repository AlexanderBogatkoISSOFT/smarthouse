﻿using System;

namespace WexHealth.Core.Models.Interfaces
{
    public interface IClaim
    {
        long ClaimId { get; set; }
        DateTime DateOfService { get; set; }
        string ClaimNumber { get; set; }
        decimal Amount { get; set; }
        int ParticipantId { get; set; }
        int BenefitsOfferingId { get; set; }
        long ClaimStatusId { get; set; }
        long ReceiptId { get; set; }
        DateTime ChangeDate { get; set; }
    }
}