﻿namespace WexHealth.Core.ViewModels.Interfaces
{
    public interface IConsumerInfoDetail : IConsumerInfo, IAddressInfo
    {
        string UserName { get; set; }
        string Password { get; set; }
        string EmployerInfo { get; set; }
    }
}