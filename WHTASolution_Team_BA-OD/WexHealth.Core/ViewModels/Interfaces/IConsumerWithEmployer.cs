﻿namespace WexHealth.Core.ViewModels.Interfaces
{
    public interface IConsumerWithEmployer : IConsumerInfo
    {
        int EmployerId { get; set; }
        int ParticipantId { get; set; }
        string EmployerName { get; set; }
        string EmployerCode { get; set; }
    }
}
