﻿namespace WexHealth.Core.ViewModels.Interfaces
{
    public interface IUserInfo
    {
        int LoginId { get; set; }
        int EmployerId { get; set; }
        string FirsName { get; set; }
        string LastName { get; set; }
        string Email { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
    }
}