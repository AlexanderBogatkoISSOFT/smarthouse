﻿using System;

namespace WexHealth.Core.ViewModels.Interfaces
{
    public interface IClaimInfo : IDomainModel
    {
        long ClaimId { get; set; }
        int ConsumerId { get; set; }
        int EmployerId { get; set; }
        string ClaimNumber { get; set; }
        string ConsumerName { get; set; }
        string EmployerName { get; set; }
        string ConsumerPhoneNumber { get; set; }
        DateTime DateOfService { get; set; }
        byte Plan { get; set; }
        decimal Amount { get; set; }
    }
}