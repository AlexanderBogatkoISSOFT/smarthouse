﻿namespace WexHealth.Core.ViewModels.Interfaces
{
    public interface IEmployerInfoDetail : IEmployerInfo, IAddressInfo
    {
        byte[] Logo { get; set; }
    }
}