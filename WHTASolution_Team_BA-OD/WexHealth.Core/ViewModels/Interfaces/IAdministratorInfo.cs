﻿namespace WexHealth.Core.ViewModels.Interfaces
{
    public interface IAdministratorInfo
    {
        int AdminId { get; set; }
        string Name { get; set; }
        string Alias { get; set; }
    }
}