﻿namespace WexHealth.Core.ViewModels.Interfaces
{
    public interface IAdministratorDetailInfo : IAdministratorInfo, IAddressInfo
    {
    }
}