﻿namespace WexHealth.Core.ViewModels.Interfaces
{
    public interface IAddressInfo : IDomainModel
    {
        string Street { get; set; }
        string City { get; set; }
        byte StateId { get; set; }
        string ZipCode { get; set; }
        string Phone { get; set; }
    }
}