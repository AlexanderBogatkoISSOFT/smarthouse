﻿namespace WexHealth.Core.ViewModels.Interfaces
{
    public interface IConsumerInfo : IDomainModel
    {
        int ConsumerId { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string SSN { get; set; }
    }
}