﻿using System;

namespace WexHealth.Core.ViewModels.Interfaces
{
    public interface IDataReport : IDomainModel
    {
        string RequestedStatus { get; set; }
        DateTime DateTime { get; set; }
        string RequestedBy { get; set; }
        string Format { get; set; }
    }
}