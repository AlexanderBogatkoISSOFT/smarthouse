﻿namespace WexHealth.Core.ViewModels.Interfaces
{
    public interface IEmployerInfo : IDomainModel
    {
        int EmployerId { get; set; }
        int AdminId { get; set; }
        string EmployerName { get; set; }
        string EmployerCode { get; set; }
    }
}