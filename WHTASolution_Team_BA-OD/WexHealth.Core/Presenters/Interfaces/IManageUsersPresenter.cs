﻿using WexHealth.Core.Views.Tools.TPASetupTool.Interfaces;

namespace WexHealth.Core.Presenters.Interfaces
{
    public interface IManageUsersPresenter : IViewPresenterBase
    {
        IManageUsersView AdminSelectionView { get; set; }
    }
}
