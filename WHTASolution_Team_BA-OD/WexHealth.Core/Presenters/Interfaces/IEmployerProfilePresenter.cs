﻿using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.Core.Presenters.Interfaces
{
    public interface IEmployerProfilePresenter : IViewPresenterBase
    {
        void LoadEmployerInfo(int employerId);
        IEmployerProfileView EmployerProfileView { get; set; }
    }
}
