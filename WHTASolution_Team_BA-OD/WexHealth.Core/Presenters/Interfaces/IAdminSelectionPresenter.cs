﻿using WexHealth.Core.Models.Interfaces;
using WexHealth.Core.Views.Tools.TPASetupTool.Interfaces;

namespace WexHealth.Core.Presenters.Interfaces
{
    public interface IAdminSelectionPresenter : IViewPresenterBase
    {
        IAdminSelectionView AdminSelectionView { get; set; }
        IAdministrator GetSelectedAdministrator();
    }
}
