﻿namespace WexHealth.Core.Presenters.Interfaces
{
    public interface IViewPresenterBase
    {
        void InitView(bool isPostBack);
    }
}