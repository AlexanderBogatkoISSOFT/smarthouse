﻿using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.Core.Presenters.Interfaces
{
    public interface IClaimsPresenter : IViewPresenterBase
    {
        IClaimsView ClaimsView { get; set; }
    }
}