﻿using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.Core.Presenters.Interfaces
{
    public interface IClaimUpdatePresenter : IViewPresenterBase
    {
        IClaimView ClaimUpdateView { get; set; }
        void SaveChanges();
    }
}