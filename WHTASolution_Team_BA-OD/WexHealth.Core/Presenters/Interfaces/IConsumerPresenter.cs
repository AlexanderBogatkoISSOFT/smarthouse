﻿using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.Core.Presenters.Interfaces
{
    public interface IConsumerPresenter : IViewPresenterBase
    {
        IConsumerView ConsumerView { get; set; }
    }
}