﻿using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.Core.Presenters.Interfaces
{
    public interface IClaimPresenter : IViewPresenterBase
    {
        IClaimView ClaimView { get; set; }
    }
}