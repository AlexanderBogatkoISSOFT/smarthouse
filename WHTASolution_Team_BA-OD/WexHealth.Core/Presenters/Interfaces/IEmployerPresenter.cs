﻿using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.Core.Presenters.Interfaces
{
    public interface IEmployerPresenter : IViewPresenterBase
    {
        IEmployerView EmployerView { get; set; }
    }
}
