﻿using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.Core.Presenters.Interfaces
{
    public interface ISignInPresenter : IViewPresenterBase
    {
        ISignInView SignInView { get; set; }
    }
}