﻿using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.Core.Presenters.Interfaces
{
    public interface IConsumerUpdatePresenter : IViewPresenterBase
    {
        IConsumerUpdateView ConsumerUpdateView { get; set; }
    }
}
