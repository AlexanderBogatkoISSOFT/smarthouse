﻿using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.Core.Presenters.Interfaces
{
    public interface IEmployersPresenter : IViewPresenterBase
    {
        IEmployersView EmployersView { get; set; }
    }
}