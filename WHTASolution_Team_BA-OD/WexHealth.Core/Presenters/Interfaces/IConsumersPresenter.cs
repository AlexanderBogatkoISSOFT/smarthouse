﻿using WexHealth.Core.Views.Portals.AdminPortal.Interfaces;

namespace WexHealth.Core.Presenters.Interfaces
{
    public interface IConsumersPresenter : IViewPresenterBase
    {
        IConsumersView ConsumersView { get; set; }
    }
}