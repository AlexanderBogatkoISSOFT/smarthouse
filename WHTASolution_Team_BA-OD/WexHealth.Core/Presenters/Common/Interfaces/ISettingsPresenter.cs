﻿using WexHealth.Core.Presenters.Interfaces;
using WexHealth.Core.Views.Common.Interfaces;

namespace WexHealth.Core.Presenters.Common.Interfaces
{
    public interface ISettingsPresenter : IViewPresenterBase
    {
        ISettingsView AdminSelectionView { get; set; }
    }
}
