﻿using System.Collections.Generic;
using WexHealth.Core.Models.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.Core.Services.Interfaces
{
    public interface IEmployerService
    {
        IEnumerable<IEmployer> GetAll();
        IEnumerable<IEmployer> Find(string employerName, string employerCode);
        IEmployerInfoDetail GetEmployerDetailById(int employerId);
        void AddNewEmployer(IEmployerInfoDetail employerInfoDetail);
        void UpdateEmployer(IEmployerInfoDetail employerInfoDetail);
        void SaveChanges(IEmployerInfoDetail employerInfoDetail);
    }
}