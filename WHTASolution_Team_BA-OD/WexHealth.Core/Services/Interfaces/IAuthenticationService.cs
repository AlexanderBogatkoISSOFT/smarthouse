﻿namespace WexHealth.Core.Services.Interfaces
{
    public interface IAuthenticationService
    {
        bool Authenticate(string login, string password);
    }
}
