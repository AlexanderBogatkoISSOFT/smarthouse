﻿using System.Collections.Generic;
using WexHealth.Core.Models.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.Core.Services.Interfaces
{
    public interface IAdminService
    {
        IAdministratorDetailInfo GetAdminInfoById(int adminId);
        void CreateNewAdmin(IAdministratorDetailInfo administratorDetailInfo);
        void UpdateAdmin(IAdministratorDetailInfo administratorDetailInfo);
        IEnumerable<IAdministratorInfo> GetAdmins();
        IEnumerable<IState> GetStates();
        void SaveChanges(IAdministratorDetailInfo administratorDetailInfo);
    }
}