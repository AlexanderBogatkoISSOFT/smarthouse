﻿using System.Collections.Generic;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.Core.Services.Interfaces
{
    public interface IUserService
    {
        IEnumerable<IUserInfo> GetUsers(int employerId);
        void AddNewUser(IUserInfo userInfo, int loginType);
        void UpdateUser(IUserInfo userInfo);
    }
}