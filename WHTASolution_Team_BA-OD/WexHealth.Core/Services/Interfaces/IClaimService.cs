﻿using System;
using System.Collections.Generic;
using WexHealth.Core.Models.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.Core.Services.Interfaces
{
    public interface IClaimService
    {
        IEnumerable<IClaimInfo> GetAll();
        IEnumerable<IClaimInfo> Find(string claimNumber, int? employerId);
        IEnumerable<IEmployer> GetEmployers();
        IClaimInfo GetClaimInfoById(int claimId);
        void UpdateClaim(int claimId, DateTime? dateOfService, decimal? amount, byte? plan);
    }
}