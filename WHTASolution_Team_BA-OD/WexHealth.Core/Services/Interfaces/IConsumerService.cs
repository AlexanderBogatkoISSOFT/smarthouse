﻿using System.Collections.Generic;
using WexHealth.Core.Models.Interfaces;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.Core.Services.Interfaces
{
    public interface IConsumerService
    {
        IEnumerable<IConsumerInfo> GetAll();
        IEnumerable<IConsumerInfo> Find(string firstName, string lastName, int? employerId);
        IEnumerable<IEmployer> GetEmployers();
        IConsumerInfoDetail GetConsumerById(int consumerId);
        IConsumerWithEmployer GetConsumerShortInfoById(int consumerId);
        IEnumerable<IState> GetAllStates();
    }
}