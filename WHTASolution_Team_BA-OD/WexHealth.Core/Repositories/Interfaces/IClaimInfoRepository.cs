﻿using System;
using System.Collections.Generic;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.Core.Repositories.Interfaces
{
    public interface IClaimInfoRepository
    {
        IEnumerable<IClaimInfo> GetAll();
        IEnumerable<IClaimInfo> Find(string claimNumber, int? employerId);
        IClaimInfo GetClaimInfoById(int claimId);
        void UpdateClaim(int claimId, DateTime? dateOfService, decimal? amount, byte? plan);
    }
}
