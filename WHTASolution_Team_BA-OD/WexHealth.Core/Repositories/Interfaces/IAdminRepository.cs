﻿using System.Collections.Generic;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.Core.Repositories.Interfaces
{
    public interface IAdminRepository
    {
        IAdministratorDetailInfo GetAdminInfoById(int adminId);
        void CreateNewAdmin(IAdministratorDetailInfo administratorDetailInfo);
        void UpdateAdmin(IAdministratorDetailInfo administratorDetailInfo);
        IEnumerable<IAdministratorInfo> GetAdmins();
        void SaveChanges(IAdministratorDetailInfo administratorDetailInfo);
    }
}