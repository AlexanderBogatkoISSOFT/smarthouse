﻿using System.Collections.Generic;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.Core.Repositories.Interfaces
{
    public interface IConsumerInfoRepository
    {
        IEnumerable<IConsumerInfo> GetAll();
        IEnumerable<IConsumerWithEmployer> Find(string firstName, string lastName, int? employerId);
        IConsumerInfoDetail GetConsumerById(int consumerId);
        IConsumerWithEmployer GetConsumerShortInfoById(int consumerId);
    }
}