﻿using System.Collections.Generic;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Core.Repositories.Interfaces
{
    public interface IAddressRepository
    {
        IEnumerable<IState> GetAllStates();
    }
}
