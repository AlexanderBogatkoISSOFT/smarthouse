﻿using System.Collections.Generic;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.Core.Repositories.Interfaces
{
    public interface IUserRepository
    {
        IEnumerable<IUserInfo> GetUsersByEmployerId(int employerId);
        IEnumerable<IUserInfo> GetUsersByAdminId(int adminId);
        void InsertUser(IUserInfo userInfo, int loginType);
        void UpdateUser(IUserInfo userInfo);
    }
}