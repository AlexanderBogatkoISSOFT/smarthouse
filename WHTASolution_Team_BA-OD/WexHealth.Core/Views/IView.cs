﻿using System.Security.Principal;

namespace WexHealth.Core.Views
{
    public interface IView
    {
        IPrincipal Principal { get; }
    }
}