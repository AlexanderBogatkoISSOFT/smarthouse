﻿namespace WexHealth.Core.Views.Portals.AdminPortal.Interfaces
{
    public interface IConsumerView : IView
    {
        string ConsumerInfo { get; set; }
        string EmployerInfo { get; set; }
        int ConsumerId { get; }
    }
}