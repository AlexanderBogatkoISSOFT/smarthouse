﻿using System;

namespace WexHealth.Core.Views.Portals.AdminPortal.Interfaces
{
    public interface IClaimView : IView
    {
        int ClaimId { get; }
        string ConsumerName { get; set; }
        int ConsumerId { get; set; }
        string PhoneNumber { get; set; }
        DateTime DateOfService { get; set; }
        string Plan { get; set; }
        string Amount { get; set; }
    }
}