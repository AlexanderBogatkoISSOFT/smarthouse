﻿namespace WexHealth.Core.Views.Portals.AdminPortal.Interfaces
{
    public interface ISignInView : IView
    {
        string Login { get; set; }
        string Password { get; set; }
    }
}