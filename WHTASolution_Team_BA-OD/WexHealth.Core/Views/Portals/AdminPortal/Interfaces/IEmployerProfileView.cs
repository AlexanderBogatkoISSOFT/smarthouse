﻿using System.Collections.Generic;

namespace WexHealth.Core.Views.Portals.AdminPortal.Interfaces
{
    public interface IEmployerProfileView : IEmployerView
    {
        string Name { get; set; }
        string Code { get; set; }
        string Street { get; set; }
        string City { get; set; }
        IEnumerable<string> States { get; set; }
        string SelectedState { get; set; }
        string ZipCode { get; set; }
        string Phone { get; set; }
    }
}