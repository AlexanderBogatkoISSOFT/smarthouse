﻿using System.Collections.Generic;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.Core.Views.Portals.AdminPortal.Interfaces
{
    public interface IClaimsView : IView
    {
        string ClaimNumber { get; set; }
        string SelectedEmployer { get; set; }
        IDictionary<int, string> EmployersDictionary { get; set; }
        IEnumerable<IClaimInfo> Claims { get; set; }
    }
}