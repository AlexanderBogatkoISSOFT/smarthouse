﻿using System.Collections.Generic;

namespace WexHealth.Core.Views.Portals.AdminPortal.Interfaces
{
    public interface IConsumerUpdateView : IConsumerView
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string SSN { get; set; }
        string Phone { get; set; }
        string Street { get; set; }
        string City { get; set; }
        IDictionary<byte, string> States { get; set; }
        byte? SelectedState { get; set; }
        string ZipCode { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
    }
}