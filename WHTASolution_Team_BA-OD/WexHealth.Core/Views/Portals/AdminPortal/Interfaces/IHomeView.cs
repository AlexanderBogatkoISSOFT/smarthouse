﻿namespace WexHealth.Core.Views.Portals.AdminPortal.Interfaces
{
    public interface IHomeView : IView
    {
        string Text { get; set; }
    }
}