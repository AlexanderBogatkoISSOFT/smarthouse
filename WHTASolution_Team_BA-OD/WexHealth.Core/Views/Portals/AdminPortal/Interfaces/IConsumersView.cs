﻿using System.Collections.Generic;
using WexHealth.Core.ViewModels.Interfaces;

namespace WexHealth.Core.Views.Portals.AdminPortal.Interfaces
{
    public interface IConsumersView : IView
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        int SelectedEmployerId { get; set; }
        IDictionary<int, string> EmployersDictionary { get; set; }
        IEnumerable<IConsumerInfo> Consumers { get; set; }
    }
}