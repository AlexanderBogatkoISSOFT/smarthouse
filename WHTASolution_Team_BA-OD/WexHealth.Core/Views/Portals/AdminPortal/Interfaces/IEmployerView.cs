﻿namespace WexHealth.Core.Views.Portals.AdminPortal.Interfaces
{
    public interface IEmployerView : IView
    {
        string AdminInfo { get; set; }
    }
}