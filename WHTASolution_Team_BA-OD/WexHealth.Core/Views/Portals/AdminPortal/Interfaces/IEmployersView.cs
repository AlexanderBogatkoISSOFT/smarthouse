﻿using System.Collections.Generic;
using WexHealth.Core.Models.Interfaces;

namespace WexHealth.Core.Views.Portals.AdminPortal.Interfaces
{
    public interface IEmployersView : IView
    {
        string EmployerName { get; set; }
        string EmployerCode { get; set; }
        IEnumerable<IEmployer> Employers { get; set; }
    }
}