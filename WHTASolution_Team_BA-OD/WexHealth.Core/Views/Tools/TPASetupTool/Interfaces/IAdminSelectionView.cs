﻿using System.Collections.Generic;

namespace WexHealth.Core.Views.Tools.TPASetupTool.Interfaces
{
    public interface IAdminSelectionView
    {
        IDictionary<int, string> Administrators { get; set; }
        int SelectedAdministrator { get; set; }
    }
}
