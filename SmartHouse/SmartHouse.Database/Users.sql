﻿CREATE TABLE [dbo].[Users]
(
	[Id] INT IDENTITY NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Email] NVARCHAR(50) NOT NULL,
    [PasswordHash] NVARCHAR(300) NOT NULL
)
