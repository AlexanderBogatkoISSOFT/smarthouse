﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

--INSERT INTO dbo.Users(Name, Email, PasswordHash)
--VALUES
--('FirstPerson',		'FirstPerson@gmail.com',	'11111'),
--('SecondPerson',	'SecondPerson@gmail.com',	'22222'),
--('ThirdPerson',		'ThirdPerson@gmail.com',	'33333')

--INSERT INTO dbo.Groups(Name, UserId)
--VALUES
--('Group-1', 1),
--('Group-2', 1),
--('Group-3', 1),
--('Group-1', 2),
--('Group-2', 2),
--('Group-3', 2),
--('Group-4', 2),
--('Group-1', 3),
--('Group-2', 3),
--('Group-3', 3),
--('Group-4', 3),
--('Group-5', 3)

--INSERT INTO dbo.TriggerDevices(Name, GroupId)
--VALUES
--('Td-1', 1),
--('Td-2', 2),
--('Td-3', 2),
--('Td-1', 3),
--('Td-2', 3),
--('Td-3', 3),
--('Td-2', 4),
--('Td-1', 5),
--('Td-2', 5),
--('Td-3', 5),
--('Td-1', 6),
--('Td-4', 6),
--('Td-1', 7),
--('Td-1', 8),
--('Td-4', 8),
--('Td-1', 9),
--('Td-4', 9),
--('Td-1', 10),
--('Td-2', 10),
--('Td-3', 10),
--('Td-2', 11),
--('Td-3', 11),
--('Td-2', 12),
--('Td-3', 12)

--INSERT INTO dbo.ActionDevices(Name, GroupId)
--VALUES
--('Ad-1', 1),
--('Ad-2', 1),
--('Ad-3', 1),
--('Ad-1', 2),
--('Ad-2', 2),
--('Ad-3', 2),
--('Ad-1', 3),
--('Ad-2', 3),
--('Ad-3', 3),
--('Ad-4', 3),
--('Ad-1', 4),
--('Ad-2', 4),
--('Ad-3', 4),
--('Ad-4', 4),
--('Ad-1', 5),
--('Ad-2', 5),
--('Ad-3', 5),
--('Ad-4', 5),
--('Ad-1', 6),
--('Ad-2', 6),
--('Ad-3', 6),
--('Ad-4', 6),
--('Ad-1', 7),
--('Ad-2', 7),
--('Ad-3', 7),
--('Ad-4', 7),
--('Ad-1', 8),
--('Ad-2', 8),
--('Ad-3', 8),
--('Ad-4', 8),
--('Ad-1', 9),
--('Ad-2', 9),
--('Ad-3', 9),
--('Ad-4', 9),
--('Ad-1', 10),
--('Ad-2', 10),
--('Ad-3', 10),
--('Ad-4', 10),
--('Ad-1', 11),
--('Ad-2', 11),
--('Ad-3', 11),
--('Ad-4', 11),
--('Ad-1', 12),
--('Ad-2', 12),
--('Ad-3', 12),
--('Ad-4', 12)