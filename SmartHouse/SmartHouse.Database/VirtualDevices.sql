﻿CREATE TABLE [dbo].[VirtualDevices]
(
	[Id] INT IDENTITY NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [GroupId] INT NULL
)
