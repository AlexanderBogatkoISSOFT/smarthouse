﻿namespace SmartHouse.Models
{
    public class Device : Model
    {
        public int? GroupId { get; set; }
        public Group Group { get; set; }
    }
}