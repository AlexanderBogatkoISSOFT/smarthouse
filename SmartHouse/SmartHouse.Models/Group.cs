﻿using System.Collections.Generic;

namespace SmartHouse.Models
{
    public class Group : Model
    {
        public int? UserId { get; set; }
        public User User { get; set; }
        public ICollection<TriggerDevice> TriggerDevices { get; set; }
        public ICollection<ActionDevice> ActionDevices { get; set; }
       // public ICollection<VirtualDevice> VirtualDevices { get; set; }
    }
}