﻿using System.Collections.Generic;

namespace SmartHouse.Models
{
    public class User : Model
    {
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public ICollection<Group> Groups { get; set; }
    }
}