﻿using System;
using System.Collections.Generic;
using Ninject;
using SmartHouse.BLL;
using SmartHouse.Models;

namespace SmartHouse.ConsoleApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            AddTestData();

            //var ninjectKernel = new StandardKernel(new SmartHouseConfig());
            //var SMCore = ninjectKernel.Get<SmartHouseCore>();

            //foreach (var user in SMCore.EfUnitOfWork.Users.GetAll())
            //{
            //    Console.WriteLine($"{user.Email} {user.Id}");
            //    foreach (var gr in SMCore.GetUserGroups(user.Id))
            //    {
            //        Console.WriteLine("\t" + gr.Name);
            //        Console.WriteLine("\t\tActionDevices:");
            //        foreach (var ad in SMCore.GetAllGroupActionDevices(gr.Id))
            //        {
            //            Console.WriteLine("\t\t\t" + ad.Name);
            //        }
            //        Console.WriteLine("\t\tTriggerDevices:");
            //        foreach (var tg in SMCore.GetAllGroupTriggerDevices(gr.Id))
            //        {
            //            Console.WriteLine("\t\t\t" + tg.Name);
            //        }
            //    }
            //}

            //Console.Read();
        }

        public static void AddTestData()
        {
            var ninjectKernel = new StandardKernel(new SmartHouseConfig());
            var smartHouseCore = ninjectKernel.Get<SmartHouseCore>();
            smartHouseCore.CreateNewUser("First", "First@gmail.com", "111");
            smartHouseCore.CreateNewUser("Second", "Second@gmail.com", "222");
            smartHouseCore.CreateNewUser("Third", "Third@gmail.com", "333");
            smartHouseCore.EfUnitOfWork.Save();
            var rand = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            foreach (var user in smartHouseCore.EfUnitOfWork.Users.GetAll())
            {
                var groups = new List<Group>();
                for (var i = 0; i < 5; i++)
                {
                    groups.Add(new Group(){Name = $"Group-{rand.Next(int.MaxValue)}"});
                }

                user.Groups = groups;
            }
            smartHouseCore.EfUnitOfWork.Save();
            foreach (var group in smartHouseCore.EfUnitOfWork.Groups.GetAll())
            {
                var ActionDevices = new List<ActionDevice>();
                for (var i = 0; i < 5; i++)
                {
                    ActionDevices.Add(new ActionDevice(){Name = $"ActionDevice-{rand.Next(int.MaxValue)}", GroupId = group.Id});
                }
                var TriggerDevices = new List<TriggerDevice>();
                for (var i = 0; i < 2; i++)
                {
                    TriggerDevices.Add(new TriggerDevice(){Name = $"TriggerDevice-{rand.Next(int.MaxValue)}", GroupId = group.Id});
                }

                group.ActionDevices = ActionDevices;
                group.TriggerDevices = TriggerDevices;
            }
            smartHouseCore.EfUnitOfWork.Save();
        }
    }
}