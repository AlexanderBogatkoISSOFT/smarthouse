﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SignInView.aspx.cs" Inherits="SmartHouse.Views.SignInView" %>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div class="form-horizontal">
        <fieldset>
            <div class="control-group">
                <label class="control-label" for="userid">Email:</label>
                <div class="controls">
                    <asp:TextBox runat="server"
                                 ID="tbx_Email"
                                 TextMode="Email"
                                 CssClass="form-control"
                     />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="passwordinput">Password:</label>
                <div class="controls">
                    <asp:TextBox runat="server"
                                 ID="tbx_Password"
                                 TextMode="Password"  
                                 CssClass="form-control"
                    />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="signin"></label>
                <div class="controls">
                    <asp:Button runat="server"
                                ValidationGroup="SignIn-VG" 
                                CssClass="btn btn-success"  
                                OnClick="SignInClick" 
                                Text="SignIn" 
                    />
                    <asp:Label runat="server" 
                               ID="lbl_ErrorMessage" 
                               ForeColor="Red"
                    />
                </div>
            </div>
            <asp:RegularExpressionValidator 
                ID="REV_Validator"
                runat="server" 
                ControlToValidate="tbx_Email"
                ErrorMessage="Not a valid email address" 
                SetFocusOnError="True" 
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                ValidationGroup="SignIn-VG"
                ForeColor="Red">
            </asp:RegularExpressionValidator>
        </fieldset>
    </div>
</asp:Content>