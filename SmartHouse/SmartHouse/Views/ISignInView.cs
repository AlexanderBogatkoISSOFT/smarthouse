﻿namespace SmartHouse.Views
{
    public interface ISignInView : IView
    {
        string Login { get; set; }
        string Password { get; set; }
    }
}