﻿using System;
using System.Web;
using SmartHouse.Presenters;

namespace SmartHouse.Views
{
    public partial class RegisterView : System.Web.UI.Page//, IRegisterView
    {
        private readonly RegisterPresenter _registerPresenter;
        
        public RegisterView()
        {
            _registerPresenter = new RegisterPresenter(this);
        }

        public string Name
        {
            get => tbx_Name.Text;
            set => tbx_Name.Text = value;
        }

        public string Email
        {
            get => tbx_Email.Text;
            set => tbx_Email.Text = value;
        }

        public string Password
        {
            get => tbx_Password.Text;
            set => tbx_Password.Text = value;
        }

        public string ConfirmPassword
        {
            get => tbx_ConfirmPassword.Text;
            set => tbx_ConfirmPassword.Text = value;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect(Request["ReturnUrl"] ?? "/");
            }
        }

        protected void OnClick(object sender, EventArgs e)
        {
            _registerPresenter.RegisterNewUser();
            Response.Redirect("~/SignIn");
        }
    }
}