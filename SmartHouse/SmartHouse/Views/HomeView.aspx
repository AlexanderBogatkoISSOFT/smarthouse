﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HomeView.aspx.cs" Inherits="SmartHouse.Views.HomeView" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/GroupControl.ascx" TagName="group" TagPrefix="g" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <asp:ListView runat="server" ID="lv_Groups">
        <ItemTemplate>
            <div style="display: inline-block;" class="item">
                <g:group runat="server" GroupName='<%# Eval("Name") %>' GroupId='<%# Eval("Id") %>'
                    OnDeleteGroup="OnDeleteGroup"
                    OnDeleteDevice="OnDeleteDevice"
                    OnUpdateTriggerDevices="OnUpdateTriggerDevices"
                    OnCreateDevice="OnCreateDevice"
                    OnSaveEditedGroup="OnSaveEditedGroup"
                    style="margin: 1rem;" />
            </div>
        </ItemTemplate>
    </asp:ListView>
   

    <div class="card" style="width: 28rem; height: auto; background:  gray; margin: 1rem; padding: 1rem; border-radius: 1rem; opacity: 0.8">
        <button class="btn btn-primary" style="margin: 0.2rem; width: 25rem;" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            +Add new group
        </button>
        <div class="collapse" id="collapseExample">
            <div class="card card-body" style="display: inline;">
                <asp:TextBox runat="server" 
                             ID="tbx_GroupName"
                             CssClass="form-control"
                             style="margin: 0.2rem; width: 18rem;"
                             placeholder="Group name"
                />
                <asp:LinkButton runat="server"
                                OnClick="OnAddGroup" 
                                Text="Save" 
                                ValidationGroup="add-edit-group"
                                CssClass="btn btn-primary"  
                                style="margin: 0;"  />
            </div>
        </div>
    </div>

    <asp:RequiredFieldValidator
        runat="server"
        ValidationGroup="add-edit-group"
        ControlToValidate="tbx_GroupName"
        ErrorMessage="This field cannot be emty!"
        ForeColor="Red">
    </asp:RequiredFieldValidator>
</asp:Content>