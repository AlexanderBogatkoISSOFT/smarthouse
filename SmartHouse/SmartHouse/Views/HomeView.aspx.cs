﻿using System;
using System.Collections.Generic;
using System.Web;
using SmartHouse.Controls;
using SmartHouse.Models;
using SmartHouse.Presenters;

namespace SmartHouse.Views
{
    public partial class HomeView : System.Web.UI.Page//, IHomeView
    {
        private readonly HomePresenter _homePresenter;

        public string UserLogin => HttpContext.Current.User.Identity.Name;

        public string GroupName
        {
            get => tbx_GroupName.Text;
            set => tbx_GroupName.Text = value;
        }

        public ICollection<Group> Groups
        {
            get => lv_Groups.Items as ICollection<Group>;
            set
            {
                lv_Groups.DataSource = value;
                lv_Groups.DataBind();
            }
        }

        public HomeView()
        {
            _homePresenter = new HomePresenter(this);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           Groups = _homePresenter.LoadGroupTable();
        }

        protected void OnAddGroup(object sender, EventArgs e)
        {
            _homePresenter.AddNewGroup();
        }

        protected void OnDeleteGroup(int groupId)
        {
           _homePresenter.DeleteGroup(groupId);
        }

        protected void OnDeleteDevice(int obj)
        {
            _homePresenter.DeleteDevice(obj);
        }

        protected void OnUpdateTriggerDevices(GroupControl obj)
        {
            _homePresenter.OnUpdateTriggerDevices(obj);
        }

        protected void OnSaveEditedGroup(int arg1, string arg2)
        {
            _homePresenter.RenameGroup(arg1, arg2);
        }

        protected void OnCreateDevice(int arg1, string arg2)
        {
            _homePresenter.OnCreateDevice(arg1, arg2);
        }
    }
}