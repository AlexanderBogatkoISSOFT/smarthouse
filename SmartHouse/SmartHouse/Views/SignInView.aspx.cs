﻿using System;
using SmartHouse.Presenters;

namespace SmartHouse.Views
{
    public partial class SignInView : System.Web.UI.Page, ISignInView
    {
        private readonly SignInPresenter _signInPresenter;

        public string Login
        {
            get => tbx_Email.Text;
            set => tbx_Email.Text = value;
        }

        public string Password
        {
            get => tbx_Password.Text;
            set => tbx_Password.Text = value;
        }

        public string ErrorMessage
        {
            get => lbl_ErrorMessage.Text;
            set => lbl_ErrorMessage.Text = value;
        }

        public SignInView()
        {
            _signInPresenter = new SignInPresenter(this);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void SignInClick(object sender, EventArgs e)
        {
            _signInPresenter.SignIn();
        }
    }
}