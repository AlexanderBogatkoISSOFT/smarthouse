﻿namespace SmartHouse.Views
{
    public interface IRegisterView : IView
    {
        string Email { get; set; }
        string Password { get; set; }
        string ConfirmPassword { get; set; }
    }
}