﻿using System.Collections.Generic;
using SmartHouse.Models;

namespace SmartHouse.Views
{
    public interface IHomeView : IView
    {
        int SelectedGroupId { get; set; }
        string GroupName { get; set; }
        string UserLogin { get; }
        ICollection<Group> Groups { get; set; }
    }
}