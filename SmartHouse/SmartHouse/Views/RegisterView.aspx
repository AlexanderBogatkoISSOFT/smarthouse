﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RegisterView.aspx.cs" Inherits="SmartHouse.Views.RegisterView" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div class="form-horizontal" style="alignment: center;">
        <fieldset>
            <div id="legend">
                <legend class="">Register</legend>
            </div>
            <div class="control-group">
                <!-- Username -->
                <label class="control-label" for="username">Username</label>
                <div class="controls">
                    <asp:TextBox runat="server"
                        CssClass="form-control"
                        ID="tbx_Name" />
                    <p class="help-block">Username can contain any letters or numbers, without spaces</p>
                </div>
            </div>

            <div class="control-group">
                <!-- E-mail -->
                <label class="control-label" for="email">E-mail</label>
                <div class="controls">
                    <asp:TextBox runat="server"
                        TextMode="Email"
                        CssClass="form-control"
                        ID="tbx_Email" />
                    <p class="help-block">Please provide your E-mail</p>
                </div>
            </div>

            <div class="control-group">
                <!-- Password-->
                <label class="control-label" for="password">Password</label>
                <div class="controls">
                    <asp:TextBox runat="server"
                        TextMode="Password"
                        CssClass="form-control"
                        ID="tbx_Password" />
                    <p class="help-block">Password should be at least 4 characters</p>
                </div>
            </div>

            <div class="control-group">
                <!-- Password -->
                <label class="control-label" for="password_confirm">Password (Confirm)</label>
                <div class="controls">
                    <asp:TextBox TextMode="Password"
                        CssClass="form-control"
                        runat="server"
                        ID="tbx_ConfirmPassword" />
                    <p class="help-block">Please confirm password</p>
                </div>
            </div>

            <div class="control-group">
                <!-- Button -->
                <div class="controls">
                    <asp:Button CssClass="btn btn-success" runat="server" OnClick="OnClick" Text="Register" />
                </div>
            </div>
        </fieldset>
    </div>
</asp:Content>