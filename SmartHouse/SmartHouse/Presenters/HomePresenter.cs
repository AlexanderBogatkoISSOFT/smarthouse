﻿using System.Collections.Generic;
using SmartHouse.Controls;
using SmartHouse.Models;
using SmartHouse.Views;
using static SmartHouse.Global;

namespace SmartHouse.Presenters
{
    public class HomePresenter : IPresenter
    {
        private readonly HomeView _homeView;
        private int _userId;

        public HomePresenter(HomeView homeView)
        {
            _homeView = homeView;
        } 

        public ICollection<Group> LoadGroupTable()
        {
            _userId = SHCore.FindUserByEmail(_homeView.UserLogin).Id;
            return SHCore.GetUserGroups(_userId);
        }

        public void AddNewGroup()
        {
            SHCore.AddGroupToUser(_homeView.GroupName, _userId);
            _homeView.Groups = LoadGroupTable();
            _homeView.Response.Redirect("/");
        }

        public void RenameGroup(int id, string name)
        {
            SHCore.EfUnitOfWork.Groups.Get(id).Name = name;
            SHCore.EfUnitOfWork.Save();
            _homeView.Groups = LoadGroupTable();
            _homeView.Response.Redirect("/");
        }

        public void DeleteGroup(int groupId)
        {
            SHCore.EfUnitOfWork.Groups.Delete(groupId);
            SHCore.EfUnitOfWork.Save();
            _homeView.Groups = LoadGroupTable();
            _homeView.Response.Redirect("/");
        }

        public void DeleteDevice(int deviceId)
        {
            SHCore.EfUnitOfWork.TriggerDevices.Delete(deviceId);
            SHCore.EfUnitOfWork.Save();
        }

        public void OnUpdateTriggerDevices(GroupControl obj)
        {
            obj.TriggerDevices = SHCore.GetAllGroupTriggerDevices(obj.GroupId);
        }

        public void OnCreateDevice(int obj, string name)
        {
            SHCore.AddTriggerDevice(new TriggerDevice() { GroupId = obj, Name = name });
            _homeView.Response.Redirect("/");
        }
    }
}