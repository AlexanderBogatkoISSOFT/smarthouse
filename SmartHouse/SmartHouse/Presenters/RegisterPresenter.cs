﻿using SmartHouse.Views;
using static SmartHouse.Global;

namespace SmartHouse.Presenters
{
    public class RegisterPresenter : IPresenter
    {
        private readonly RegisterView _registerView;

        public RegisterPresenter(RegisterView registerView)
        {
            _registerView = registerView;
        }

        public void RegisterNewUser()
        {
            var user = SHCore.FindUserByEmail(_registerView.Email);
            if (user == null)
            {
                SHCore.CreateNewUser(_registerView.Name, _registerView.Email, _registerView.Password);
            }
        }
    }
}