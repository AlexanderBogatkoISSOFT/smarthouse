﻿using SmartHouse.Views;
using static System.Web.Security.FormsAuthentication;
using static SmartHouse.Global;

namespace SmartHouse.Presenters
{
    public class SignInPresenter : IPresenter
    {
        private readonly SignInView _signInView;

        public SignInPresenter(SignInView view)
        {
            _signInView = view;
        }

        public void SignIn()
        {
            if (_signInView.Login != null && _signInView.Password != null && Authenticate(_signInView.Login, _signInView.Password))
            {
                SetAuthCookie(_signInView.Login, false);
                _signInView.Response.Redirect(_signInView.Request["ReturnUrl"] ?? "/");
            }
            else
            {
                _signInView.ErrorMessage = "Login or password is incorrect";
            }
        }

        public bool Authenticate(string email, string password)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password)) return false;
            return SHCore.CheckUser(email, password);
        }
    }
}