﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;

namespace SmartHouse
{
    public partial class SiteMaster : MasterPage
    {
        public bool IsAuthenticated { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            IsAuthenticated = HttpContext.Current.User.Identity.IsAuthenticated;
            BTN_SignOut.Visible = IsAuthenticated;
            a_Register.Visible = !IsAuthenticated;
            a_SignIn.Visible = !IsAuthenticated;
        }

        protected void OnClick(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }
    }
}