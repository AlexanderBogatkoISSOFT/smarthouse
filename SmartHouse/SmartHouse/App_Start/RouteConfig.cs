using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;

namespace SmartHouse
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var settings = new FriendlyUrlSettings();
            settings.AutoRedirectMode = RedirectMode.Permanent;
            routes.EnableFriendlyUrls(settings);

            routes.MapPageRoute("", "", "~/Views/HomeView.aspx");
            routes.MapPageRoute("", "SignIn", "~/Views/SignInView.aspx");
            routes.MapPageRoute("", "Register", "~/Views/RegisterView.aspx");
        }
    }
}