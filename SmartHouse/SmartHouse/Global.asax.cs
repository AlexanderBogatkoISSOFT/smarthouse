﻿using Ninject;
using System;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using SmartHouse.BLL;

namespace SmartHouse
{
    public class Global : HttpApplication
    {
        public static StandardKernel NinjectKernel = new StandardKernel(new SmartHouseConfig());
        public static SmartHouseCore SHCore = NinjectKernel.Get<SmartHouseCore>();

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}