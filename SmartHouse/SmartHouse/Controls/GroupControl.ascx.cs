﻿using SmartHouse.Models;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace SmartHouse.Controls
{
    public partial class GroupControl : System.Web.UI.UserControl
    {
        public string GroupName
        {
            get => L_GroupName.Text;
            set => L_GroupName.Text = value;
        }

        public List<TriggerDevice> TriggerDevices
        {
            get => LV_Devices.Items as List<TriggerDevice>;
            set
            {
                LV_Devices.DataSource = value;
                LV_Devices.DataBind();
            }
        }

        public int GroupId { get; set; }

        public event Action<int> DeleteGroup;
        public event Action<GroupControl> UpdateTriggerDevices; 
        public event Action<int> DeleteDevice;
        public event Action<int, string> CreateDevice;
        public event Action<int, string> SaveEditedGroup; 

        protected void Page_Load(object sender, EventArgs e)
        {
            UpdateTriggerDevices?.Invoke(this);
        }

        protected void OnDeleteGroup(object sender, CommandEventArgs e)
        {
            var groupId = int.Parse((string)e.CommandArgument);
            DeleteGroup?.Invoke(groupId);
        }

        protected void AddDevice(object sender, CommandEventArgs e)
        {
            var groupId = int.Parse((string)e.CommandArgument);
            CreateDevice?.Invoke(groupId, tbx_NewDeviceName.Text);
            UpdateTriggerDevices?.Invoke(this);
        }

        protected void OnDeleteDevice(int deviceId)
        {
            DeleteDevice?.Invoke(deviceId);
            UpdateTriggerDevices?.Invoke(this);
        }

        protected void SaveGroup(object sender, CommandEventArgs e)
        {
            var groupId = int.Parse((string)e.CommandArgument);
            SaveEditedGroup?.Invoke(groupId, TB_GroupName.Text);
        }
    }
}