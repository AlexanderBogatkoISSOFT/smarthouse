﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupControl.ascx.cs" Inherits="SmartHouse.Controls.GroupControl"  EnableViewState="true"%>
<%@ Register TagPrefix="d" TagName="device" Src="~/Controls/DeviceControl.ascx" %>
<div class="card" style="width: 31rem; height: auto; background: #dfe3e6; margin: 1rem; border-radius: 0.5rem; padding: 1rem;">
    <div class="container d-flex flex-row bd-highlight mb-3" style="height: auto; width: 30rem; margin: 0; padding: 0;">
        <asp:Label runat="server" 
                   class="p-2 bd-highlight" 
                   ID="L_GroupName" 
                   style="width: 20rem; height: 4rem; font-size: 2rem; padding: 0; margin: 0;"
        />

        <button class="btn btn-secondary;" style="margin: 0.2rem; width: 6rem;" type="button" data-toggle="collapse" data-target="#edit-group<%#GroupId%>" aria-expanded="false" aria-controls="collapseExample">
            Edit
        </button>


        <asp:LinkButton runat="server" 
                        class="p-2 bd-highlight" 
                        CssClass="btn" 
                        BackColor="#6c757d" 
                        ForeColor="White" 
                        CommandArgument=<%# GroupId %> 
                        OnCommand="OnDeleteGroup"  
                        Text="Delete"
        />
        
        <div class="collapse" id="edit-group<%#GroupId%>">
            <div class="card card-body container" style="display: inline-block; width: 27rem; height: 3rem;">
                <asp:TextBox runat="server" 
                             ID="TB_GroupName" 
                             style="margin: 0.2rem; width: 18rem;" CssClass="form-control"
                />
                <asp:Button runat="server" 
                            CssClass="btn" 
                            BackColor="#6c757d" 
                            ForeColor="White"   
                            ID="LB_SaveGroup" 
                            OnCommand="SaveGroup" 
                            CommandArgument=<%#GroupId%>
                            style="margin: 0;"
                            Text="Save" 
                />
            </div>
        </div>
    </div>
   

    <asp:ListView runat="server" ID="LV_Devices">
        <ItemTemplate>
            <d:device runat="server" 
                      DeviceName='<%# Eval("Name") %>' 
                      DeviceId='<%# Eval("Id") %>' 
                      OnDeleteDevice="OnDeleteDevice"/>
        </ItemTemplate>
    </asp:ListView>
    
    
    <button class="btn btn-primary" style="margin: 0.2rem; width: 25rem;" type="button" data-toggle="collapse" data-target="#group<%#GroupId%>" aria-expanded="false" aria-controls="collapseExample">
        +Add new device
    </button>
    <div class="collapse" id="group<%#GroupId%>">
        <div class="card card-body" style="display: inline-block;">
            <asp:TextBox runat="server" style="margin: 0.2rem; width: 18rem;" CssClass="form-control" ID="tbx_NewDeviceName" placeholder="Device name"></asp:TextBox>
            <asp:Button runat="server" 
                            OnCommand="AddDevice" 
                            CssClass="btn" 
                            BackColor="#6c757d" 
                            ForeColor="White"
                            CommandArgument="<%#GroupId%>"
                            Text="Add" 
                            style="margin: 0;"
            />
        </div>
    </div>
</div>