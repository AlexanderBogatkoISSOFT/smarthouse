﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DeviceControl.ascx.cs" Inherits="SmartHouse.Controls.DeviceControl" %>
<div class="card container" style="width: 28rem; height: 4rem; background: whitesmoke; padding: 0; margin: 0.3rem; border-radius: 0.3rem;">
    <asp:Label runat="server" 
               ID="LB_DeviceName" 
               style="padding: 1rem; margin: 0; padding: 0;" 
     />
    <asp:LinkButton runat="server" 
                    style="margin-top: 0.3rem; opacity: 0.8;" 
                    BackColor="#9d9d9d"  
                    ForeColor="White" 
                    OnCommand="OnDeleteDevice" 
                    CommandArgument=<%# DeviceId %> 
                    Text="Delete" 
                    CssClass="btn" 
     />
</div>