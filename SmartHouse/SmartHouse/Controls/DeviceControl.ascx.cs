﻿using System;
using System.Web.UI.WebControls;

namespace SmartHouse.Controls
{
    public partial class DeviceControl : System.Web.UI.UserControl
    {
        public string DeviceName
        {
            get => LB_DeviceName.Text;
            set => LB_DeviceName.Text = value;
        }
 
        public int DeviceId { get; set; }
        public event Action<int> DeleteDevice;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void OnDeleteDevice(object sender, CommandEventArgs e)
        {
            var deviceId = int.Parse((string)e.CommandArgument);
            DeleteDevice?.Invoke(deviceId);
        }
    }
}