﻿using SmartHouse.IDAL.IRepositories;

namespace SmartHouse.IDAL
{
    public interface IEFUnitOfWork
    {
        IUserRepository Users { get; }
        IGroupRepository Groups { get; }
        ITriggerDeviceRepository TriggerDevices { get; }
        IVirtualDeviceRepository VirtualDevices { get; }
        IActionDeviceRepository ActionDevices { get; }
        //IUnusedDeviceRepository UnusedDevices { get; }

        void Save();
        void Dispose();
    }
}