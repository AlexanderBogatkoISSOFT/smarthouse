﻿using SmartHouse.Models;

namespace SmartHouse.IDAL.IRepositories
{
    public interface IUserRepository : IRepository<User>
    {
    }
}