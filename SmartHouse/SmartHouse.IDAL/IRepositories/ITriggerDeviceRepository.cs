﻿using SmartHouse.Models;

namespace SmartHouse.IDAL.IRepositories
{
    public interface ITriggerDeviceRepository : IRepository<TriggerDevice>
    {
    }
}