﻿using SmartHouse.Models;

namespace SmartHouse.IDAL.IRepositories
{
    public interface IActionDeviceRepository : IRepository<ActionDevice>
    {
    }
}