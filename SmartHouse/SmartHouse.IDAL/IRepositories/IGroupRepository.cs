﻿using SmartHouse.Models;

namespace SmartHouse.IDAL.IRepositories
{
    public interface IGroupRepository : IRepository<Group>
    {
    }
}