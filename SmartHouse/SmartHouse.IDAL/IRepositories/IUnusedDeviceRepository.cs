﻿using SmartHouse.Models;

namespace SmartHouse.IDAL.IRepositories
{
    public interface IUnusedDeviceRepository : IRepository<Device>
    {
    }
}