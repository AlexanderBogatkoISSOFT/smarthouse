﻿using SmartHouse.Models;

namespace SmartHouse.IDAL.IRepositories
{
    public interface IVirtualDeviceRepository : IRepository<VirtualDevice>
    {
    }
}