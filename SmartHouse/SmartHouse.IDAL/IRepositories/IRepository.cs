﻿using System.Collections.Generic;
using SmartHouse.Models;

namespace SmartHouse.IDAL.IRepositories
{
    public interface IRepository<T> where T : Model
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        void Create(T item);
        void Update(T item);
        void Delete(int id);
    }
}