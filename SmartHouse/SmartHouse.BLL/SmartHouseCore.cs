﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using SmartHouse.IDAL;
using SmartHouse.Models;

namespace SmartHouse.BLL
{
    public class SmartHouseCore
    {
        private const string Salt = "Salt";
        public IEFUnitOfWork EfUnitOfWork { get; }

        public SmartHouseCore(IEFUnitOfWork efUnitOfWork)
        {
            EfUnitOfWork = efUnitOfWork;
        }

        public void CreateNewUser(string name, string email, string password)
        {
            var passwordHash = GetPasswordHash(password);
            var user = new User(){Name = name, Email = email, PasswordHash = passwordHash};
            EfUnitOfWork.Users.Create(user);
            EfUnitOfWork.Save();
        }

        public User FindUserByEmail(string email)
        {
            return EfUnitOfWork.Users.GetAll().FirstOrDefault(u => u.Email == email);
        }

        public bool CheckUser(string email, string password)
        {
            var u = FindUserByEmail(email);
            if (u != null)
            {
                return u.PasswordHash == GetPasswordHash(password);
            }
            return false;
        }

        public string GetPasswordHash(string password)
        {
            var inputBytes = Encoding.UTF8.GetBytes(password + Salt);
            var hashedBytes = new SHA256CryptoServiceProvider().ComputeHash(inputBytes);
            return BitConverter.ToString(hashedBytes);
        }

        public ICollection<Group> GetUserGroups(int userId)
        {
            return EfUnitOfWork.Groups.GetAll().Where(g => g.UserId == userId).ToList();
        }

        public void AddGroupToUser(string groupName, int userId)
        {
            var group = new Group() {Name = groupName, UserId = userId};
            EfUnitOfWork.Groups.Create(group);
            EfUnitOfWork.Save();
        }

        public List<TriggerDevice> GetAllGroupTriggerDevices(int groupId)
        {
            return EfUnitOfWork.TriggerDevices.GetAll().Where(tg => tg.GroupId == groupId).ToList();
        }

        public void AddTriggerDevice(TriggerDevice device)
        {
            EfUnitOfWork.TriggerDevices.Create(device);
            EfUnitOfWork.Save();
        }

        public ICollection<ActionDevice> GetAllGroupActionDevices(int groupId)
        {
            return EfUnitOfWork.ActionDevices.GetAll().Where(ad => ad.GroupId == groupId).ToList();
        }

        public ICollection<VirtualDevice> GetAllGroupVirtualDevices(int groupId)
        {
            return EfUnitOfWork.VirtualDevices.GetAll().Where(vd => vd.GroupId == groupId).ToList();
        }
    }
}