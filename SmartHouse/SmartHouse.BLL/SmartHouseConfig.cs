﻿using Ninject.Modules;
using SmartHouse.DAL;
using SmartHouse.IDAL;

namespace SmartHouse.BLL
{
    public class SmartHouseConfig : NinjectModule
    {
        public override void Load()
        {
            Bind<IEFUnitOfWork>().To<EFUnitOfWork>();
        }
    }
}