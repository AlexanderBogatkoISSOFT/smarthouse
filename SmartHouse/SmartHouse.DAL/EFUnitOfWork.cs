﻿using System;
using SmartHouse.DAL.Repositories;
using SmartHouse.IDAL;
using SmartHouse.IDAL.IRepositories;

namespace SmartHouse.DAL
{
    public class EFUnitOfWork : IDisposable, IEFUnitOfWork
    {
        private readonly SmartHouseContext _db = new SmartHouseContext();
        private bool _disposed;
        private UserRepository _userRepository;
        private GroupRepository _groupRepository;
        private TriggerDeviceRepository _triggerDeviceRepository;
        private VirtualDeviceRepository _virtualDeviceRepository;
        private ActionDeviceRepository _actionDeviceRepository;
        //private UnusedDeviceRepository _unusedDeviceRepository;

        public IUserRepository Users => _userRepository ?? (_userRepository = new UserRepository(_db));

        public IGroupRepository Groups => _groupRepository ?? (_groupRepository = new GroupRepository(_db));

        public ITriggerDeviceRepository TriggerDevices => _triggerDeviceRepository ?? (_triggerDeviceRepository = new TriggerDeviceRepository(_db));

        public IVirtualDeviceRepository VirtualDevices => _virtualDeviceRepository ?? (_virtualDeviceRepository = new VirtualDeviceRepository());

        public IActionDeviceRepository ActionDevices => _actionDeviceRepository ?? (_actionDeviceRepository = new ActionDeviceRepository(_db));

        //public IUnusedDeviceRepository UnusedDevices => _unusedDeviceRepository ?? (_unusedDeviceRepository = new UnusedDeviceRepository());

        public void Save()
        {
            _db.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }
            if (disposing)
            {
                _db.Dispose();
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}