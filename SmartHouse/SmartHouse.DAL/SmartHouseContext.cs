﻿using System.Data.Entity;
using SmartHouse.Models;

namespace SmartHouse.DAL
{
    public class SmartHouseContext : DbContext
    {
        public SmartHouseContext() : base("SmartHouse.Database")
        { }
        public DbSet<User> Users { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<TriggerDevice> TriggerDevices { get; set; }
        public DbSet<VirtualDevice> VirtualDevices { get; set; }
        public DbSet<ActionDevice> ActionDevices { get; set; }
        //public DbSet<Device> UnusedDevices { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<Group>().ToTable("Groups");
            modelBuilder.Entity<TriggerDevice>().ToTable("TriggerDevices");
            modelBuilder.Entity<VirtualDevice>().ToTable("ActionDevices");
            modelBuilder.Entity<ActionDevice>().ToTable("VirtualDevices");
            //modelBuilder.Entity<Device>().ToTable(nameof(UnusedDevices));

            //modelBuilder.Entity<ActionDevice>().Property(td => td.GroupId).HasColumnName("GroupId");
            //modelBuilder.Entity<ActionDevice>().Property(td => td.Id).HasColumnName("Id");

            //modelBuilder.Entity<Group>()
            //    .HasMany(p => p.ActionDevices)
            //    .WithRequired(p => p.Group);

            //modelBuilder.Entity<TriggerDevice>()
            //.Map(m =>
            //{
            //    m.MapInheritedProperties();
            //    m.ToTable("TriggerDevices");
            //});
            //modelBuilder.Entity<ActionDevice>().Map(m =>
            //{
            //    m.MapInheritedProperties();
            //    m.ToTable("ActionDevices");
            //});
            //modelBuilder.Entity<ActionDevice>().Map(m =>
            //{
            //    m.MapInheritedProperties();
            //    m.ToTable("VirtualDevices");
            //});

            base.OnModelCreating(modelBuilder);
        }
    }
}