﻿using System.Collections.Generic;
using System.Data.Entity;
using SmartHouse.IDAL.IRepositories;
using SmartHouse.Models;

namespace SmartHouse.DAL.Repositories
{
    public class ActionDeviceRepository : IActionDeviceRepository
    {
        private readonly SmartHouseContext _db;

        public ActionDeviceRepository(SmartHouseContext context)
        {
            _db = context;
        }

        public IEnumerable<ActionDevice> GetAll()
        {
            return _db.ActionDevices;
        }

        public ActionDevice Get(int id)
        {
            return _db.ActionDevices.Find(id);
        }

        public void Create(ActionDevice item)
        {
            _db.ActionDevices.Add(item);
        }

        public void Update(ActionDevice item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var device = _db.ActionDevices.Find(id);
            if (device != null)
                _db.ActionDevices.Remove(device);
        }
    }
}