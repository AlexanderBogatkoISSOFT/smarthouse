﻿using System.Collections.Generic;
using System.Data.Entity;
using SmartHouse.IDAL.IRepositories;
using SmartHouse.Models;

namespace SmartHouse.DAL.Repositories
{
    public class GroupRepository : IGroupRepository
    {
        private readonly SmartHouseContext _db;

        public GroupRepository(SmartHouseContext context)
        {
            _db = context;
        }

        public IEnumerable<Group> GetAll()
        {
            return _db.Groups;
        }

        public Group Get(int id)
        {
            return _db.Groups.Find(id);
        }

        public void Create(Group group)
        {
            _db.Groups.Add(group);
        }

        public void Update(Group group)
        {
            _db.Entry(group).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var group = _db.Groups.Find(id);
            if (group != null)
                _db.Groups.Remove(group);
        }
    }
}