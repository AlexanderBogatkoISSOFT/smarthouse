﻿using System.Collections.Generic;
using SmartHouse.IDAL.IRepositories;
using SmartHouse.Models;

namespace SmartHouse.DAL.Repositories
{
    public class UnusedDeviceRepository : IUnusedDeviceRepository
    {
        public IEnumerable<Device> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public Device Get(int id)
        {
            throw new System.NotImplementedException();
        }

        public void Create(Device item)
        {
            throw new System.NotImplementedException();
        }

        public void Update(Device item)
        {
            throw new System.NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}