﻿using SmartHouse.Models;
using System.Collections.Generic;
using System.Data.Entity;
using SmartHouse.IDAL.IRepositories;

namespace SmartHouse.DAL.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly SmartHouseContext _db;

        public UserRepository(SmartHouseContext context)
        {
            _db = context;
        }

        public IEnumerable<User> GetAll()
        {
            return _db.Users;
        }

        public User Get(int id)
        {
            return _db.Users.Find(id);
        }

        public void Create(User user)
        {
            _db.Users.Add(user);
        }

        public void Update(User user)
        {
            _db.Entry(user).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var user = _db.Users.Find(id);
            if (user != null)
                _db.Users.Remove(user);
        }
    }
}