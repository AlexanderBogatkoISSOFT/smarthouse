﻿using System.Collections.Generic;
using System.Data.Entity;
using SmartHouse.IDAL.IRepositories;
using SmartHouse.Models;

namespace SmartHouse.DAL.Repositories
{
    public class TriggerDeviceRepository : ITriggerDeviceRepository
    {
        private readonly SmartHouseContext _db;

        public TriggerDeviceRepository(SmartHouseContext context)
        {
            _db = context;
        }

        public IEnumerable<TriggerDevice> GetAll()
        {
            return _db.TriggerDevices;
        }

        public TriggerDevice Get(int id)
        {
            return _db.TriggerDevices.Find(id);
        }

        public void Create(TriggerDevice item)
        {
            _db.TriggerDevices.Add(item);
        }

        public void Update(TriggerDevice item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var device = _db.TriggerDevices.Find(id);
            if (device != null)
                _db.TriggerDevices.Remove(device);
        }
    }
}