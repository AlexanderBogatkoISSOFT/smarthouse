﻿using System.Collections.Generic;
using SmartHouse.IDAL.IRepositories;
using SmartHouse.Models;

namespace SmartHouse.DAL.Repositories
{
    public class VirtualDeviceRepository : IVirtualDeviceRepository
    {
        public IEnumerable<VirtualDevice> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public VirtualDevice Get(int id)
        {
            throw new System.NotImplementedException();
        }

        public void Create(VirtualDevice item)
        {
            throw new System.NotImplementedException();
        }

        public void Update(VirtualDevice item)
        {
            throw new System.NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}